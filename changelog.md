## Changelog

### 4 July 2014

    arranger: NavigationPane - added reverse transition support
    arranger: NavigationPane - 
    
    effects: Removal of transition class names
    
    events: CSS3 Animation & Transition event listening
    events: animationend & transitionend normalized via EventHandler
    
    transition: Reverse transition animations
    transition: Improved transition performance
    transition: Support for more complex transitions
    
    uicore: Fixed ownership issues with koiosUI.View subclasses
    

### 3 July 2014

__modules-web__

    animator: Added koiosUI.AnimationSupport
    animator: Fix for properties without numerical starting value
    animator: Improved suffix discovery on animations
    animator: Transform animations
    
    arranger: Began koiosUI.NavigationPane
    
    uicore: Improved layout on BB10 devices
    uicore: Align now a property of koiosUI.View as well
    
    transition: Added koiosUI.navigations
    transition: Added koiosUI.NavigationTransition
    
### 2 July 2014

__modules-web__

    animator: koiosUI.animate() now returns resolving promise
    
    navigation: added koiosUI.NavigationPane
    
    uicore: Began koiosUI.AccessoryView
    uicore: Added .presentAccessory()
    
### 1 July 2014

__core__

    Bug fixes with owned objects
    
__modules-web__

    scroller: Improved scrolling
    
    uicore: Fixed inherited view issues
    uicore: Fixed view ownership issues
    
### 30 June 2014

__core__

    Computed properties suppport (custom "get" methods)
    
    
__modules-web__

    events: DOM events now pass through koiosUI.EventHandler
    events: Touch/Mouse events normalized to Tap Events
    events: Tap, DoubleTap event
    events: Flick, Hold, HoldEnd gestures
    events: Drag, DragMove, DragEnd events
    
### 29 June 2014

__modules-web__

    scroller: Updated koiosUI.ScrollView
    scroller: Added koiosUI.ScrollAnimator
    scroller: Added koiosUI.TouchScrollHandler
    scroller: Added koiosUI.TranslateHandler
    
### 28 June 2014

__core__

    Added koios.Event
    
__modules-web__

    scroller: Basic koiosUI.ScrollView
    scroller: Added koiosUI.OverflowScrollHandler
    scroller: Added koiosUI.ScrollWheelHandler
    
    uicore: Fixes for Mobile Safari
    
### 27 June 2014

__core__

    Added .events object for this.on() support
    
__modules-web__

    uicore: Added support for appearance effects in DrawableSupport
    uicore: Added koiosUI.View .requiresNode()
    uicore: Began DOM event support
    uicore: Fixed Safari Layout
    
### 26 June 2014

__core__

    Added koios.Device browser detection
    Added koios.Device.hasTouch & .hasGesture
    Added koios.Device.vendorPrefix()
    Added koios.lerp(), koios.easedLerp() and koios.easing
    
    Fixed koios.array.each not using native forEach
    
    koios.array.each now passes in index as well as element.
    
__modules-web__

    animator: added koiosUI.requestAnimationFrame, .cancelAnimationFrame
    animator: added additional koios.easing methods
    animator: added koiosUI.Animator
    animator: added koiosUI.Animation
    
    dom: added koios.getComputedStyle()
    
    effects: added koiosUI .effect(), appear(), attention() & disappear()
    effects: added koiosUI.EffectSupport
    
    transform: Added koiosUI.transform(), .domTransforms(), .transformsToCss()
    transform: Added koiosUI.canAccelerate, .canTransform, .cssKeys
    
    uicore: stubbed koiosUI.View .show(), .hide(), .appear() & .disappear()
    
### 25 June 2014

__core__

    Added koios.string.toObject()
    
__modules-web__

    arranger: Fixed default className for layouts in ArrangerSupport
    arranger: Implemented this.arrange() for ArrangerSupport inherited objects
    
    uicore: koiosUI.DrawableSupport's 'rendered' event now 'render'
    uicore: Fixed multiple render event emissions
    uicore: Fixed koiosUI.View issues with cssStyle being string
    
### 24 June 2014

__core__

    Fixed multiple once() listeners of the same eventst
    
__modules-web__

    arranger: Added koiosUI.Arranger
    arranger: Added koiosUI.ArrangerSupport
    arranger: Added koiosUI.Layout
    arranger: Added koiosUI.Pane

    uicore: Rearranged files
    uicore: Added koios_uicore.scss
    uicore: Added koiosUI.FlexLayoutSupport
    uicore: Added koiosUI.ViewController
    uicore: Root View Controller support in koiosUI.Application
        
### 23 June 2014

__modules-web__

    uicore: Added koiosUI
    uicore: Added koiosUI.View
    uicore: Added koiosUI.DrawableSupport
    uicore: Added koiosUI.Application
    uicore: Added koiosUI.Fragment
    uicore: Added koiosUI.AjaxFragment
    
    style: Added koiosUI.color