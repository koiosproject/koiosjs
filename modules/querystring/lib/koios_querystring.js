/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koios_querystring.js
Provides koios.QueryString - a query string wrapper
and assistant class
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module*/

/**
Provides a helper object to handle query strings.

@module web/querystring
*/

/**
koios.QueryString is a helper object for manipulating query strings. It is 
used internally with koios.Ajax.

It is aliased as koios.queryString(), and can be called with a string (including the ?)

    var qs = koios.queryString("?koios=rocks");

Or it can generated from an Object itself

    var qs = koios.queryString({koios:"rocks"});
    
@class koios.QueryString
@extend koios.Class
*/

koios.define({
    className: "koios.QueryString",
    extend: "koios.Class",
    alias: "koios.queryString",

    _stripHTML: false,
    
    $constructor: function (args) {
        if (koios.isObject(args)) {
            this.theObject = args;
        } else if (koios.isString(args)) {
            this.theObject = {};
            this._parseString(args);
        }
        
        this.$super("$constructor");
    },
    
// =========================================================================
// Private
    
    _parseString: function (theString) {
        var parts = theString.split("?"),
            t = this,
            args,
            argParts;
        
        if (parts.length > 1) {
            args = parts[1].split("&");
            
            koios.each(args, function (theItem) {
                argParts = theItem.split("=");
                if (koios.exists(argParts[0]) && koios.exists(argParts[1])) {
                    t.theObject[argParts[0]] = argParts[1];
                }
            });
        }
    },
    
// =========================================================================
// Query Manipulation
       
/**
Returns the value associated with a given key in a query string. Omit theKey to return
the entire query string as an object.

@method get
@param {String} theKey - the key to return
@returns Object or Value
*/
    
    get: function (theKey) {
        if (!theKey) {
            return this.theObject;
        } else {
            return this.theObject[theKey];
        }
    },
        
/**
Removes a key or an array of keys from the query string

@method remove
@param {String or Array} theKey - the key or keys to remove
@returns this
*/
    
    remove: function (theKey) {
        if (koios.isString(theKey)) {
            delete this.theObject[theKey];
        } else if (koios.isArray(theKey)) {
            var remove = this.bind("remove");
            koios.each(theKey, function (theItem) {
                remove(theItem);
            });
        }
        
        return this;
    },
    
/**
Adds either a key-value object to the query string, or sets theKey to theValue within
the query string.

    var qs = koios.queryString({koios:"rocks"});
    qs.set("library", "koios");
    qs.set({query:"built"});
    
@method set
@param {String or Object} theKey - either an object to mix into the Object, or theKey to set theValue to.
@param {Value} theValue - the value to set theKey to if theKey is a valid String key (optional)
@returns this
*/
    
    set: function (theKey, theValue) {
        if (koios.isString(theKey)) {
            this.theObject[theKey] = theValue;
        } else if (koios.isObject(theKey)) {
            koios.mixin(this.theObject, theKey);
        }
        
        return this;
    },

/**
Returns the query string as a String itself. 

    var qs = koios.queryString({koios:"rocks"});
    qs.add("library", "koios");
    qs.toString();
    // -> ?koios=rocks&library=koios
    
If you pass in true as the lone parameter, it will omit the opening "?"

@method toString
@param {BOOL} ignoreQueryStart - pass true to omit the opening "?" (optional)
@returns String
*/
    
    toString: function (ignoreQueryStart) {
        var str = (ignoreQueryStart) ? "" : "?";
        
        koios.each(this.theObject, function (key, value) {
            value = encodeURIComponent(value);
            if (koios.exists(key) && koios.exists(value)) {
                str += key + "=" + value + "&";
            }
        });
        
        str = str.substr(0, str.length - 1);
        return str;
    }
});
