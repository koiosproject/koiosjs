/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koios_definemixins.js
Provides additional plugins for koios.define()
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module*/

// =================================================

/**
@class koios
*/

function _addAccumulated(accumulated, extended) {
    if (koios.indexOf(accumulated, extended) === -1) {
        accumulated.push(extended);
    }
}

// =================================================
// Core Define Mixins

koios.define.addMixin(function (theName, theConstructor, thePrototype) {
    var next = theConstructor.prototype._extends,
        extendedProps,
        inherit,
        broadcastable;

    // Core Statics
    koios.mixin(theConstructor, {
        _subclassFn: [],
        
        create: function (theObject) {
            return new this(theObject);
        },
        
        extend: function (theObject) {
            theObject.extend = thePrototype.className;
            return koios.define(theObject);
        },
        
        mixin: function (theObject) {
            koios.mixin(this.prototype, theObject);
        },
        
        inherit: function (theMixin) {
            var ctor = koios.getConstructor(theMixin),
                proto = ctor.prototype;
            
            if (koios.isaSubclassOf(proto, "koios.Mixin")) {
                proto.mixin(theConstructor, thePrototype);
                koios.mixin(thePrototype, proto.mixinProperties);
            }
        }
    });

    thePrototype._initFn = [];
    
// =================================================
// Subclassing
    
    if (!theConstructor.subclass) {
        theConstructor.subclass = function (className) {
//            koios.log("subclassing " + theName + " with " + className);
        };
    }
    
    
    while (next) {
        next.subclass(theName, theConstructor, thePrototype);
        next = next.prototype._extends;
    }
    
// =================================================
// Additional Statics
    
    if (thePrototype.statics) {
        koios.mixin(theConstructor, thePrototype.statics);
        thePrototype.statics = null;
    }
    
// =================================================
// Accumulated 

    if (!thePrototype.accumulated) {
        thePrototype.accumulated = [];
    }
        
    // The accumulated property, is by definition, accumulated.
    if (thePrototype._extends) {
        extendedProps = thePrototype._extends.prototype.accumulated;
        koios.each(extendedProps, function (theProp) {
            _addAccumulated(thePrototype.accumulated, theProp);
        });
    }
    
    if (!thePrototype._extends) {
        return;
    }
    
    koios.each(thePrototype.accumulated, function (key) {
        var val = koios.clone(thePrototype._extends.prototype[key]),
            curVal = thePrototype[key],
            newVal;
        
        if (koios.isObject(val)) {
            curVal = koios.mixin({}, thePrototype[key]);
            newVal = koios.mixin({}, val);
            thePrototype[key] = koios.mixin(newVal, curVal);
        } else if (koios.isArray(val)) {
            koios.each(val, function (item) {
                if (koios.indexOf(curVal, item) === -1) {
                    thePrototype[key].push(item);
                }
            });
        } else {
            thePrototype[key] = curVal;
        }
    });
    
// =================================================
// Inherit support

    if (thePrototype.inherit) {
        inherit = (koios.isArray(thePrototype.inherit)) ? thePrototype.inherit : [thePrototype.inherit];
        
        koios.each(inherit, function (theItem) {
            var ctor = koios.getConstructor(theItem),
                proto = ctor.prototype;
            
            if (koios.isaSubclassOf(proto, "koios.Mixin")) {
                proto.mixin(theConstructor, thePrototype);
                koios.mixin(thePrototype, proto.mixinProperties);
            }
        });
        
//        thePrototype.inherit = null;
    }

// =================================================
// Broadcast support
    
    if (thePrototype.broadcastable) {
        broadcastable = (koios.isArray(thePrototype.broadcastable)) ? thePrototype.broadcastable : [thePrototype.broadcastable];
        koios.each(broadcastable, function (ev) {
            koios.messageEmitter.broadcast(theName + ":" + ev);
        });
    }
    
});
