/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koios_singleton.js
Provides koios.singleton()
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module*/

// =================================================

/**
@class koios
*/

/**
A singleton generator for koios.define()

@method singleton
*/

koios.singleton = function (theObject, theContext) {
    theObject.ignoreAlias = true;
    
    var name = theObject.alias || theObject.className,
        Ctor = koios.define(theObject);
    
    koios.setObject(name, new Ctor(), theContext);
};
