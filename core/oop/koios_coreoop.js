/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koios_coreoop.js
Provides utility functions for koios.define()
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module*/

// =================================================

/**
A placeholder object used to build KoiosJS objects
@class koios.component
*/

koios.component = function () {};

var _id = 0,
    global = this;

koios._uid = function () {
    _id += 1;
    return _id;
};

/**
@class koios
*/

/**
Returns a constructor for a given koios.Class derived object

@method getConstructor
@param {String or Function} theClass - the class to look for
@returns Object
*/

koios.getConstructor = function (theClass) {
    if (koios.isFunction(theClass) || !theClass) {
        return theClass;
    }
    
    var parts = theClass.split("."),
        obj = global,
        i,
        l = parts.length,
        key;
    
    for (i = 0; i < l; i += 1) {
        key = parts[i];
        obj = obj[key];
        if (!obj) {
            return {};
        }
    }
    
    return obj;
};

/**
Generates the generic constructor function used by KoiosJS

@private
*/

koios._makeConstructor = function () {
    return function () {
        if (this.$constructor) {
            this.$constructor.apply(this, arguments);
        }
        if (this.$init) {
            this.$init.apply(this, arguments);
        }
    };
};

/**
Sets the prototype as part of the koios.define() process - you should have zero
reason to invoke this directly

@private
*/

koios._setPrototype = function (name, ctor, proto) {
    koios.component.prototype = proto;
    ctor.prototype = new koios.component();
};
