/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koios_define.js
Provides koios.define()
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module*/

// =================================================

/**
@class koios
*/

/**
The Core Object Orientation generator.

@method define
*/

koios.define = function (theObject) {
    if (!koios.exists(theObject)) {
        throw "koios.define() error: You must pass an object definition into koios.define()";
//        return koios.throw("koios.define() error: You must pass an object definition into koios.define()");
    }
    
    if (!theObject.className) {
        throw "koios.define() error: You must give your new class a name.";
//        return koios.throw("koios.define() error: You must give your new class a name.");
    }

    var name = theObject.className,
        Ctor = koios._makeConstructor(),
        extClass = theObject.extend,
        extend = extClass && koios.getConstructor(extClass),
        proto = extend && extend.prototype,
        alias;
    
    if (theObject.extend && !proto) {
        throw "koios.define() error: Attempting to subclass a class '" + extClass + "' which does not exist";
//        return koios.throw("koios.define() error: Attempting to subclass a class '" + extClass + "' which does not exist");
    }
    
    delete theObject.extend;
    
    if (proto) {
        koios._setPrototype(name, Ctor, proto);
    } else {
        koios._setPrototype(name, Ctor, {});
    }
    
    Ctor.prototype.className = name;
    Ctor.prototype._extends = extend;
    Ctor.prototype._ctor = Ctor;
    
    koios.mixin(Ctor.prototype, theObject);
    
    koios.each(koios.define.mixins, function (theMixin) {
        theMixin(name, Ctor, Ctor.prototype);
    });
    
    koios.setObject(name, Ctor);
    
    if (!theObject.ignoreAlias) {
        alias = (theObject.hasOwnProperty("alias")) ? theObject.alias : name.toLowerCase();
        if (alias !== name) {
            koios.setObject(alias, function (args) {
                return new Ctor(args);
            });
        }
    }
    
    return Ctor;
};

// =================================================

koios.define.mixins = [];
koios.define.addMixin = function (theMixin) {
    koios.define.mixins.push(theMixin);
};