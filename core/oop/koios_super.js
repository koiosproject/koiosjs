/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koios_super.js
Provides this.$super() for within OO
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module*/

// =================================================

/**
@class koios
*/

koios._super = function (thePrototype, theContext, theMethod, theArguments) {
    var args = [],
        className,
        extend,
        nextClass,
        nextProto,
        proto = null,
        i,
        key;
    
    if (koios.isArray(theArguments)) {
        args = theArguments.slice();
    } else if (theArguments) {
        args = koios.toArray(theArguments);
    }

    for (i = 0; i < args.length; i += 1) {
        key = args[i];
        
        if (koios.isObject(key) && key._sender) {
            proto = key._sender;
            args.splice(i);
        }
    }

    if (proto) {
        className = proto.className;
		extend = proto._extends.prototype;
    } else {
        while (thePrototype._extends) {
            if (thePrototype.className === thePrototype[theMethod]._owner) {
                break;
            }
            thePrototype = thePrototype._extends.prototype;
        }

        className = thePrototype.className;
        extend = thePrototype._extends.prototype;
    }

    if (!extend) {
        // For testing...
        koios["throw"]("koios._super serious error: extend doesn't exist.");
    }

    if (extend.className) {
        if (extend[theMethod] && extend[theMethod]._owner === extend.className) {
            nextClass = extend.className;
        } else {
            nextClass = extend[theMethod]._owner;
        }

        nextProto = koios.getConstructor(nextClass).prototype;
        args.push({_sender: nextProto});
        nextProto[theMethod].apply(theContext, args);
    }
};

function _makeSuper(thePrototype) {
    return function (method, args) {
        koios._super(thePrototype, this, method, args);
    };
}

koios.define.addMixin(function (theName, theConstructor, thePrototype) {
    var fns = koios.object.functions(thePrototype, true);
    koios.each(fns, function (theItem) {
        if (!theItem._owner) {
            theItem._owner = theName;
        }
    });

    thePrototype.$super = _makeSuper(thePrototype);
});
