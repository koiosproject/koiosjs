/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koios_propertysupport.js
Provides koios.PropertySupport - enabling property definitions within
KoiosJS objects
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module*/

/**
A support object which enables property definitions within KoiosJS

@class koios.PropertySupport
@extends koios.Mixin
*/

/**
@module core
*/

function _makeSet(prop) {
    return function (newVal) {
        var owner = prop.owner,
            emit = (owner.emit) ? owner.bind("emit") : koios.noop,
            curVal = prop.value;
        
        emit(prop.name + ":beforeChange", prop.value);
        
        if (prop.frozen) {
            return;
        }
        
        if (!prop.validate(newVal)) {
            return;
        }
        
        prop.value = newVal;
        
        emit(prop.name + ":change", newVal, curVal);
    };
}

function _makeProp(proto, key, value) {
    var val = (value.hasOwnProperty("value")) ? value.value : null,
        validate = (value.hasOwnProperty("validate")) ? value.validate : function () { return true; },
        prop = {
            _default: val,
            name: key,
            owner: proto,
            value: val,
            validate: validate,
            frozen: false,
            get: value.get
        };
    
    
    prop.set = _makeSet(prop);
    return prop;
}

koios.define({
    className: "koios.PropertySupport",
    extend: "koios.Mixin",
    
    mixin: function (theConstructor, thePrototype) {
        if (!thePrototype.properties) {
            thePrototype.properties = {};
        }
        
        if (!koios.contains(thePrototype.accumulated, "properties")) {
            thePrototype.accumulated.push("properties");
        }

        thePrototype._initFn.push(this._setProps);
    },
    
    _setProps: function () {
        var theObject = this,
            props = {};
        
        koios.each(this.properties, function (key, value) {
            var val = (value.hasOwnProperty("value")) ? value.value : null,
                prop = _makeProp(theObject, key, value);
            
            props[key] = prop;
        });
        
        this._props = props;
    },
    
    mixinProperties: {

// =================================================
// Property Manipulation

/**
Freezes a property with name propName, which prevents it from being
mutated until unfrozen

__Inherited from koios.PropertySupport__

@method freeze
@param {String} propName - the name of the property to freeze
*/
        
        freeze: function (propName) {
            if (this._props.hasOwnProperty(propName)) {
                this._props[propName].frozen = true;
            }
        },

/**
Returns the value of the property with name propName

__Inherited from koios.PropertySupport__

@method get
@param {String} propName - the name of the property to return
*/

        get: function (propName) {
            if (this._props.hasOwnProperty(propName)) {
                if (this._props[propName].get) {
                    return this._props[propName].get.apply(this, []);
                } else {
                    return this._props[propName].value;
                }
            }
            return null;
//            return (this._props.hasOwnProperty(propName)) ? this._props[propName].value : null;
        },

/**
Returns the value of the property to the default value set at koios.define() time.

__Inherited from koios.PropertySupport__

@method reset
@param {String} propName - the name of the property to reset
*/

        reset: function (propName) {
            if (this._props.hasOwnProperty(propName)) {
                var prop = this._props[propName];
                this.set(propName, prop._default);
            }
        },
 
/**
Sets the value of the property to propVal, assuming the property isn't frozen and that
the optional validate method is passed.

__Inherited from koios.PropertySupport__

@method set
@param {String} propName - the name of the property to set
@param {Value} propVal - the value to set the property to.
*/

        set: function (propName, propVal) {
            var set = (this._props.hasOwnProperty(propName)) ? this._props[propName].set : koios.noop;
            set(propVal);
        },
 
/**
Unfreezes a property with name propName, which once again allows it to be mutated.

__Inherited from koios.PropertySupport__

@method unfreeze
@param {String} propName - the name of the property to unfreeze
*/

        unfreeze: function (propName) {
            if (this._props.hasOwnProperty(propName)) {
                this._props[propName].frozen = false;
            }
        }
    }
});
