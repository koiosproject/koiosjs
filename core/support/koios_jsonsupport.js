/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koios_jsonsupport.js
Provides koios.JSONSupport - enabling basic JSON support in
Koios objects
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module*/

/**
A support object which provides toObject(), toString() and log() methods
to the target object

@class koios.JSONSupport
@extends koios.Mixin
*/

koios.define({
    className: "koios.JSONSupport",
    extend: "koios.Mixin",
    ignoreAlias: true,

    mixin: function (theConstructor, thePrototype) {
        if (!thePrototype.$stringable) {
            thePrototype.$stringable = [];
        }
    },
    
    mixinProperties: {
/**
Turns all this.$stringable elements to a key-value object

@method toObject
@returns Object
*/
        
        toObject: function () {
            var _stringable = this.$stringable,
                ret = {},
                key,
                i,
                l = koios.isArray(_stringable) ? _stringable.length : 0;
            
            if (koios.isFunction(_stringable)) {
                _stringable = this.$stringable();
            }

            for (i = 0; i < l; i += 1) {
                key = _stringable[i];
                if (this.hasOwnProperty(key)) {
                    ret[key] = this[key];
                } else if (this.get) {
                    ret[key] = this.get(key);
                } else {
                    ret[key] = null;
                }
            }
            
            return ret;
        },

/**
Turns the result of this.toObject() to a JSON stringified string

@method toString
@returns String
*/

        toString: function () {
            return koios.JSON.stringify(this.toObject());
        },

/**
Logs the result of toString()

@method log
*/

        log: function () {
            koios.log(this.toString());
        }
    }
});
