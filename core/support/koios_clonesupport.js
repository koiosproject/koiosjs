/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koios_clonesupport.js
Provides koios.CloneSupport - A support object which provides cloneability
for KoiosJS objects.
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module*/

/**
A support object which provides cloneability for KoiosJS objects.

@class koios.CloneSupport
@extends koios.Mixin
*/

koios.define({
    className: "koios.CloneSupport",
    extend: "koios.Mixin",
    ignoreAlias: true,
    
    mixin: function (theConstructor, thePrototype) {
        if (!thePrototype.$clonable) {
            thePrototype.$clonable = [];
        }
    },
    
    mixinProperties: {
/**
.clone() returns a copy of a KoiosJS object. It queries for this.$clonable to return an array of keys
that need to be cloned

__Inherited from koios.CloneSupport__

@method clone
@returns Object
*/
        
        clone: function () {
            var obj = {},
                t = this,
                className = obj.className || this.className,
                Ctor = koios.getConstructor(className),
                clonable = (koios.isFunction(this.$clonable)) ? this.$clonable() : this.$clonable;
            
            koios.each(clonable, function (theItem) {
                obj[theItem] = koios.clone(t[theItem]);
            });
            
            if (!koios.isEmpty(Ctor)) {
                return new Ctor();
            }
        }
    }
});
            