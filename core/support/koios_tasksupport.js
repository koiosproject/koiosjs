/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koios_tasksupport.js
Provides koios.TaskSupport - enables task management and execution
within KoiosJS objects
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module*/

/**
An abstract support object which enables enables task management and execution
within KoiosJS objects

@class koios.TaskSupport
@extends koios.Mixin
*/

/**
@module core
*/

function _taskParams(task, args) {
    var firstArg = args[0],
        _args = [];
    
    if (task.params && koios.isArray(task.params) && koios.isObject(firstArg)) {
        koios.each(task.params, function (theParam) {
            if (koios.isString(theParam)) {
                _args.push(koios.template(theParam, firstArg));
            } else {
                _args.push(theParam);
            }
        });
        
        _args = _args.concat(Array.prototype.slice.call(args, 1));
        return _args;
    } else {
        return args;
    }
}

koios.define({
    className: "koios.TaskSupport",
    extend: "koios.Mixin",
    ignoreAlias: true,
    
    mixin: function (theConstructor, thePrototype) {
        if (!thePrototype.tasks) {
            thePrototype.tasks = [];
        }
        
        thePrototype._initFn.push(this._setTasks);
    },
    
    _setTasks: function () {
        var tasks = [];
        
        koios.each(this.tasks, function (theTask) {
            theTask.owner = this;
            var newTask = koios.task(theTask);
            tasks.push(newTask);
        });
        
        this._tasks = tasks;
    },
    
    mixinProperties: {

// =================================================

/**
Returns a task with name taskName

__Inherited from koios.TaskSupport__

@method getTask
@param {String} taskName - the name of the task to return
@returns koios.Task
*/
        
        getTask: function (taskName) {
            var tasks = koios.array.fitler(this._tasks, function (theTask) {
                return (theTask.name === taskName);
            });
            
            if (tasks.length > 0) {
                return tasks[0].clone();
            } else {
                return null;
            }
        },
        
/**
.task() is an overloaded function. It can

Take a task by name and execute it:

    this.task("taskName");
    
Take a function, make a task out of it, and execute it:

    this.task(this.bind("fnName"));
    
or take a koios.Task and execute it:

    this.task(task);
    
__Inherited from koios.TaskSupport__

@method task
@returns koios.Task
*/
        
        task: function (theTask) {
            if (koios.isString(theTask)) {
                theTask = this.getTask(theTask);
            } else if (koios.isFunction(theTask)) {
                theTask = koios.task(theTask);
            } else if (!koios.isaSubclassOf(theTask, "koios.Task")) {
                return;
            }
            
            theTask.owner = this;
            
            var _args = Array.prototype.slice.call(arguments, 1),
                args = _taskParams(theTask, _args);
            
            theTask.enqueue.apply(theTask, args);
            return theTask;
        }
    }
});