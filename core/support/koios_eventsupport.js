/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koios_eventsupport.js
Provides koios.EventSupport - A support object which event 
handling and emitting for Koios objects
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module*/

/**
A support object which event handling and emitting for Koios objects

@class koios.EventSupport
@extends koios.Mixin
*/

koios.define({
    className: "koios.EventSupport",
    extend: "koios.Mixin",

    mixin: function (theConstructor, thePrototype) {
        if (!koios.contains(thePrototype.accumulated, "events")) {
            thePrototype.accumulated.push("events");
        }
        
        thePrototype._initFn.push(this._setEmitter);
        thePrototype._initFn.push(this._setEvents);
    },
    
    _setEmitter: function () {
/**
A koios.Emitter object that handles all events and messages pertaining to a given
object

This should be considered private, but is documented in order to make it accesible
to developers.

__Inherited from koios.EventSupport__

@attribute _emitter
@type koios.Emitter
*/
        
        this._emitter = koios.emitter();
        this._emitter._extend(this);
        
    },
        
    _setEvents: function () {
        var t = this;
        
        koios.each(this.events, function (key, value) {
            t.on(key, function (ev) {
                return t._bubble(ev, value);
            });
        });
    },
    
    mixinProperties: {

        bubble: function (eventName, theEvent) {
            if (!this.emit(eventName, theEvent)) {
                var owner = this.get("owner");
                if (owner) {
                    return owner.bubble(eventName, theEvent);
                }
            } else {
                return true;
            }
        },
        
        _bubble: function (theEvent, targetFn) {
            if (this[targetFn]) {
                return this[targetFn].call(this, theEvent);
            }
            
            var owner = this.get("owner");
            
            if (owner) {
                return owner._bubble(theEvent, targetFn);
            }
        },
/**
Attaches a listener function to an event. See koios.on() for more information.

__Example__

    var listener = this.on("property:change", function (newVal, oldVal) {
        // Do something when this.set("property", ...) is called
    });

__Inherited from koios.EventSupport__

@method on
@param {String} eventName - the name of the event to listen
@param {Function} eventListener - the listener function
@returns Listener
*/
    
/**
Removes a listener from an event. If a string is passed as the first parameter (without a second
parameter), all listeners for a given event will be removed. If a Listener object is passed in,
that specific listener will be removed.

__Example__

    this.off("property:change");
    this.off(listener);

__Inherited from koios.EventSupport__

@method off
@param {String or Listener} theListener - either a string referencing an event name, or a listener
object returned by on()
*/

/**
Attaches a listener function to an event that will only be executed once

__Example__

    this.once("property:change", function (newVal, oldVal) {
        // Do something when this.set("property", ...) is called
    });

__Inherited from koios.EventSupport__

@method once
@param {String} eventName - the name of the event to listen
@param {Function} eventListener - the listener function
@returns Listener
*/

/**
Emits an event and notifies all listeners for that event. Any parameters beyond the
eventName will be passed along to all event listeners.

__Example__

    this.emit("eventName", parameters);
    
__Inherited from koios.EventSupport__

@method emit
@param {String} eventName - the name of the event to emit.
*/
   
/**
Emits an event with name eventName both via an event listenable via this.on() as well as koios.on(). The global
event follows the following naming convention

    [class name]:[event]
    
So every time you destroy a koios.Base, the following event is emitted globally

    koios.Base:destroy
    
This same event is emitted every time a koios.Base is destroyed. If you want to watch a specific instance's destroy message, 
monitor it via it's own .on() method. Avoid using emitGlobally as much as possible as it causes additional potentially
detrimental performance implications

__Inherited from koios.EventSupport__

@method emitGlobally
@param {String} eventName - the name of the event
*/

        emitGlobally: function (evName) {
            var globEv = this.className + ":" + evName,
                args = Array.prototype.splice.call(arguments, 1),
                globArgs = [globEv].concat(args),
                locArgs = [evName].concat(args);
            
            koios.emit.apply(koios, globArgs);
            this.emit.apply(this, locArgs);
        }
    }

    
});
