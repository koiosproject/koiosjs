/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koios_class.js
Provides koios.Class
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module*/

// =================================================

/**
Mixins (usually called [something]Support in Koios's naming, allows for the inclusion
of features without additional subclassing. They are never instantiated, only their
mixin function and mixinProperties are ever used.

    
    koios.define({
        className:"koios.JSONSupport",
        extend:"koios.Mixin",
        ignoreAlias: true,
        
        mixin: function (theConstructor, thePrototype) {
        },
        
        mixinProperties: {
            toObject: function() {
                var _stringable = this._stringable;
                if (koios.isFunction(_stringable)) {
                    _stringable = this._stringable();
                }

                var ret = {};
                for (var i=0;i<_stringable.length;i++) {
                    var key = _stringable[i];
                    if (key in this) {
                        ret[key] = this[key];
                    } else if (this.get) {
                        ret[key] = this.get(key);
                    } else {
                        ret[key] = null;
                    }
                }
                return ret;
            },

            toString: function() {
                return koios.JSON.stringify(this.toObject());
            },

            log: function() {
                koios.log(this.toString());
            }
        },
    });

@class koios.Mixin
@extends koios.Class
*/

koios.define({
    className: "koios.Mixin",
    extend: "koios.Class",
    ignoreAlias: true,
    
    mixin: function (theConstructor, thePrototype) {
    },
    
    mixinProperties: {}
});