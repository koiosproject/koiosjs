/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koios_class.js
Provides koios.Class
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module*/

// =================================================

/**
The lowest base class used by the KoiosJS OO system. You shouldn't have any need to
subclass this directly (koios.Object is a more likely target for low level objects), but it
is available should you choose to.

@class koios.Class
*/

koios.define({
    className: "koios.Class",
    accumulated: ["_initFn"],
    ignoreAlias: true,

    _bindable: true,
    _destroyed: false,
    _constructor: "_ct",
    _initFn: [],
    
    $constructor: function (args) {
        this.__id = koios._uid();
        this._destroyed = false;
        koios.mixin(this, args);
    },
    
    $init: function () {
        var t = this;
        
        if (!this.name) {
            this._generateObjectName();
        }
        
        koios.each(this._initFn, function (theFn) {
            theFn.apply(t);
        });
    },
    
// =================================================
// Private Decorators
    
    _generateObjectName: function () {
        var className = this.className || this.prototype.className,
            tempName = className + "_" + this.__id;
        
        this.name = tempName.replace(".", "");
    },

// =================================================
// Function Handling

/**
Calls a function by theFn (or a function itself), with this as it's scope. All arguments
after the first argument will be passed along to the function

__Example__

    this.call("someFn", 1, 2, 3);
    
@method call
@param {String or Function} theFn - the function reference or function to call
@returns Value (optional)
*/
    
    call: function (fnName) {
        var args = Array.prototype.splice.call(arguments, 1);
        
        if (koios.isFunction(fnName)) {
            return fnName.apply(this, args);
        } else {
            if (this[fnName]) {
                return this[fnName].apply(this, args);
            } else {
                if (koios.config("warnEmptyCalls")) {
                    koios.warn(this.className + ".call() warning: method " + fnName + " does not exist");
                }
            }
        }
    },
    
/**
Safely binds a function to a given koios.Class-derived object, as long as the class
is bindable and not yet destroyed. Like koios.bind, you can pass a function or the name
of a function owned by the object as the lone parameter

@method bind
@param {Function or String} theFn - the function (or name) to bind
@returns Function
*/
    
    bind: function (theFn) {
        if (!this._destroyed && this._bindable) {
            return koios.bind(this, theFn);
        } else {
            return null;
        }
    },

// =================================================
// Subclass checking
  
/**
Returns whether or not this.className is equal to the class represented either by a string 
representation of the constructor, or the constructor itself.

__Example__

    var foo = new koios.Object();
    foo.isa(koios.Object);          // true
    foo.isa("koios.Object");        // true
    foo.isa("koios.Class");         // false
    
@method isa
@param {Function or String} className - the class name or constructor to check against
@returns BOOL
*/
    
    isa: function (className) {
        if (koios.isString(className)) {
            return this.className.toLowerCase() === className.toLowerCase();
        } else {
            try {
                return this.className.toLowerCase() === className.className.toLowerCase();
            } catch (error) {
                return false;
            }
        }
    },

/**
Returns whether or not this is a subclass of className, represented either by a string
representation of the constructor, or the constructor itself.

__Example__

    var foo = new koios.Object();
    foo.isaSubclassOf(koios.Object);          // true
    foo.isaSubclassOf("koios.Object");        // true
    foo.isaSubclassOf("koios.Class");         // true

@method isaSubclassOf
@param {Function or String} className - the class name or constructor to check against
@returns BOOL
*/
    
    isaSubclassOf: function (className) {
        if (koios.isString(className)) {
            var ctor = koios.getConstructor(className);
            if (!koios.isEmpty(ctor)) {
                return this instanceof ctor;
            }
        } else {
            return this instanceof className;
        }
    },

// =================================================
// Garbage Collection
   
/**
Sets an object up to be ready for garbage collection

@method destroy
*/
    
    destroy: function () {
        this._destroyed = true;
        this._bindable = false;
    }
});
    