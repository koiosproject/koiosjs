/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koios_object.js
Provides utility functions for object manipulation in KoiosJS (not to be
confused with koios.Object - a low level object in the OO stack
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module*/

// =================================================

/**
A set of Object manipulation methods for KoiosJS - not to be confused with
koios.Object - a low level object to be subclassed as part of the koios.define()
OO chain

@class koios.object
*/

koios.object = {
    
/**
Returns whether or not theObject contains theKey

__Example__

    koios.object.containsKey({koios:"rocks"}, "koios");     // true
    koios.object.containsKey({koios:"rocks"}, "rocks");     // false
    
@method containsKey
@param {Object} theObject - the object to check
@param {String} theKey - the key to check
@returns BOOL
*/
    containsKey: function (theObject, theKey) {
        return theObject.hasOwnProperty(theKey);
    },

/** 
Returns whether or not theObject has a key whose value is theValue

__Example__

    koios.object.containsValue({koios:"rocks"}, "koios");     // false
    koios.object.containsValue({koios:"rocks"}, "rocks");     // true

@method containsValue
@param {Object} theObject - the object to check
@param {Value} theValue - the value to look for
@returns BOOL
*/
    
    containsValue: function (theObject, theValue) {
        var values = this.values(theObject);
        return koios.indexOf(values, theValue) > -1;
    },

/**
Iterates through theObject, passing the key, value, index and theObject into theCallback

__Example__

    koios.object.each({koios: true, rocks: false}, function (key, value, index, object) {
        console.log(key);
    });
    
    // Would log "koios", followed by "rocks"

@method each
@param {Object} theObject - the object to iterate through
@param {Function} theCallback - the method to be invoked with each key-value combo
*/
    
    each: function (theObject, theCallback) {
        var keys = this.keys(theObject),
            i,
            l = keys.length,
            key;
        
        for (i = 0; i < l; i += 1) {
            key = keys[i];
            theCallback(key, theObject[key], i, theObject);
        }
    },

/**
Returns whether or not theObject equals otherObject. An object is considered equal to another object
if it contains the same keys, and the keys are equal to the same values.

__Example__

    koios.object.equals({a:true, b:false}, {a:true, b:false});          // true
    koios.object.equals({a:true, b:false}, {a:true, b:true});           // false
    koios.object.equals({a:true, b:false}, {a:true, b:false, c:true});  // false
    
@method equals
@param {Object} theObject - the first object to check
@param {Object} otherObject - the object to check theObject against.
@returns BOOL
*/
    
    equals: function (theObject, otherObject) {
        if (theObject === otherObject) {
            return true;
        }
        
        if (!theObject || !otherObject) {
            return false;
        }
        
        if (this.keys(theObject).length !== this.keys(otherObject).length) {
            return false;
        }
        
        var key;
        
        for (key in theObject) {
            if (theObject.hasOwnProperty(key)) {
                if (theObject[key] !== otherObject[key]) {
                    return false;
                }
            }
        }
        
        return true;
    },
    
/**
Returns either an array of all functions within an object, or an array of all the lookup keys relating to the
functions.

@method functions
@param {Object} theObject - the object to lookup
@param {BOOL} asFunctions - whether or not to return an array of functions, or just the keys themselves.
@returns Array
*/
    
    functions: function (theObject, asFunctions) {
        var keys, values;
        
        if (asFunctions) {
            values = this.values(theObject);
            return koios.array.filter(values, koios.isFunction);
        } else {
            keys = this.keys(theObject);
            return koios.array.filter(keys, function (theKey) {
                return koios.isFunction(theObject[theKey]);
            });
        }
    },
    
/**
Inverts the key-value pairs of an object and returns it.

__Example__

    koios.object.invert({a:"b"});           // {b:"a"}
    
@method invert
@param {Object} theObject - the object to invert
@returns Object
*/
    
    invert: function (theObject) {
        var ret = {};
        this.each(theObject, function (key, value, idx, obj) {
            ret[value] = key;
        });
        
        return ret;
    },
    
/**
Returns an array of all keys in an object

__Example__

    koios.object.keys({koios:true, rocks:true});        // ["koios", "rocks"];

@method keys
@param {Object} theObject - the object to return the keys from
@returns Array
*/
    
    keys: function (theObject) {
        var keys = [],
            key;
        
        for (key in theObject) {
            if (theObject.hasOwnProperty(key)) {
                keys.push(key);
            }
        }
        
        return keys;
    },
    
/**
Returns the number of key value pairs in an object

__Example__

    koios.object.length({koios:true, rocks:true});        // 2

@method length
@param {Object} theObject - the object to return the length of
@returns Number
*/
    
    length: function (theObject) {
        return this.keys(theObject).length;
    },
    
/**
Returns an array of all values returned by calling theCallback with theKey, theValue, theIndex and theObject passed
as parameters

__Example__

    koios.object.map({koios:true, rocks:true}, function (key, value, index, object) {
        return key;
    });
    
    // ["koios", "rocks"];

@method map
@param {Object} theObject - the object to query
@param {Function} theCallback - the method to invoke with each key value pair
@returns Array
*/
    
    map: function (theObject, theCallback) {
        var ret = [];
        
        this.each(theObject, function (key, value, idx, obj) {
            ret.push(theCallback(key, value, idx, obj));
        });
            
        return ret;
    },
    
/**
Mixes otherObject into theObject. This is an alternate mapping of koios.mixin(theObject, otherObject)

@method mixin
@param {Object} theObject - the object to mix into
@param {Object} otherObject - the object to mix into theObject
@returns Object
*/
    
    mixin: function (theObject, otherObject) {
        return koios.mixin(theObject, otherObject);
    },
    
/**
Returns an array of key-value pairs, either as an array (asObjects = false) or as objects (asObjects = true)

__Example__

    koios.object.pairs({koios:true, rocks:true});           // [["koios", true], ["rocks", true]]
    koios.object.pairs({koios:true, rocks:true}, true);     // [{"koios": true}, {"rocks": true}]
    
@method pairs
@param {Object} theObject - the object to query
@param {BOOL} asObjects - whether or not to return the array containing objects (default is false)
@returns Array
*/
    
    pairs: function (theObject, asObjects) {
        var ret = [];
        
        this.each(theObject, function (key, value, idx, object) {
            if (asObjects) {
                var obj = {};
                obj[key] = value;
                ret.push(obj);
            } else {
                ret.push([key, value]);
            }
        });
        
        return ret;
    },
    
/**
Returns an object containing only key-value pairs that don't pass theCallback

__Example__

    koios.object.reject({koios:true, rocks:false}, function (key, value, index, object) {
        return value;
    });
    
    // {rocks: false}
    
@method reject
@param {Object} theObject - the object to query
@param {Function} theCallback - the method to invoke with each key value pair
@returns Object
*/
    
    reject: function (theObject, theCallback) {
        var ret = {};
        
        this.each(theObject, function (key, value, idx, obj) {
            if (!theCallback(key, value, idx, obj)) {
                ret[key] = value;
            }
        });
        
        return ret;
    },
    
/**
Turns theObject into a String. By default this is the same as calling JSON.stringify(theObject), but if you pass
in an optional 2nd parameter as it will allow you to format to other options.

Setting theStyle to "css" will return an object in a definition that looks like a css definition:

    koios.object.toString({color:"green", width:"100px"}, "css");
    // -> color:green; width:100px

@method toString
@param {Object} theObject - the object to turn into a string.
@param {String} theStyle - the style of stringification you're looking for. Default is "json", options include "json" & "css"
@returns String
*/
    
    toString: function (theObject, theStyle) {
        theStyle = theStyle.toLowerCase();
        if (!theStyle || theStyle === "json") {
            return koios.JSON.stringify(theObject);
        }
        
        var ret = "";

        if (theStyle === "css") {
            koios.each(theObject, function (theKey, theValue) {
                ret += theKey + ":" + theValue + "; ";
            });
        }
        
        return koios.string.trimWhitespace(ret);
    },

/**
Returns just the values of the object as an array

__Example__

    koios.object.values({koios:true, rocks:true});      // [true, true]
    
@method values
@param {Object} theObject - the object to query
@returns Array
*/
    
    values: function (theObject) {
        var ret = [];
        
        this.each(theObject, function (key, value, idx, obj) {
            ret.push(value);
        });
        
        return ret;
    }
};