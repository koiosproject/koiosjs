/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koios_validate.js
Provides utility functions for validation in KoiosJS
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module*/

var _objToString = function (obj) {
    return Object.prototype.toString.call(obj);
};

// =================================================
// Type Checking

/**
@class koios
*/

/**
Returns whether or not theFunction is a function

@method isFunction
@param {Function} theFunction - the object to check
@returns BOOL
*/

koios.isFunction = function (theFunction) {
    return typeof theFunction === "function";
};

/**
Returns whether or not theObject is an object

@method isObject
@param {Object} theObject - the object to check
@returns BOOL
*/

koios.isObject = function (theObject) {
    return typeof theObject === "object" && _objToString(theObject) !== "[object Array]";
};

/**
Returns whether or not theObject is an array

@method isArray
@param {Object} theObject - the object to check
@returns BOOL
*/

koios.isArray = function (theObject) {
    return _objToString(theObject) === "[object Array]";
};

/**
Returns whether or not theObject is a number

@method isNumber
@param {Object} theObject - the object to check
@returns BOOL
*/

koios.isNumber = function (theObject) {
    return _objToString(theObject) === "[object Number]" && !koios.isNaN(theObject);
};

/**
Returns whether or not theObject is a string

@method isString
@param {Object} theObject - the object to check
@returns BOOL
*/

koios.isString = function (theObject) {
    return _objToString(theObject) === "[object String]";
};

/**
Returns whether or not theObject is a date

@method isDate
@param {Object} theObject - the object to check
@returns BOOL
*/

koios.isDate = function (theObject) {
    return _objToString(theObject) === "[object Date]";
};

/**
Returns whether or not theObject is a arguments object

@method isArguments
@param {Object} theObject - the object to check
@returns BOOL
*/

koios.isArguments = function (theObject) {
    return _objToString(theObject) === "[object Arguments]";
};

/**
Returns whether or not theObject is a Boolean

@method isBoolean
@param {Object} theObject - the object to check
@returns BOOL
*/

koios.isBoolean = function (theObject) {
    return _objToString(theObject) === "[object Boolean]";
};

/**
Returns whether or not theEvent is an Event

@method isEvent
@param {Object} theObject - the object to check
@returns BOOL
*/

koios.isEvent = function (theEvent) {
    var s = theEvent.toString(),
        ptr = /\[object (Keyboard|Mouse|Focus|Wheel|Composition|Storage)?Event[s]?\]/;

    return ptr.test(s);
};

/**
Returns whether or not theObject is a Regex

@method isRegex
@param {Object} theObject - the object to check
@returns BOOL
*/

koios.isRegex = function (theObject) {
    return theObject instanceof RegExp;
};

// =================================================
// Existence Checking

/**
Returns whether or not theObject isn't undefined

@method exists
@param {Object} theObject - the object to check
@returns BOOL
*/

koios.exists = function (theObject) {
    return typeof theObject !== "undefined";
};

/**
Returns whether or not theObject is NaN

@method isNaN
@param {Object} theObject - the object to check
@returns BOOL
*/

koios.isNaN = function (theObject) {
    return theObject !== theObject;
};

/**
Returns whether or not theObject is empty

@method isEmpty
@param {Object} theObject - the object to check
@returns BOOL
*/

koios.isEmpty = function (theObject) {
    if (!theObject) {
        return true;
    } else if (koios.isString(theObject) || koios.isArguments(theObject) || koios.isArray(theObject)) {
        return theObject.length === 0;
    } else if (koios.isNumber(theObject)) {
        return theObject === 0;
    } else if (koios.isObject(theObject)) {
        var keys = koios.object.keys(theObject);
        return keys.length === 0;
    } else {
        return false;
    }
};

/**
Returns whether or not theObject is false-ish (0, false, undefined, etc...)

@method isFalseish
@param {Object} theObject - the object to check
@returns BOOL
*/

koios.isFalseish =  function (theObject) {
    return (!theObject) ? true : false;
};

// =================================================
// Koios OO type checking

/**
Returns whether or not theObject is a koios.Class derived object, and is a type of theClass

@method isa
@param {Object} theObject - the object to check
@param {String or Object} theClass - the class to check against
@returns BOOL
*/

koios.isa = function (theObject, theClass) {
    return (theObject && theObject.isa && theObject.isa(theClass));
};

/**
Returns whether or not theObject is a koios.Class derived object, and is a subclass of theClass

@method isaSubclassOf
@param {Object} theObject - the object to check
@param {String or Object} theClass - the class to check against
@returns BOOL
*/
   
koios.isaSubclassOf =  function (theObject, theClass) {
    return (theObject && theObject.isaSubclassOf && theObject.isaSubclassOf(theClass));
};
