/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koios_string.js
Provides utility functions for string manipulation in KoiosJS
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module*/

// =================================================

koios.stringTemplates = {
    handlebars: /\{{([^{}]*)}}/g,
    "es6": /\$\{([^{}]*)\}/g,
};

function _quickTemplate (templateString, replacements) {
    var fn = function (theMacro, theName) {
        var res = theMacro;
        if (replacements.hasOwnProperty(theName)) {
            res = replacements[theName];
        }

        if (res !== theMacro) {
            return res;
        } else {
            return theMacro;
        }
    },
        result = templateString,
        rx = koios.stringTemplates[koios.config("templateStyle")];

    return result.replace(rx, fn);
};

/**
A set of String manipulation methods for KoiosJS
@class koios.string
*/

koios.string = {
    
/**
Returns a string camelCased, using divider to generate separate words

__Example__

    koios.string.camelCase("koios-rocks", "-");     // koiosRocks
    
@method camelCase
@param {String} theString - theString to camelCase
@param {String} divider - the string to use as a divider between words (default is -)
@returns String
*/
    
    camelCase: function (theString, divider) {
        if (!divider) {
            divider = "-";
        }
        
        var s = this.trimWhitespace(theString),
            first = true,
            p;
        
        if (s.indexOf(divider) === -1) {
            return s;
        }
        
        p = koios.array.map(s.split(divider), function (theItem) {
            if (!first) {
                return koios.string.cap(theItem);
            } else {
                first = false;
                return theItem;
            }
        });
        
        return p.join("");
    },
    
/**
Returns a string whose first character has been capitalized

@method cap
@param {String} theString - the string to capitalize
@returns String
*/

    cap: function (theString) {
        return theString.slice(0, 1).toUpperCase() + theString.slice(1);
    },
    
/**
Returns whether or not theString contains otherString - either case sensitively or otherwise.

__Example__

    koios.string.contains("koios rocks", "koios");          // true
    koios.string.contains("koios rocks", "Koios");          // false
    koios.string.contains("koios rocks", "Koios", true);    // true
    
@method contains
@param {String} theString - the string to look through
@param {String} otherString - the substring to look for.
@param {BOOL} ignoreCase - whether or not to search case sensitively
@returns BOOL
*/
    
    contains: function (theString, otherString, ignoreCase) {
        if (ignoreCase) {
            return theString.toLowerCase().indexOf(otherString.toLowerCase()) > -1;
        } else {
            return theString.indexOf(otherString) > -1;
        }
    },
    
/**
Returns a string with template patters replaced by their respective values within
theObject.

__Example__

    koios.string.template("${library} rocks!", {library: "koios"});     // koios
    
@method template
@param {String} theString - the string to template
@param {Object} theObject - the object to take the values from
@param {BOOL} useDeep - whether or not to deep template the string (optional)
@returns String
*/
    
    template: function (theString, theObject, useDeep) {
        if (useDeep) {
            koios["throw"]("deepTemplating not yet implemented");
        } else {
            return _quickTemplate(theString, theObject);
        }
    },
    
/**
Converts a string to an object. By default this is the same as JSON.parse(), but if you pass
in an optional type parameter, you can parse this differently. Values for theType are "css" and "json"

@method toObject
@param {String} theString - the string to convert
@param {String} theType - the type of string to convert (optional - options include "css" and "json")
@returns Object
*/
    
    toObject: function (theString, theType) {
        if (!theType || theType === "json") {
            return koios.JSON.parse(theString);
        } else if (theType === "css") {
            var ret = {};
            
            koios.each(theString.split(";"), function (theItem) {
                var pairs = theItem.split(":"),
                    key,
                    value;

                if (pairs.length > 1) {
                    key = koios.string.trimWhitespace(pairs[0]);
                    value = koios.string.trimWhitespace(pairs[1]);
                    ret[key] = value;
                }
            });
            
            return ret;
        }
    },

/**
Trims the whitespace from the beginning and ending of theString

__Example__

    koios.string.trimWhitespace("  koios  ");   // "koios"
    
@method trimWhitespace
@param {String} theString - the string to trim
@returns String
*/
    
    trimWhitespace: function (theString) {
        function lTrim(value) {
            var re = /\s*((\S+\s*)*)/;
            return value.replace(re, "$1");
        }
        
        function rTrim(value) {
            var re = /((\s*\S+)*)\s*/;
            return value.replace(re, "$1");
        }
        
        var r = rTrim(theString),
            l = lTrim(r);
        return l;
    },
    
/**
Returns a string whose first character has been uncapitalized

@method uncap
@param {String} theString - the string to uncapitalize
@returns String
*/

    uncap: function (theString) {
        return theString.slice(0, 1).toLowerCase() + theString.slice(1);
    }
};
