/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koios_config.js
Provides a simple configuration setter for KoiosJS
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module, process, setImmediate*/

/**
@class koios
*/

var _configDefaults = {
	logLevel: 5,  // koios.Logger.LOG
	showWindowAlerts: true,
	warnEmptyCalls: false,
	warnFrozenProperties: true,
	templateStyle: "es6"
};

koios.ENV.config = {};

/**
TODO: Document

@method config
*/

koios.config = function (paramName, paramValue) {
    if (arguments.length === 0) {
        return koios.ENV.config;
    }

    if (arguments.length === 1) {
        if (koios.isString(paramName)) {
            return koios.ENV.config[paramName];
        } else if (koios.isObject(paramName)) {
            koios.mixin(koios.ENV.config, paramName);
            return koios.ENV.config;
        }

        return koios.warn("koios.config() error: Unknown paramName type");
    }

    koios.ENV.config[paramName] = paramValue;
    return paramValue;
};

koios._version = {
    "koios": "0.0.1"
};

/**
TODO: Document

@method version
*/

koios.version = function () {
    return koios._version.koios;
};

koios.config(_configDefaults);