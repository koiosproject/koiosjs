/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koios_functions.js
Provides utility functions for function manipulation in KoiosJS
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module, process, setImmediate*/

/**
@class koios
*/

var _queue = {},
    _tos = {};

/**
Bind provides an extension to the standard Function.prototype.bind method, which creates a new
method whose this keyword is set to theScope.

@method bind
@param {Object} theScope - the scope to bind this to
@param {Function or String} theMethod - either a function to bind, or the name of the function owned
by theScope to bind to
@returns Function
*/

koios.bind = function (theScope, theMethod) {
    var args = [theScope].concat(Array.prototype.slice.call(arguments, 2));

    if (arguments.length === 1) {
        theMethod = theScope;
        theScope = this;
    }

    if (koios.isString(theMethod)) {
        theMethod = theScope[theMethod];
    }

    return Function.prototype.bind.apply(theMethod, args);
};

/**
Clears a timeout referenced by theName set by koios.timeout()

@method clearTimeout
@param {String} theName - the reference name set in koios.timeout();
*/

koios.clearTimeout = function (theName) {
    if (_tos[theName]) {
        clearTimeout(_tos[theName]);
        _tos[theName] = null;
    }
};

/**
Executes theCallback once the current execution stack has completed - ideal for pushing the execution
off until the browser is ready to handle it

@method defer
@param {Function} theCallback - the function to defer
*/

koios.defer = function (theCallback) {
    if (typeof process === "object" && process.nextTick) {
        process.nextTick(theCallback);
    } else if (typeof setImmediate === "function") {
        setImmediate(theCallback);
    } else {
        setTimeout(theCallback, 0);
    }
};

/**
Marks a method as deprecated, providing a warning via the console to a better
replacement, but invokes theFn anyway.

__Example__

    var deprecatedFn = koios.deprecated(function() {
        // Do something deprecated...
    }, "deprecatedFn", "notDeprecatedFn");
    
@method deprecated
@param {Function} theFn - the function to invoke that has been deprecated
@param {String} fnName - the name of theFn which has been deprecated
@param {String} replacement - the name of the function to invoke instead
@returns Function
*/

koios.deprecated = function (theFn, fnName, replacement) {
    return function () {
        koios.warn(fnName + " is deprecated - use " + replacement + "instead.");
        theFn.apply(this, arguments);
    };
};

/**
Returns a function which caches return values for a given input value

__Example__

    var timesTwo = koios.memoize(function(val) {
        return val * 2;
    });
    
    timesTwo(2);        // calls the function, returns 4
    timesTwo(2);        // doesn't call the function, returns 4
    
@method memoize
@param {Function} theFn - the function to memoize
@returns Function
*/

koios.memoize = function (theFn) {
    if (!theFn.memoize) {
        theFn.memoize = {};
    }
    
    return function () {
        var args = Array.prototype.slice.call(arguments),
            hash = "",
            i = args.length,
            currentArg = null;

        while (i > -1) {
            currentArg = args[i];
            hash += (currentArg === {}(currentArg)) ? JSON.stringify(currentArg) : currentArg;
            i -= 1;
        }

        return (theFn.memoize.hasOwnProperty(hash)) ? theFn.memoize[hash] : theFn.memoize[hash] = theFn.apply(this, args);
    };
};

/**
koios.once() is an overloaded function. If given just a function, it will return a function which can only be called once, but will return the same value each time.

__Example__

    var onceFn = koios.once(fnThatTakesALongTime);
    
    onceFn();       // Executes fnThatTakesALongTime
    onceFn();       // Doesn't execute fnThatTakesALongTime

If you give two parameters - a string event name, and a function, it is the same as calling koios.MessageEmitter.once - it allows for a
single time listener for a Koios event.

__Example__

    var listener = koios.once("koios.Model:create", function () {
        // This will only be caused once when a koios.Model is created.
    });
    
@method once
@param {Function or String} theFn - the function to only call once, or the name of an event to listen for
@param {Function} theListener - a function listener if passed along with a string for the first parameter (optional)
@returns Function or Object
*/

koios.once = function (theFn, theListener) {
    var exec = false,
        res = null;
    
    if (koios.isFunction(theFn)) {
        return function () {
            if (exec) {
                return res;
            }

            exec = true;
            res = theFn.apply(this, arguments);
            theFn = null;
            return res;
        };
    } else {
        koios.messageEmitter.once(theFn, theListener);
    }
};

/**
Stops any execution of a callback with name theQueueName, the same name used with koios.wait() or koios.throttle()

@method stop
@param {String} theQueueName - the name of the execution queue to stop
*/

koios.stop = function (theQueueName) {
    if (_queue[theQueueName]) {
        clearTimeout(_queue[theQueueName]);
        _queue[theQueueName] = null;
    }
};

/**
Executes theCallback once, but stops all calls with the same theQueueName until theTimeout has ended.

@method throttle
@param {String} theQueueName - the name of the execution queue
@param {Function} theCallback - the method to invoke
@param {Number} theTimeout - the number of milliseconds to wait until theQueueName can be executed again.
*/

koios.throttle = function (theQueueName, theCallback, theTimeout) {
    if (_queue[theQueueName]) {
        return;
    }
    
    theCallback();
    
    _queue[theQueueName] = setTimeout(function () {
        koios.stop(theQueueName);
    });
};

/**
Sets a timeout memoized to theName, calling theCallback after theTimeout milliseconds. You can always
stop this timeout by calling koios.clearTimeout(theName);

@method timeout
@param {String} theName - a lookup name for the timeout
@param {Function} theCallback - the method to invoke
@param {Number} theTimeout - the number of milliseconds to wait until invoking theCallback
*/

koios.timeout = function (theName, theCallback, theTimeout) {
    _tos[theName] = setTimeout(function () {
        theCallback();
        koios.clearTimeout(theName);
    }, theTimeout);
};

/**
Stops all execution of theQueueName, including theCallback until theTimeout milliseconds have
occured.

@method wait
@param {String} theQueueName - the name of the wait queue.
@param {Function} theCallback - the function to execute after theTimeout
@param {Number} theTimeout - the number of milliseconds to wait until calling theCallback
*/

koios.wait = function (theQueueName, theCallback, theTimeout) {
    koios.stop(theQueueName);
    _queue[theQueueName] = setTimeout(function () {
        koios.stop(theQueueName);
        theCallback();
    }, theTimeout);
};

/**
Returns a function which invokes the second function, passing the first in as a parameter allowing
for safe overriding.

__Example__

    var fn = function () {
        console.log("second");
    }
    
    var wrapped = koios.wrap(fn, function(theFn) { 
        console.log("first");
        theFn()
    });
    
    // Will log "first" then "second"

@method wrap
@param {Function} theFn - the function to pass into theCallback
@param {Function} theCallback - the first function to invoke
@returns Function
*/

koios.wrap = function (theFn, theCallback) {
    return function () {
        var args = [theFn].concat(koios.toArray(arguments));
        theCallback.apply(this, args);
    };
};
