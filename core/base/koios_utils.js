/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koios_utils.js
Provides general utility functions for KoiosJS
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module*/

/**
@class koios
*/

var em = {},
    global = this;

/**
Attempts to make a shallow clone of any object passed into it

@method clone
@param {Object} theObject - the object to clone
@returns Object
*/

koios.clone = function (theObject) {
    var obj, i, key;
    
    if (!koios.exists(theObject) || typeof theObject !== "object" || koios.isFunction(theObject)) {
        return theObject;
    }
    
    if (koios.isDate(theObject)) {
        return new Date(theObject.getTime());
    }
    
    if (theObject.hasOwnProperty("nodeType") && theObject.hasOwnProperty("cloneNode")) {
        return theObject.cloneNode(true);
    }
    
    if (theObject instanceof RegExp) {
        return new RegExp(theObject);
    }
    
    if (koios.isArray(theObject) || koios.isArguments(theObject)) {
        obj = [];
        for (i = 0; i < theObject.length; i += 1) {
            obj.push(theObject[i]);
        }
        
        return obj;
    }
    
    if (koios.isObject(theObject)) {
        obj = {};
        
        for (key in theObject) {
            if (theObject.hasOwnProperty(key)) {
                obj[key] = theObject[key];
            }
        }
        
        return obj;
    }
    
    return theObject;
};

/**
Polyfill for Array.indexOf() - returns the index of an item in an
array, or -1 if not found

@method indexOf
@param {Array} theArray - the array to search
@param {Object} theItem - the item to search for
@returns Number
*/

koios.indexOf = function (theArray, theItem) {
    var i, item;
    
    if (koios.exists(theArray)) {
        if (theArray.indexOf) {
            return theArray.indexOf(theItem);
        } else {
            for (i = 0; i < theArray.length; i += 1) {
                item = theArray[i];
                if (item === theItem) {
                    return i;
                }
            }
        }
    }
    
    return -1;
};

/**
A general purpose mixin function, mixing in the properties of source into target

__Example__

    koios.mixin({a:true}, {b:false});   // {a:true, b:false}

@method mixin
@param {Object} target - the object to mix into
@param {Object} source - the object to mix from
@returns Object
*/
        
koios.mixin = function (target, source) {
    var value, key;

    if (source) {
        for (key in source) {
            if (source.hasOwnProperty(key)) {
                value = source[key];
                if (em[key] !== value) {
                    target[key] = value;
                }
            }
        }
    }
    return target;
};

/**
A no operation function, ideal for passing around empty callbacks

@method noop
*/

koios.noop = function () {};

/**
Polyfill for Date.now() - returns the current timestamp

@method now
@returns Number
*/

koios.now = function () {
    if (Date.now) {
        return Date.now();
    } else {
        return new Date().getTime();
    }
};

/**
Returns a string with theNumber, padded to a given theLength with zeroes

__Example__

    koios.pad(1, 3);        // "001"

@method pad
@param {Number} theNumber - the number to pad
@param {Number} theLength - the length of theNumber to pad to
@returns String
*/

koios.pad = function (theNumber, theLength) {
    if (arguments.length === 1) {
        theLength = 2;
    }

    var prefix = "";
    if (parseInt(theNumber, 10) < 0) {
        prefix = "-";
        theNumber = (-1 * (parseInt(theNumber, 10))).toString();
    } else {
        theNumber = theNumber.toString();
    }

    while (theNumber.length < theLength) {
        theNumber = "0" + theNumber;
    }

    return prefix + theNumber;
};

/**
Returns a random number between 0 and theBounds

@method rand
@param {Number} theBounds - the top boundry (optional, default is 1)
@returns Number
*/

koios.rand = function (theBounds) {
    if (arguments.length === 0) {
        theBounds = 1;
    }
    
    return Math.floor(Math.random() * theBounds);
};

koios.setObject = function (theName, theValue) {
    var parts = theName.split("."),
        obj = global,
        i,
        l = parts.length - 1,
        key;
    
    for (i = 0; i < l; i += 1) {
        key = parts[i];
        if (!obj[key]) {
            obj[key] = {};
        }
        
        obj = obj[key];
    }
    
    obj[parts[l]] = theValue;
};

/**
Attempts to convert theObject to an array

@method toArray
@param {Array-ish} theObject - the object to convert
@returns Array
*/

koios.toArray = function (theObject) {
    return Array.prototype.slice.call(theObject);
};

// =================================================
// Multi purpose alternate mappings

/**
Tries to determine whether or not the first object contains the second object. This works for 
Arrays, Strings and Objects.

__Examples__

    koios.contains([1,2,3], 3);         // true
    koios.contains("1,2,3", "3");       // true
    
@method contains
@param {Object} theObject - the object to search in
@param {Object} theItem - the object to look for
@returns BOOL
*/

koios.contains = function (theObject, theItem) {
    if (koios.isArray(theObject)) {
        return koios.array.contains(theObject, theItem);
    } else if (koios.isString(theObject)) {
        return koios.string.contains(theObject, theItem);
    } else if (koios.isObject(theObject)) {
        return (koios.object.containsKey(theObject, theItem) || koios.object.containsValue(theObject, theItem));
    }
};

/**
Iterates through the items of theObject (either an Array or an Object), calling theCallback with each
item. See koios.array.each and koios.object.each for more documentation

@method each
@param {Object or Array} theObject - the object to iterate through
@param {Function} theCallback - the method to invoke with each object
*/

koios.each = function (theObject, theCallback) {
    if (koios.isArray(theObject)) {
        return koios.array.each(theObject, theCallback);
    } else if (koios.isObject(theObject)) {
        return koios.object.each(theObject, theCallback);
    }
};
