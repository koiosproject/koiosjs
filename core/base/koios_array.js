/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koios_array.js
Provides utility functions for array manipulation in KoiosJS
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module*/

// =================================================

/**
A set of Array manipulation methods for KoiosJS
@class koios.array
*/

koios.array = {

/**
An alternative mapping for koios.Array.every

__Example__

    koios.array.all([1,2,3], koios.isNumber);     // true
    koios.array.all([1,2,"3"], koios.isNumber);   // false
    
@method all
@param {Array} theArray - the array to query
@param {Function} theCallback - the callback function to invoke
@returns BOOL
*/
    
    all: function (theArray, theCallback) {
        return this.every(theArray, theCallback);
    },

/**
Adds theItem to theArray

@method append
@param {Array} theArray - the array to append to
@param {Object} theItem - the object to add
@returns Array
*/
    
    append: function (theArray, theItem) {
        return theArray.concat(theItem);
    },
   
/**
Returns a copy of theArray with all false-ish values removed

__Example__

    koios.array.compact([1,0,2,false,3,NaN,4,undefined,5]);    
    // [1,2,3,4,5]

@method compact
@param {Array} theArray - the array to compact
@returns Array
*/
    
    compact: function (theArray) {
        return this.filter(theArray, function (theItem) {
            return !koios.isFalseish(theItem);
        });
    },
    
/**
Returns whether or not theArray contains theItem

__Example__

    koios.array.contains([1,2,3], 2);   // true
    koios.array.contains([1,2,3], 4);   // false

@method contains
@param {Array} theArray - the array to append to
@param {Object} theItem - the object to check for
@returns BOOL
*/
    
    contains: function (theArray, theItem) {
        return koios.indexOf(theArray, theItem) > -1;
    },
    
/**
Returns the difference between two arrays by removing all items in theArray 
contained by filterOut

__Example__

    koios.array.difference([1,2,3], [2,3,4]);
    // [1]
    
@method difference
@param {Array} theArray - the first array
@param {Array} filterOut - the second array
@returns Array
*/
    
    difference: function (theArray, filterOut) {
        if (!koios.isArray(filterOut)) {
            filterOut = [filterOut];
        }
        
        return this.reject(theArray, function (theItem) {
            return koios.indexOf(filterOut, theItem) > -1;
        });
    },

/**
Calls theCallback once for every item in theArray, passing the item as the lone parameter to the
method invokation.

__Example__

    koios.array.each([1,2,3], function (theItem) {
        console.log(theItem);
    });
    
    // Will log 1, 2 & 3 as separate lines

@method each
@param {Array} theArray - the array to iterate through
@param {Function} theCallback - the method to invoke
*/
    
    each: function (theArray, theCallback) {
        // This will be overrided further on by the array prototype method if available.
        var i,
            l = theArray.length;
        
        for (i = 0; i < l; i += 1) {
            theCallback(theArray[i], i);
        }
    },

/**
Determines whether or not theArray equals theSecond

__Example__

    koios.array.equals([1,2,3], [1,2,3]);   // true
    koios.array.equals([1,2,3], [1,2,4]);   // false

@method equals
@param {Array} theArray - the first array
@param {Array} theSecond - the second array
@returns BOOL
*/
    
    equals: function (theArray, theSecond) {
        if (theArray === theSecond) {
            return true;
        }
        
        if (theArray === null || theSecond === null) {
            return false;
        }
        
        if (theArray.length !== theSecond.length) {
            return false;
        }
        
        var i;
        
        for (i = 0; i < theArray.length; i += 1) {
            if (theArray[i] !== theSecond[i]) {
                return false;
            }
        }
        
        return true;
    },
    
/**
Returns whether or not every value in theArray passes theCallback

__Example__

    koios.array.every([1,2,3], koios.isNumber);     // true
    koios.array.every([1,2,"3"], koios.isNumber);   // false

@method every
@param {Array} theArray - the array to query
@param {Function} theCallback - the callback function to invoke
@returns BOOL
*/
    
    every: function (theArray, theCallback) {
        // This will be overrided further on by the array prototype method if available.
        var i = 0,
            l = theArray.length,
            key;
        
        for (i = 0; i < l; i += 1) {
            key = theArray[i];
            if (!theCallback(key)) {
                return false;
            }
        }
        
        return true;
    },

/**
Returns an array of all items in theArray which passes theCallback's test

__Example__

    koios.array.filter([1,2, "three"], koios.isNumber);
    // [1,2]
    
@method filter
@param {Array} theArray - the array to query
@param {Function} theCallback - the callback function to invoke
@returns Array
*/
    
    filter: function (theArray, theCallback) {
        var i = 0,
            l = theArray.length,
            ret = [];
        
        for (i = 0; i < l; i += 1) {
            if (theCallback(theArray[i])) {
                ret.push(theArray[i]);
            }
        }
        
        return ret;
    },

/**
Returns an array of values returned by invoking theCallback with each of theArray's items

__Example__

    koios.array.map([1,2,3], function (theItem) {
        return theItem * 2;
    });
    
    // returns [2,4,6]

@method map
@param {Array} theArray - the array to query
@param {Function} theCallback - the callback function to invoke
@returns Array
*/
    
    map: function (theArray, theCallback) {
        var i = 0,
            l = theArray.length,
            ret = [];
        
        for (i = 0; i < l; i += 1) {
            ret.push(theCallback(theArray[i]));
        }
        
        return ret;
    },
    
/**
Returns whether or not none of the items in theArray pass theCallback

__Example__

    koios.array.none([1,2,3], koios.isString);      // true
    koios.array.none([1,2,"3"], koios.isString);    // false
    
@method none
@param {Array} theArray - the array to query
@param {Function} theCallback - the callback function to invoke
@returns BOOL
*/
    
    none: function (theArray, theCallback) {
        var i = 0,
            l = theArray.length;
        
        for (i = 0; i < l; i += 1) {
            if (theCallback(theArray[i])) {
                return false;
            }
        }
        
        return true;
    },

/**
A polyfill for Array.reduce - returns a single value generated by invoking theCallback for each
item in the array, passing in the cumulative value, the item in the array, the index and the array itself
as parameters. This method iterates from first object to last object.

__Example__

    koios.array.reduce([1,2,3], function (currentValue, nextValue, index, array) {
        return currentValue + nextValue
    });
    
    // will return 6
    
@method reduce
@param {Array} theArray - the array to query
@param {Function} theCallback - the callback function to invoke
@returns Value
*/
    
    reduce: function (theArray, theCallback) {
        var i = 0,
            l = theArray.length,
            ret;
        
        if (l < 2) {
            return theArray[0];
        }
        
        ret = theArray[0];
        
        for (i = 1; i < l; i += 1) {
            ret = theCallback(ret, theArray[i], i, theArray);
        }
        
        return ret;
    },
    
/**
An alternate mapping for koios.array.reduce()

@method reduceLeft
@param {Array} theArray - the array to query
@param {Function} theCallback - the callback function to invoke
@returns Value
*/
    
    reduceLeft: function (theArray, theCallback) {
        return this.reduce(theArray, theCallback);
    },
    
/**
A polyfill for Array.reduceRight - returns a single value generated by invoking theCallback for each
item in the array, passing in the cumulative value, the item in the array, the index and the array itself
as parameters. This method iterates from last object to first object.

__Example__

    koios.array.reduceRight(["rocks", "koios"], function (currentValue, nextValue, index, array) {
        return currentValue + " " + nextValue
    });
    
    // will return "koios rocks"
    
@method reduce
@param {Array} theArray - the array to query
@param {Function} theCallback - the callback function to invoke
@returns Value
*/
   
    reduceRight: function (theArray, theCallback) {
        var i = 0,
            l = theArray.length,
            ret = theArray[l - 1];

        if (l < 2) {
            return theArray[0];
        }

        for (i = l - 2; i > -1; i -= 1) {
            ret = theCallback(ret, theArray[i], i, theArray);
        }
        
        return ret;
    },
    
/**
Returns an array of values in theArray that do not pass theCallback

__Example__

    koios.array.reject([1,2,"3"], koios.isNumber);  // ["3"]

@method reject
@param {Array} theArray - the array to query
@param {Function} theCallback - the callback function to invoke
@returns Array
*/
    
    reject: function (theArray, theCallback) {
        var i = 0,
            l = theArray.length,
            ret = [];
        
        for (i = 0; i < l; i += 1) {
            if (!theCallback(theArray[i])) {
                ret.push(theArray[i]);
            }
        }
        
        return ret;
    },

/**
Removes theItem from theArray, if theArray contains it.

@method remove
@param {Array} theArray - the array to manipulate
@param {Object} theItem - the item to remove
@returns Array
*/
    
    remove: function (theArray, theItem) {
        var idx = this.indexOf(theArray, theItem);
        
        if (idx > -1) {
            theArray.splice(idx, 1);
            return theArray;
        } else {
            return theArray;
        }
    },
    
/**
Shuffles an array randomly

@method shuffle
@param {Array} theArray - the array to shuffle
@returns Array
*/
    
    shuffle: function (theArray) {
        var obj = koios.clone(theArray);
        return obj.sort(function () {
            return Math.random() - 0.5;
        });
    },
    
/**
Returns whether or not any of the items in theArray pass theCallback

__Example__

    koios.array.some([1,2,3], koios.isString);      // false
    koios.array.some([1,2,"3"], koios.isString);    // true

@method some
@param {Array} theArray - the array to query
@param {Function} theCallback - the callback function to invoke
@returns BOOL
*/
    
    some: function (theArray, theCallback) {
        var i = 0,
            l = theArray.length;
        
        for (i = 0; i < l; i += 1) {
            if (theCallback(theArray[i])) {
                return true;
            }
        }
        
        return false;
    }
};

// =================================================

/*
Use the prototype functions whenever possible.
*/

var protoFns = ["every", "filter", "forEach", "indexOf", "lastIndexOf", "map", "reduce", "reduceRight", "some"],
    i;

function wrapNative(key) {
    return function (theArray) {
        var args = koios.toArray(arguments).slice(1);
        return theArray[key].apply(theArray, args);
    };
}

for (i = 0; i < protoFns.length; i += 1) {
    var key = protoFns[i];
    if (Array.prototype.hasOwnProperty(key)) {
        koios.array[key] = wrapNative(key);
    }
}

if (koios.array.forEach) {
    koios.array.each = koios.array.forEach;
}
