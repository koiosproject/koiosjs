/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koios_lerp.js
Provides lerp functions for KoiosJS
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module*/

/**
@class koios
*/

/**
Returns an interpolation of the distance between theStartTime and theDuration at any given 
moment (koios.now()). Maximum return value is 1.

@method lerp
@param {Number} theStartTime - the starting timestamp to calculate against
@param {Number} theDuration - the duration to calculate against
@returns Number
*/

koios.lerp = function (theStartTime, theDuration) {
    var lerp = (koios.now() - theStartTime) / theDuration;
    return (lerp >= 1) ? 1 : lerp;
};

/**
Performs an easing function against the result of koios.lerp(). This is used
internally for koiosUI.Animations. A set of easing functions are provided
as within koios.easing

@method easedLerp
@param {Number} theStartTime - the starting timestamp to calculate against
@param {Number} theDuration - the duration to calculate against
@param {Function} theEasing - the easing function to perform
@param {BOOL} isReverse - whether or not to reverse the easing function
@returns Number
*/

koios.easedLerp = function (theStartTime, theDuration, theEasing, isReverse) {
    var lerp = koios.lerp(theStartTime, theDuration);
    if (isReverse) {
        return lerp >= 1 ? 0 : (1 - theEasing(1 - lerp));
    } else {
        return lerp >= 1 ? 1 : theEasing(lerp);
    }
};

/**
A set of easing functions to be used with .easedLerp() or koiosUI.Animations

The following functions are provided as a default:

    "linear"
    "swing"
    
__Included in modules-web/animator module__

    "spring",
    "ease",
    "easeIn",
    "easeOut",
    "easeInOut",
    "easeInQuad",
    "easeOutQuad",
    "easeInOutQuad",
    "easeInCubic",
    "easeOutCubic",
    "easeInOutCubic",
    "easeInQuart",
    "easeOutQuart",
    "easeInOutQuart",
    "easeInQuint",
    "easeOutQuint",
    "easeInOutQuint",
    "easeInExpo",
    "easeOutExpo",
    "easeInOutExpo",
    "easeInSine",
    "easeOutSine",
    "easeInOutSine",
    "easeInCirc",
    "easeOutCirc",
    "easeInOutCirc",
    "easeInElastic",
    "easeOutElastic",
    "easeInOutElastic",
    "easeInBack",
    "easeOutBack",
    "easeInOutBack",
    "easeInBounce",
    "easeOutBounce",
    "easeInOutBounce"
    
@class koios.easing
*/

koios.easing = {
    linear: function (n) {
        return n;
    },
    swing: function (n) {
        return 0.5 - Math.cos(n * Math.PI) / 2;
    }
};