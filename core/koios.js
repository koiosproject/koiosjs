/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module*/

if (typeof koios === "undefined") {
/**
The base koios object
@class koios
*/
    
/**
Core KoiosJS - a package of KoiosJS methods that are universally available
in KoiosJS applications

@module core
*/
    
    koios = {
        ENV: {},
        JSON: JSON,
        
        toString: function () {
            return "KoiosJS \nA Cross-Platform JavaScript Application Toolkit\n\nVersion: \n" + koios.JSON.stringify(koios.version());
        }
    };
}

if (typeof module !== "undefined" && module.exports) {
    module.exports = koios;
} else {
    this.koios = koios;
}