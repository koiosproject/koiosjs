/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koios_ready.js
Provides the koios:ready event
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module*/

/**
@class koios
*/

// =================================================

koios.messageEmitter.broadcast("koios:ready");

if (koios.Device.hasWindow) {
    koios.addListener(document, "DOMContentLoaded", function (event) {
        koios.emit("koios:ready");
    });
} else {
    koios.defer(function () {
        koios.emit("koios:ready");
    });
}

/**
Passes a listener to be executed once koios and the browser/server is ready
to go. If you call this once koios is ready, the callback function will be executed
immediately.

@method ready
@param {Function} theListener - the callback function to execute
*/

koios.ready = function (theListener) {
    if (koios.loaded) {
        theListener();
    } else {
        koios.on("koios:ready", theListener);
    }
};

/**
Whether or not the DOM/Server has loaded and KoiosJS is ready to go

@attribute loaded
@type BOOL
*/

koios.loaded = false;

koios.ready(function () {
    koios.loaded = true;
});