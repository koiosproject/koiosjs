/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koios_device.js
Provides koios.Device - a lightweight device information
wrapper
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module*/

// =================================================

/**
A singleton wrapper which provides basic information about the platform and device
your application is running on.

@class koios.Device
*/

if (typeof window !== "undefined") {
    
    /* Borrowed from EnyoJS for the time being. */
    var platforms = [
		// Android 4+ using Chrome
		{platform: "androidChrome", regex: /Android .* Chrome\/(\d+)[.\d]+/},
		// Android 2 - 4
		{platform: "android", regex: /Android (\d+)/},
		// Kindle Fire
		// Force version to 2, (desktop mode does not list android version)
		{platform: "android", regex: /Silk\/1./, version: 2, extra: {silk: 1}},
		// Kindle Fire HD
		// Force version to 4
		{platform: "android", regex: /Silk\/2./, version: 4, extra: {silk: 2}},
		// Windows Phone 7 - 8
		{platform: "windowsPhone", regex: /Windows Phone (?:OS )?(\d+)[.\d]+/},
		// IE 8 - 10
		{platform: "ie", regex: /MSIE (\d+)/},
		// iOS 3 - 5
		// Apple likes to make this complicated
		{platform: "ios", regex: /iP(?:hone|ad;(?: U;)? CPU) OS (\d+)/},
		// webOS 1 - 3
		{platform: "webos", regex: /(?:web|hpw)OS\/(\d+)/},
		// desktop Safari
		{platform: "safari", regex: /Version\/(\d+)[.\d]+\s+Safari/},
		// desktop Chrome
		{platform: "chrome", regex: /Chrome\/(\d+)[.\d]+/},
		// Firefox on Android
		{platform: "androidFirefox", regex: /Android;.*Firefox\/(\d+)/},
		// FirefoxOS
		{platform: "firefoxOS", regex: /Mobile;.*Firefox\/(\d+)/},
		// desktop Firefox
		{platform: "firefox", regex: /Firefox\/(\d+)/},
		// Blackberry 10+
		{platform: "blackberry", regex: /BB1\d;.*Version\/(\d+\.\d+)/},
		// Tizen
		{platform: "tizen", regex: /Tizen (\d+)/}    
    ];
    
    koios.singleton({
        className: "koios.Device",
        extend: "koios.Class",

        _elStyle: document.createElement("div").style,
        userAgent: navigator.userAgent,
        
        hasWindow: true,
        hasTouch: Boolean((window.hasOwnProperty("ontouchstart")) || window.navigator.msMaxTouchPoints),
        hasGesture: Boolean((window.hasOwnProperty("ongesturestart")) || window.navigator.msMaxTouchPoints),
        hasPointer: window.PointerEvent || window.MSPointerEvent,
        
        
        $init: function () {
            this.$super("$init", arguments);
            
            var ua = this.userAgent,
                t = this,
                match,
                version;
            
            koios.each(platforms, function (thePlatform) {
                match = thePlatform.regex.exec(ua);
                if (match) {
                    if (thePlatform.version) {
                        version = thePlatform.version;
                    } else {
                        version = Number(match[1]);
                    }
                    
                    t[thePlatform.platform] = version;
                    if (thePlatform.extra) {
                        koios.mixin(t, thePlatform.extra);
                    }
                } else {
                    t[thePlatform.platform] = 0;
                }
            });
            
            koios.defer(this.bind(function () {
                this.hasTransform = (this.vendorPrefix() + "Transform") in this._elStyle;
                this.hasPerspective = (this.vendorPrefix() + "Perspective") in this._elStyle;
                this.hasTransition = (this.vendorPrefix() + "Transition") in this._elStyle;
                this.hasAccelerate = this.hasPerspective;
            }));                        
        },
        
        vendorPrefix: function () {
            if (this._vendorPrefix) {
                return this._vendorPrefix;
            }

            if (koios.Device.is("ios") || koios.Device.is("android") || koios.Device.is("chrome") || koios.Device.is("androidChrome") || koios.Device.is("safari") || koios.Device.is("blackberry") || koios.Device.is("tizen")) {
                this._vendorPrefix = "webkit";
            }
            if (koios.Device.is("firefox") || koios.Device.is("firefoxOS") || koios.Device.is("androidFirefox")) {
                this._vendorPrefix = "moz";
            }

            if (koios.Device.is("ie")) {
                this._vendorPrefix = "ms";
            }

            return this._vendorPrefix;
        },
        
        is: function (thePlatform, minVersion) {
            if (arguments.length === 1) {
                minVersion = 1;
            }
                        
            return this[thePlatform] >= minVersion;
        },
        
        isLessThan: function (thePlatform, minVersion) {
            if (arguments.length === 1) {
                minVersion = 1;
            }
            
            if (!this[thePlatform] && this[thePlatform] === 0) {
                return false;
            }
            
            return this[thePlatform] <= minVersion;
        },
    });
} else {
    
    koios.singleton({
        className: "koios.Device",
        extend: "koios.Class",
        
        hasWindow: false
    });
    
}