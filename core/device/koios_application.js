/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koios_application.js
Provides koios.Application - the central hub of any KoiosJS
Application.
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module*/

// =================================================

/**
Provides basic UI rendering and handling class for KoiosJS apps

@module web/uicore
*/

/**
koios.Application is the central hub of any KoiosJS Application.
@class koios.Application
@extend koios.Base
*/

koios.define({
    className: "koios.Application",
    extend: "koios.Base",
    ignoreAlias: true,
    
    $init: function () {
        this.$super("$init", arguments);
        
        koios.ready(this.bind("onReady"));
    },
    
// =================================================
// Lifecycle
    
    onReady: function () {
    }
});