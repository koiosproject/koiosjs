/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koios_localstorage.js
Provides koios.LocalStorage
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module*/

/**
The koios.LocalStorage is a polyfilled wrapper around window.localStorage - a simple
key value store that persists between sessions. By default koios.Settings uses
this to backup settings between sessions.

@class koios.LocalStorage
@extends koios.Class
*/

koios.singleton({
    className: "koios.LocalStorage",
    extend: "koios.Class",
    ignoreAlias: true,
    
    $init: function () {
        this.$super("$init", arguments);
        
        this._setStorage();
        
        koios.messageEmitter.broadcast("koios:storage");
        koios.addListener(window, "storage", function (event) {
            koios.emit("koios:storage", event);
        });
    },
    
// =================================================
// Private
    
    _setStorage: function () {
        if (!this.localStorage) {
            if (typeof window !== "undefined" && window.localStorage) {
                this.localStorage = window.localStorage;
            }
        }
    },
    
// =================================================

/**
Clears all of the local storage

@method clear
*/
    
    clear: function () {
        this.localStorage.clear();
    },
    
/**
Gets the value stored in local storage by theKey

@method get
@param {String} theKey - the name of the key to lookup
@returns Value
*/
    
    get: function (key) {
        return this.localStorage.getItem(key);
    },
    
/**
Removes the value associated with theKey from local storage

@method remove
@param {String} theKey - the name of the key to remove
*/
    
    remove: function (key) {
        if (!key) {
            this.clear();
        } else {
            this.localStorage.removeItem(key);
        }
    },
    
/**
Sets theKey to theValue in local storage

@method set
@param {String} theKey - the name of the key to set
@param {Value} theValue - the value to set theKey to
*/
    
    set: function (key, value) {
        this.localStorage.setItem(key, value);
        koios.emit("koios:storage");
    }
});