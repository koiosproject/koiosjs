/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koios_settings.js
Provides koios.Settings
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module*/

/**
@class koios
*/

// =================================================

/**
koios.Settings is a persistent settings key-value store for use
within your KoiosJS apps. These are persisted in browser so they
shouldn't be considered safe from tampering, nor (by default) do they
persist to a server.

koios.Settings is a singleton and does not need to be instantiated.

@class koios.Settings
@extend koios.Class
*/

koios.singleton({
    className: "koios.Settings",
    extend: "koios.Class",
    ignoreAlias: true,
    
/**
By default koios.Settings.set() will automatically call .synchronize(). Set 
koios.Settings.autoSynchronizes to false in order to prevent this - you will
need to call .synchronize() yourself.

@attribute autoSynchronizes
@type BOOL
@default true
*/
    
    autoSynchronizes: true,
    
    _localKey: "_koiosSettings",
    
    $init: function () {
        this.$super("$init", arguments);
        
        this.store = koios.LocalStorage;
        var _settings = this.store.get(this._localKey) || "{}";
        this._settings = koios.JSON.parse(_settings);
        
        this.synchronize();
    },
    
// =================================================
// Defaults
    
/**
Registers a default key-value pair for the settings. If no setting has been set for theKey, theValue will be
set to it allowing you to override default settings at anytime.

@method registerDefault
@param {String} theKey - the key to set the default for
@param {Value} theValue - the value to set the key to
*/
    
    registerDefault: function (theKey, theValue) {
        if (!this._settings.hasOwnProperty(theKey)) {
            this.set(theKey, theValue);
        }
    },

/**
Registers a set of default key-value pairs for the settings. For every key-value pair, the settings will look
for a value already stored in the settings before setting theValue - allowing you to override the defaults
at anytime.

    koios.Settings.registerDefaults({
        "valueOne": 1,
        "valueTwo": 2
    });

@method registerDefaults
@param {Object} theDefaults - the key value object to set as defaults
*/

    registerDefaults: function (theDefaults) {
        var t = this;
        
        koios.each(theDefaults, function (key, value) {
            t.registerDefault(key, value);
        });
    },

// =================================================
// Getting & Setting
    
/**
Removes all settings from koios.Settings. 

@method clear
*/
    
    clear: function () {
        this._settings = {};
        this.synchronize();
    },
    
/**
Either returns all settings (if no key is passed in), or just the value of the
setting registered under theKey

    koios.Settings.get();               // all settings
    koios.Settings.get("valueOne");     // just the setting registered as valueOne

@method get
@param {String} theKey - the key to lookup (optional)
@returns Object or value
*/
    
    get: function (theKey) {
        if (theKey) {
            return this._settings[theKey];
        } else {
            return this._settings;
        }
    },
    
/**
Sets theKey to theValue for the settings. If .autoSynchronizes is set to true (default is true),
it will also call .synchronize()

@method set
@param {String} theKey - the key to set
@param {Value} theValue - the value to set theKey to
*/
    
    set: function (theKey, theValue) {
        this._settings[theKey] = theValue;
        
        if (this.autoSynchronizes) {
            this.synchronize();
        }
    },
    
// =================================================
// Synchronizing
    
/**
Stores the Settings into local storage to persist between sessions

@method synchronize
*/
    
    synchronize: function () {
        var JSON = koios.JSON.stringify(this._settings);
        this.store.set(this._localKey, JSON);
    }
});