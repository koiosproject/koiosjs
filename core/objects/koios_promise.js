/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koios_promise.js
Provides koios.Promise - an A+/Promises spec compliant object
which doubles as a shim for the forthcoming ES6 Promise object.
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module*/

koios._promiseResolver = {
    executeCallback: function (callback, promise, args) {
        var result;
        
        try {
            result = callback(args);
        } catch (error) {
            return promise.reject(error);
        }

        this.resolve(promise, result);
    },

    resolve: function (promise, result) {
        if (!koios.isObject(result) && !koios.isFunction(result)) {
            promise.fulfill(result);
            return promise;
        }

        if (result === promise) {
            return koios["throw"]("koios.Promise error(): you cannot fulfill a promise with itself.");
        }

        var _then = null,
            called = false;
        
        try {
            _then = result.then;
        } catch (error) {
            return promise.reject(error);
        }

        if (!koios.isFunction(_then)) {
            return promise.fulfill(result);
        }

        try {
            _then.call(result, function (y) {
                if (called) {
                    return;
                }
                koios._promiseResolve.resolve(promise, y);
                called = true;
            }, function (z) {
                if (called) {
                    return;
                }
                promise.reject(z);
                called = true;
            });
        } catch (e) {
            if (!called) {
                promise.reject(e);
            }
            called = true;
        }
    }
};

// =================================================

/**
koios.Promise is an A+/Promises spec compliant object
which doubles as a shim for the forthcoming ES6 Promise object.

@class koios.Promise
@extends koios.Class
*/

koios.define({
    className: "koios.Promise",
    extend: "koios.Class",
    
    statics: {
        state: {
            REJECTED: -1,
            PENDING: 0,
            FULFILLED: 1
        }
    },
    
    $init: function () {
        this.$super("$init", arguments);
        
        this.state = koios.Promise.state.PENDING;
        this.value = null;
        this.reason = null;
        this._callbacks = [];
    },
    
// =================================================
// Promise Information
    
/**
Returns whether or not the promise is considered "settled" - meaning that it's state
is either REJECTED or FULFILLED

@method settled
@returns BOOL
*/
    
    settled: function () {
        return (this.state !== koios.Promise.state.PENDING);
    },
    
// =================================================
// Wrapper
    
    _callback: function (onFulfilled, onRejected, promise) {
        this._callbacks.push({
            onFulfilled: onFulfilled,
            onRejected: onRejected,
            promise: promise
        });
    },
    
// =================================================
// Private Resolution
    
    _fulfill: function (callback) {
        if (koios.isFunction(callback.onFulfilled)) {
            koios._promiseResolver.executeCallback(callback.onFulfilled, callback.promise, this.value);
        } else {
            callback.promise.fulfill(this.value);
        }
    },
    
    _reject: function (callback) {
        if (koios.isFunction(callback.onRejected)) {
            koios._promiseResolver.executeCallback(callback.onRejected, callback.promise, this.reason);
        } else {
            callback.promise.reject(this.reason);
        }
    },

    _execute: function (fulfilled) {
        var t = this;
        if (fulfilled) {
            koios.each(this._callbacks, function (theItem) {
                t._fulfill(theItem);
            });
        } else {
            koios.each(this._callbacks, function (theItem) {
                t._reject(theItem);
            });
        }
        this.clearCallbacks();
    },
    
// =================================================
// Override these for future, repeatable promises

    clearCallbacks: function () {
        this._callbacks.length = 0;
    },

    shouldChangeState: function (callback) {
        if (this.state === koios.Promise.state.PENDING) {
            callback();
        }
    },

    _promise: function () {
        return koios.promise();
    },

    _retVal: function (promise) {
        return promise._thenable();
    },

    _thenable: function () {
        return {then: this.bind("then")};
    },
    
// =================================================
// Public Resolution

/**
Marks a promise as successfully completed, and sets its value to theValue - which will then get
passed along to each step of the promise resolution chain.

@method fulfill
@param {Object} theValue - the successful value of the promise
@returns this
*/
    
    fulfill: function (value) {
        var t = this;
        
        this.shouldChangeState(function () {
            t.state = koios.Promise.state.FULFILLED;
            t.value = value;

            koios.defer(koios.bind(t, "_execute", true));
        });

        return t;
    },

/**
Marks a promise as completed unsuccessfully, and sets its reason to theReason - which will then get
passed along to each step of the promise resolution chain.

@method reject
@param {Object} theReason - the reason for failure of the promise
@returns this
*/
    
    reject: function (reason) {
        var t = this;
        this.shouldChangeState(function () {
            t.state = koios.Promise.state.REJECTED;
            t.reason = reason;

            koios.defer(koios.bind(t, "_execute", false));
        });
    },

/**
Adds a onFulfilled and/or an onRejected method to be called when the promise is completed, returning
another promise which can be chained

@method then
@param {Function} onFulfilled - a method to be called when the promise is fulfilled
@param {Function} onRejected - a method to be called when the promise is rejected
@returns koios.Promise
*/
    
    then: function (onFulfilled, onRejected) {
        var promise = this._promise(),
            t = this;

        if (this.state === koios.Promise.state.PENDING) {
            this._callback(onFulfilled, onRejected, promise);
        } else if (this.state === koios.Promise.state.FULFILLED) {
            if (koios.isFunction(onFulfilled)) {
                koios.defer(function () {
                    koios._promiseResolver.executeCallback(onFulfilled, promise, t.value);
                });
            } else {
                promise.fulfill(this.value);
            }
        } else if (this.state === koios.Promise.state.REJECTED) {
            if (koios.isFunction(onRejected)) {
                koios.defer(function () {
                    koios._promiseResolver.executeCallback(onRejected, promise, t.reason);
                });
            } else {
                promise.reject(this.reason);
            }
        }

        return this._retVal(promise);
    }
});

/**
@class koios
*/

/**
Returns whether or not theObject is promise-ish (is an object, and has a .then() method)

@method isPromise
@param {Object} theObject - the object to check
@returns BOOL
*/

koios.isPromise = function (theObject) {
    return (theObject && koios.isObject(theObject) && theObject.then);
};
