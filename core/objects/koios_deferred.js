/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koios_deferred.js
Provides koios.Deferred - a special class of promises that provide a few
extra convenience methods, and are also repeatable - allowing for saving
a chain of functions to be repeated.
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module*/

// =================================================

/**
koios.Deferred is a special class of promises that provide a few
extra convenience methods, and are also repeatable - allowing for saving
a chain of functions to be repeated.

@class koios.Deferred
@extends koios.Class
*/

koios.define({
    className: "koios.Deferred",
    extend: "koios.Promise",
    
    multipleCallsAllowed: true,
    
// =================================================

    shouldChangeState: function (callback) {
        if (this.multipleCallsAllowed || this.state === koios.Promise.state.PENDING) {
            callback();
        }
    },

    clearCallbacks: function () {
        if (!this.multipleCallsAllowed) {
            this._callbacks.length = 0;
        }
    },

    _promise: function () {
        return koios.deferred();
    },

    _retVal: function (promise) {
        return promise;
    },

// =================================================

/**
Allows you to add a method that will be called, regardless of success or failure of the promise/deferred
itself

@method always
@param {Function} theCallback - the method to invoke regardless of success or failure
@returns koios.Deferred
*/
    
    always: function (theCallback) {
        return this.then(theCallback, theCallback);
    },

/**
Adds a callback function that delays the callback chain for a given theTimeout milliseconds

@method delay
@param {Number} theTimeout - the number of milliseconds to delay the promise resolution
@returns koios.Deferred
*/
    
    delay: function (theTimeout) {
        var d = koios.deferred(),
            t = this;
        
        d.always(koios.noop);
        this.always(function (val) {
            setTimeout(function () {
                if (t.state === koios.Promise.state.FULFILLED) {
                    d.fulfill(val);
                } else {
                    d.reject(val);
                }
            }, theTimeout);
        });
        
        return d;
    },

/**
A convenience mapping of this.then(null, theCallback)

@method error
@param {Function} theCallback - the callback function to execute on rejection
@returns koios.Deferred
*/

    error: function (theCallback) {
        return this.then(null, theCallback);
    },

/**
Removes a given koios.Deferred object from the promise resolution chain

@method forget
@param {koios.Deferred} theDeferred - the deferred object to remove
@returns this
*/
    
    forget: function (theDeferred) {
        this._callbacks = koios.array.filter(this._callbacks, function (theItem) {
            return (theItem.promise.__id !== theDeferred.__id);
        });
    
        return this;
    },

/**
A convenience mapping of this.then(theCallback, null);

@method success
@param {Function} theCallback - the callback function to execute on success
@returns koios.Deferred
*/
    
    success: function (theCallback) {
        return this.then(theCallback);
    }
});