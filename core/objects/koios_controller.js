/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koios_controller.js
Provides koios.Controller - a low level object with task support that
is designed to make the manipulation of managed objects easier.
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module*/

// =================================================

/**
koios.Controller is a low level object with task support that
is designed to make the manipulation of managed objects easier.

@class koios.Controller
@extends koios.Base
@uses koios.TaskSupport
*/

koios.define({
    className: "koios.Controller",
    extend: "koios.Base",
    inherit: ["koios.TaskSupport"],
    ignoreAlias: true
});