/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koios_task.js
Provides koios.Task - the basic building block of asynchronous
queues in koios.
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module*/

// =================================================

/**
koios.Task is a low level class used to implement asynchronous queues
in KoiosJS. When combined with koios.Queues you can safely defer non-"pressing"
items until the execution context is clearer

@class koios.Task
@extends koios.Class
@uses koios.CloneSupport
*/

koios.define({
    className: "koios.Task",
    extend: "koios.Class",
    inherit: ["koios.CloneSupport"],
    ignoreAlias: true,
    
    statics: {
        priority: {
            LOW: 1,
            MEDIUM: 5,
            HIGH: 10
        }
    },
    
    $init: function () {
        this.$super("$init", arguments);
        
        this.$clonable = ["className", "name", "method", "owner", "priority", "params"];
    },

// =================================================

    _fn: function () {
        var t = this,
            args = koios.toArray(arguments);
        
        return function () {
            t.execute.apply(t, args);
        };
    },
    
// =================================================

/**
Executes the task itself

@method execute
*/
    
    execute: function () {
        var args = koios.toArray(arguments);
        
        if (koios.isString(this.method)) {
            this.owner[this.method].apply(this.owner, args);
        } else {
            this.method.apply(this.owner, args);
        }
    },
    
/**
Adds the task to a given queue. If no queue is provided, it tells the this.queue (or koios.hgQueue) to enqueue
the task

@method enqueue
@returns this
*/
    
    enqueue: function (queue) {
        if (queue) {
            this.queue = queue;
        }
        
        if (!this.queue) {
            this.queue = koios.bgQueue;
        }
        
        if (!this.hasOwnProperty("priority")) {
            this.priority = koios.Task.priority.MEDIUM;
        }
        
        this.queue.addTask(this.name, this.priority, this._fn.apply(this, koios.toArray(arguments)));
        
        return this;
    },
    
/**
The priority of the task itself - cannot be changed once the task has been enqueued. .setPriority() is available
as a convenience method (which returns this)

    task.setPriority(koios.Task.priority.HIGH)
    
@attribute priority
@type Number
*/
    
    setPriority: function (newPriority) {
        this.priority = newPriority;
        return this;
    }
});

// =================================================

/**
@class koios
*/

/**
Creates a koios.Task object from a function definition 

__Example__

    var task = koios.task(this.bind("doSomething"));
    
@method task
@param {Function} theFn - the function to task out.
@returns koios.Task
*/

koios.task = function (theFn) {
    if (koios.isFunction(theFn)) {
        theFn = {method: theFn};
    }
    
    return new koios.Task(theFn);
};
