/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koios_queue.js
Provides koios.Queue - a mechanism for handling asynchronous tasking
with priority support within Koios.
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module*/

// =================================================

/**
koios.Queue is a mechanism for handling asynchronous tasks with priorities. It allows you to
safely defer an action which isn't time sensitive until the execution context has cleared.

@class koios.Queue
@extends koios.Class
@uses koios.PropertySupport
@uses koios.EventSupport
*/

koios.define({
    className: "koios.Queue",
    extend: "koios.Class",
    inherit: ["koios.PropertySupport", "koios.EventSupport"],
    ignoreAlias: true,
    
    priorityLevel: -1,
    defaultPriority: 5,
    
    properties: {
        priorityLevel: {
            value: -1,
            validate: koios.isNumber
        }
    },
    
    $init: function () {
        this.$super("$init", arguments);
        
        this.tasks = [];
        this._taskMap = {};
        this.stopped = false;
        this.taskCount = 0;
        
        this.on("priorityLevel:change", this.bind("priorityLevelChanged"));
        this.set("priorityLevel", this.priorityLevel);
        
        var i;
        
        for (i = 0; i < koios.Task.priority.HIGH; i += 1) {
            this.tasks.push([]);
        }
    },
    
// =================================================
// Task Manipulation
    
    addTask: function (taskName, taskPriority, theFn) {
        var task = {fn: theFn, name: taskName, priority: taskPriority - 1},
            level = this.get("priorityLevel");
        
        this.tasks[task.priority].push(task);
        this._taskMap[taskName] = task;
        this.taskCount += 1;
        
        if (level < task.priority) {
            this.set("priorityLevel", task.priority);
        }
    },
    
    removeTask: function () {
    },
    
// =================================================
// Events
    
    priorityLevelChanged: function () {
        var level = this.get("priorityLevel");
        
        if (this.priorityLevel === -1 && level > -1) {
            this._start();
        }
        
        this.priorityLevel = level;
    },
    
// =================================================
// Queue Manipulation
    
/**
Starts a queue if it has been stopped. 

@method start
*/
    
    start: function () {
        if (this.stopped) {
            this.stopped = false;
            this._next();
        }
    },
    
/**
Stops a queue

@method stop
*/
    
    stop: function () {
        this.stopped = true;
    },
    
// =================================================
// Queue Execution
    
    _next: function () {
        if (!this.stopped) {
            setTimeout(this.bind("_onTick"), 10);
        }
    },
    
    _onTick: function () {
        var level = this.get("priorityLevel"),
            tasks = this.tasks[level],
            task = tasks.shift();
        
        if (task) {
            this.taskCount -= 1;
            this._taskMap[task.name] = null;
            task.fn();
        }
        
        this._setNextPriorityLevel();
        
        if (this.taskCount > 0) {
            this._next();
        } else {
            this._queueDone();
        }
    },
    
    _queueDone: function () {
        this.set("priorityLevel", -1);
        this._taskMap = {};
    },
    
    _setNextPriorityLevel: function () {
        var level = this.get("priorityLevel"),
            i;
        
        for (i = level; i >= 0; i -= 1) {
            if (this.tasks[i].length >  0) {
                this.set("priorityLevel", i);
                return;
            }
        }
    },
    
    _start: function () {
        this._next();
    }
});

// =================================================

/**
@class koios
*/

/**
A general purpose background queue - the default queue for tasks without a queue
set.

@attribute bgQueue
@type koios.Queue
*/

koios.bgQueue = new koios.Queue();