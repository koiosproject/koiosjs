/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koios_base.js
Provides koios.Base
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module*/

// =================================================

/**
koios.Base is the lowest level object that takes full advantage of
the koios.define() OO system.

@class koios.Base
@extends koios.Class
@uses koios.EventSupport
@uses koios.PropertySupport
*/

koios.define({
    className: "koios.Base",
    extend: "koios.Class",
    inherit: ["koios.EventSupport", "koios.PropertySupport"],
    accumulated: ["broadcastable", "objects"],

    broadcastable: ["create", "destroy"],
    objects: [],
    properties: {
        owner: {value: null}
    },
    
    events: {},
    ignoreAlias: true,
    ignoreOwner: false,
    defaultClass: "koios.Base",
    
    $constructor: function () {
        this.objects = [];
        this.$super("$constructor", arguments);
    },
    
    $init: function () {
        this.$super("$init", arguments);
        
        this._$ = {};
        
        this.on("owner:change", this.bind("onOwnerChanged"));
        this.on("owner:beforeChange", this.bind("onBeforeOwnerChanged"));
        
        if (!this.ignoreOwner) {
            if (!this.owner) {
                this.set("owner", koios.master);
            } else {
                this.set("owner", this.owner);
                this.owner = null;
            }
        }
        
        this._generateOwnedObjects();
        this.emitGlobally("create", this);
    },

// =================================================
// Owned Objects

    _generateOwnedObjects: function () {
        if (this._objects) {
            this.addObjects(this._objects);
        }
//        this.objects.length = 0;
    },
    
/**
Add either an object or an object definition derived from koios.Base to an object's
ownership chain. If the object isn't already created, or the object's owner property isn't
set to this, then .addObject() is effectively a remapping of this.create();

__Example__

    this.addObject({className: "koios.Controller", name: "fooController");
    
@method addObject
@param {Object} theObject - the object (or koios.define() object definition) to add.
@returns Object
*/
    
    addObject: function (theObject) {
        if (this.app) {
            theObject.app = this.app;
        }
     
        if (!koios.isaSubclassOf(theObject, "koios.Class")) {
            return this.create(theObject);
        } else {
            if (theObject.get("owner") !== this) {
                theObject.set("owner", this);
                return theObject;
            }
            
            this._$[theObject.name] = theObject;
        }
        
        return theObject;
    },

/**
Adds an array of either an objects or an object definitions derived from koios.Base to an object's
ownership chain. 

@method addObjects
@param {Array} theObjects - the objects to add
@returns Array
*/
   
    addObjects: function (theObjects) {
        var t = this;
        
        if (!koios.isArray(theObjects)) {
            theObjects = [theObjects];
        }
        
        return koios.array.map(theObjects, function (theItem) {
            return t.addObject(theItem);
        });
    },
    
/**
Creates an object from an object definition and returns it. Unless specifically specified,
the create() function will set the owner parameter to this, and in doing so, trigger the .addObject()
ownership chain update

__Example__

    var obj = this.create({className: "koios.Base", name: "anotherObj"});
    
@method create
@param {Object} theObject - the object (or koios.define() object definition) to create.
@returns Object
*/
    
    create: function (objects, args) {
        if (!koios.isArray(objects)) {
            objects = [objects];
        }
        
        var obj = [],
            t = this;
        
        koios.each(objects, function (theObject) {
            var object = koios.clone(theObject),
                theClass = object.className || t.defaultClass,
                Ctor = koios.getConstructor(theClass);
            
            koios.mixin(object, args);
            object.className = (koios.isFunction(theClass)) ? object.prototype.className : theClass;
            object.owner = (object.hasOwnProperty("owner")) ? object.owner : t;
            
            if (!Ctor) {
                return koios["throw"](t.className + ".create() error: could not find constructor for class: " + theClass);
            }
            
            obj.push(new Ctor(object));
        });
        
        if (obj.length === 1) {
            return obj[0];
        } else {
            return obj;
        }
    },
    
/**
Removes an object from ownership, leaving the item without an owner - a strange state for a KoiosJS object. This is not the same as calling destroy() on the object (which will also take care of removing itself from ownership). It isn't likely that you'll ever need
to invoke .removeObject() directly

@method removeObject
@param {Object} theObject - the object to remove
*/
    
    removeObject: function (theObject) {
        if (this._$[theObject.name]) {
            this._$[theObject.name] = null;
        }
    },

// =================================================
// Ownership Events
    
    onBeforeOwnerChanged: function () {
        var owner = this.get("owner");
        if (owner) {
            owner.removeObject(this);
        }
    },
    
    onOwnerChanged: function () {
        var owner = this.get("owner");
        if (owner) {
            koios.log(this.name + " owner changed to " + owner.name, koios.Logger.INTERNAL);
            owner.addObject(this);
        }
    },
    
// =================================================
// Ownership Children
    
    $: function (args) {
        var keys = koios.object.keys(this._$),
            obj = [],
            t = this,
            i,
            key,
            value;
        
        if (arguments.length === 0) {
            return koios.object.values(this._$);
        }
        
        if (koios.isString(args)) {
            return this._$[args];
        }
        
        koios.each(args, function (key, value) {
            obj = koios.array.filter(obj, function (theItem) {
                if (koios.contains(key, "${")) {
                    return (t.get(key) === value);
                } else {
                    return (theItem[key] && theItem[key] === value);
                }
            });
        });
        
        return obj;
    },
    
// =================================================
// Garbage Collection
    
    destroy: function () {
        // Let observers know we're being destroyed
        this.emitGlobally("destroy");

        // Set the owner to null so that the ownership chain won't keep the object
        this.set("owner", null);

        // Remove any listeners
//        this.removeAllListeners();

        // Destroy down the chain
        koios.each(this.$(), function (theItem) {
            theItem.destroy();
        });

        // Finally have koios.Class clean the rest up.
        this.$super("destroy", arguments);
    },

/**
Defers the destruction of an object until after the current execution context. 

This is the same as calling

    koios.defer(this.bind("destroy"));
    
@method destroyLater
*/
    
    destroyLater: function () {
        koios.defer(this.bind("destroy"));
    }
});

// =================================================

/**
@class koios
*/

/**
A master object which owns all unowned objects

@attribute master
@type koios.Base
*/

koios.Base.subclass = function (theName, theConstructor, thePrototype) {
    if (thePrototype.objects && thePrototype.objects.length > 0) {
        thePrototype._objects = thePrototype.objects;
    }
};

koios.master = new koios.Base({name: "koiosMaster", ignoreOwner: true});

