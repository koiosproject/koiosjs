/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koios_listen.js
Provides koios.addListener
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module*/

koios.addListener = function (target, event, listener) {
    target.addEventListener(event, listener);
};