/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koios_eventhandler.js
Provides koios.EventHandler
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module*/

/**
The koios.EventHandler is a singleton object which provides an interface
for global DOM/Window event listening.

@class koios.EventHandler
@extends koios.Class
*/

koios.singleton({
    className: "koios.EventHandler",
    extend: "koios.Class",
    ignoreAlias: true,
    
    $init: function () {
        this.$super("$init", arguments);
    },
  
/**
Adds a listener to theEvent on theScope, calling theHandler when the event occurs

    koios.EventHandler.listen("resize", function (theEvent) {
        // Do something when the window resizes
    }, window);
    
@method listen
@param {String} theEvent - the name of the event to listen for
@param {Function} theHandler - the method to call
@param {Object} theScope - the scope to attach the event to (default is the global scope)
*/
    
    listen: function (theEvent, theHandler, theScope) {
        if (typeof window !== "undefined") {
            this._listenBrowser(theEvent, theHandler, theScope);
        } else {
            this._listenServer(theEvent, theHandler, theScope);
        }
    },
    
    _listenBrowser: function (theEvent, theHandler, theScope) {
        if (!theScope) {
            theScope = window;
        }
        
        theScope.addEventListener(theEvent, theHandler, false);
    },
    
    _listenServer: function (theEvent, theHandler, theScope) {
    },
    
// =================================================
// DOM Events
    
    // These are all the hooks for DOM events we listen for and normalize
    _domEvents: ["click", "mousedown", "mouseup", "mousemove"],
    _eventMap: {
        "click": {mouse: "click", touch: null},  // We remap our own click handler for touch,
        "mousedown": {mouse: "mousedown", touch: "touchstart"},
        "mouseup": {mouse: "mouseup", touch: "touchend"},
        "mousemove": {mouse: "mousemove", touch: "touchmove"}
    },
    
    isDOMEvent: function (theEvent) {
        return koios.contains(this._domEvents, theEvent);
    },
    
    _remappedEvent: function (theEvent) {
        var handler = this._eventMap[theEvent];
        if (koios.Device.hasTouch) {
            return handler.touch;
        } else {
            return handler.mouse;
        }
    },
    
    attachDOMEvent: function (theView, theEvent, theListener) {
        var domEvent = this._remappedEvent(theEvent);
        
        if (!theView._domEvents) {
            theView._domEvents = [];
        }
        
        if (!koios.contains(theView._domEvents, domEvent)) {
            theView._domEvents.push(domEvent);
            theView.domNode.addEventListener(domEvent.toLowerCase(), function (event) {
                theView.emit(theEvent);
                return theListener(event);
            });
        }
    }
});
        
