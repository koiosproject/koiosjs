/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koios_emitter.js
Provides koios.Emitter
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module*/

/**
A koios.Emitter is an abstract class that handles event dispatch and listening. All koios.Class-derived objects which
inherit the koios.EventSupport get an Emitter accessible via this._emitter and as such, is very rarely necessary for 
you to create directly.

@class koios.Emitter
@extends koios.Class
*/

// =================================================

koios.define({
    className: "koios.Emitter",
    extend: "koios.Class",
    
    $init: function () {
        this.$super("$init", arguments);
        
        this.events = {};
        this._broadcastable = [];
    },
    
// =================================================

    _extend: function (obj) {
        var fns = ["on", "off", "once", "emit"],
            t = this;
        
        this.owner = obj;
        
        koios.each(fns, function (theItem) {
            if (!obj.hasOwnProperty(theItem)) {
                obj[theItem] = t.bind(theItem);
            }
        });
    },
    
// =================================================
// Listener Retrieval
    
    listeners: function (theEvent) {
        if (!this.events[theEvent]) {
            this.events[theEvent] = [];
        }
        
        return this.events[theEvent];
    },
    
    _listenersAsObject: function (theEvent) {
        var listeners = this.listeners(theEvent),
            obj = {};
        
        obj[theEvent] = listeners;
        return obj;
    },

// =================================================
// Listener Manipulation
     
    addListener: function (theEvent, theListener) {
        var listeners = this.listeners(theEvent),
            key;
        
        if (koios.isFunction(theListener)) {
            theListener = {
                count: -1,
                listener: theListener
            };
        }
        
        if (!theListener || !koios.isObject(theListener)) {
            return;
        }
        
//        if (!theListener.hasOwnProperty("owner")) {
//            theListener.owner = this;
//        }
        
        theListener._event = theEvent;
        listeners.push(theListener);
        
        return theListener;
    },
    
    
    on: function (theEvent, theListener) {
//        if (koios.EventHandler.isDOMEvent(theEvent)) {
//            if (this.owner.domNode) {
//                koios.EventHandler.attachDOMEvent(this.owner, theEvent, function (event) {
//                    var ev = koios.event(event);
//                    return theListener(ev);
//                });
//            } else if (this.owner.requiresNode) {
//                var owner = this.owner;
//                
//                this.owner.requiresNode(function () {
//                    koios.EventHandler.attachDOMEvent(owner, theEvent, function (event) {
//                        var ev = koios.event(event);
//                        return theListener(ev);
//                    });
//                });
//            }
//        } else {
//            return this.addListener(theEvent, theListener);
//        }
        return this.addListener(theEvent, theListener);
    },
    
    once: function (theEvent, theListener) {
        if (koios.isFunction(theListener)) {
            theListener = {
                count: 1,
                listener: theListener
            };
        } else {
            theListener.count = 1;
        }
        
        return this.addListener(theEvent, theListener);
    },
 
    off: function (theEvent, theListener) {
        return this.removeListener(theEvent, theListener);
    },
    
    removeAllListeners: function (theEvent) {
        if (koios.exists(theEvent)) {
            this.events[theEvent] = [];
        } else {
            this.events = {};
        }
    },
    
    removeListener: function (theEvent, theListener) {
        if (!koios.exists(theListener)) {
            if (!koios.isString(theEvent)) {
                // We just got a listener, remove it.
                
                theListener = theEvent;
                theEvent = theEvent._event;
            } else {
                // We got an event name, remove all listeners from it.
                return this.removeAllListeners(theEvent);
            }
        }
                
        var listeners = this.listeners(theEvent);
        koios.array.remove(listeners, theListener);
    },
    
// =================================================
// Broadcasting
    
    broadcast: function (eventName) {
        if (!koios.isArray[eventName]) {
            eventName = [eventName];
        }
        
        var t = this;
        koios.each(eventName, function (ev) {
            t._broadcastable.push(ev);
        });
    },
    
    messages: function (filter) {
        var messages = this._broadcastable;
        if (arguments.length === 0) {
            return messages;
        } else {
            return koios.array.filter(messages, function (theMessage) {
                return koios.contains(theMessage, filter);
            });
        }
    },
    
// =================================================
// Event Emitting
    
    emit: function (theEvent) {
//        if (theEvent === "render") {
//            console.log("emitting " + this.owner.className + ":" + theEvent);
//        }
        
        var args = Array.prototype.slice.call(arguments, 1),
            listeners = this.listeners(theEvent),
            i,
            theListener,
            retVal,
            t = this;
        
        if (!args) {
            args = [];
        }
       
        for (i = 0; i < listeners.length; i += 1) {
            theListener = listeners[i];
            retVal = theListener.listener.apply(null, args);
            if ((theListener.hasOwnProperty("count")) && theListener.count > -1) {
                theListener.count -= 1;
                if (theListener.count === 0) {
                    this.removeLater(theEvent, theListener);
                }
            }
            
            if (koios.isBoolean(retVal) && retVal) {
                return true;
            }
        }
//        
//        koios.each(listeners, function (theListener) {
//            theListener.listener.apply(null, args);
//            if ((theListener.hasOwnProperty("count")) && theListener.count > -1) {
//                theListener.count -= 1;
//                if (theListener.count === 0) {
//                    koios.defer(function () {
//                        t.removeListener(theEvent, theListener);
//                    });
//                }
//            }
//        });
    },

    removeLater: function (theEvent, theListener) {
        var t = this;
        
        koios.defer(function () {
            t.removeListener(theEvent, theListener);
        });
    },
    
// =================================================
// Garbage Collection
    
    destroy: function () {
        this.$super("destroy", arguments);
    }
});

// =================================================

/**
@class koios
*/

/**
A default global koios.Emitter is available and retained by koios itself as koios.messageEmitter. You shouldn't have
any need to interface with this directly too often, but it is provided for you.

@attribute messageEmitter
@type koios.Emitter
*/

koios.messageEmitter = koios.emitter();
koios.messageEmitter._extend(koios);