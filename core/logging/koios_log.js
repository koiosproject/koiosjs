/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koios_log.js
Provides extensible logging via Koios
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module*/

/**
@class koios
*/

koios.log = function (theLog, theLevel) {
    if (arguments.length === 1) {
        theLevel = koios.Logger.LOG;
    }
    
    var logLevel = koios.config("logLevel");
    if (theLevel <= logLevel) {
        koios.emit("koios:log", theLog, theLevel);
    }
};

koios.warn = function (theLog) {
    koios.log(theLog, koios.Logger.WARN);
};

koios["throw"] = function (theLog) {
    koios.log(theLog, koios.Logger.ALERT);
    throw theLog;
};

koios.messageEmitter.broadcast("koios:log");
