/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koios_logger.js
Provides the base logger for Koios
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module*/

/**
An abstract plugin logger that receives koios:log emitted events, and routes
them to the appropriate handlers.

@class koios.Logger
@extends koios.Class
*/

koios.define({
    className: "koios.Logger",
    extend: "koios.Class",
    ignoreAlias: true,

    _logging: true,

    statics: {
		_PRIVATE: 7,
		INTERNAL: 6,
		LOG: 5,
		WARN: 4,
		ERROR: 3,
		ALERT: 2,
		ALL: 1
    },

    _calls: ["alert", "error", "warn", "log", "internal", "_private"],

    $init: function () {
        this.$super("$init", arguments);
        
        var i,
            l = this._calls.length;
        
        for (i = 0; i < l; i += 1) {
            if (!this[this._calls[i]]) {
                this[this._calls[i]] = koios.noop;
            }
        }

        if (this._logging) {
            this._listener = koios.on("koios:log", this.bind("_message"));
        }
    },

// =================================================

    _message: function (theLog, theLevel) {
        if (theLevel === 1) {
            this.call("alert");
        } else {
            this.call(this._calls[theLevel - 1], theLog);
        }
    },

    stopLogging: function () {
        if (this._logging) {
            this._logging = false;
            koios.off(this._listener);
        }
    },

    startLogging: function () {
        if (!this._logging) {
            this._logging = true;
            this._listener = koios.on("koios:log", this.bind("_message"));
        }
    }
});
