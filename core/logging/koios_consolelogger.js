/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koios_consolelogger.js
Provides a logger for Koios that outputs logging messages to the console,
if available
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module, console*/

/**
Provides a logger for Koios that outputs logging messages to the console,
if available. You shouldn't need to create one yourself - one is provided
for you.

@class koios.ConsoleLogger
@extends koios.Logger
*/

koios.define({
    className: "koios.ConsoleLogger",
    extend: "koios.Logger",
    ignoreAlias: true,

    $init: function () {
        this.$super("$init", arguments);

        this.console = console ||  {
            log: koios.noop,
            warn: koios.noop,
            error: koios.noop
        };
    },

// =================================================

	alert: function (theLog) {
		this.console.error(theLog);
		if (typeof window !== "undefined" && koios.config("showWindowAlerts")) {
			window.alert(theLog);
        }
    },

	error: function (theLog) {
		this.console.error(theLog);
    },

	warn: function (theLog) {
		this.console.warn(theLog);
    },

	log: function (theLog) {
		this.console.log(theLog);
    },

	internal: function (theLog) {
		this.console.log(theLog);
    },

	_private: function (theLog) {
		this.console.log(theLog);
    }
});

koios._consoleLogger = new koios.ConsoleLogger();
