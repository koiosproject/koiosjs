/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koiosui_viewcontroller.js
Provides koiosUI.ViewController
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module*/

/**
Provides basic UI rendering and handling class for KoiosJS apps

@module web/uicore
*/

/**
A higher level controller for views.

@class koiosUI.ViewController
@extends koiosUI.View
@uses koiosUI.ArrangerSupport
*/

koios.define({
    className: "koiosUI.ViewController",
    extend: "koiosUI.View",
    inherit: ["koios.TaskSupport"],
    
    $init: function () {
        this.$super("$init", arguments);
    }
});
