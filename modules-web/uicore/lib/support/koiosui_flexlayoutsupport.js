/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koiosui_flexlayoutsupport.js
Provides koiosUI.FlexLayoutSupport
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module, koiosUI*/

var _classes = {
    horizontal: "koios-flexbox-horizontal",
    vertical: "koios-flexbox-vertical"
},
    _defProps = {
        flex: {value: 1},
        direction: {value: "column"},
        width: {value: 0},
        height: {value: 0},
        align: {value: null}
    };

/**
koiosUI.FlexLayoutSupport is an abstract koios.Mixin that polyfills the koiosUI.View
system using the CSS Flexible Box Model.

@class koiosUI.FlexLayoutSupport
@extend koios.Mixin
*/


koios.define({
    className: "koiosUI.FlexLayoutSupport",
    extend: "koios.Mixin",
    
    mixin: function (theConstructor, thePrototype) {
        koios.mixin(thePrototype.properties, _defProps);
        
        thePrototype._initFn.push(this._setupFlex);
    },
    
    _setupFlex: function () {
        if (this.ignoreLayout) {
            return;
        }
        
        this.on("flex:change", this.bind("flexChanged"));
        this.on("direction:change", this.bind("directionChanged"));
        this.on("width:change", this.bind("widthChanged"));
        this.on("height:change", this.bind("heightChanged"));
        this.on("align:change", this.bind("alignChanged"));
        
        var props = ["flex", "direction", "width", "height", "align"],
            t = this;
        
        koios.each(props, function (theProp) {
            if (t[theProp] && t[theProp] !== _defProps[theProp].value) {
                t.set(theProp, t[theProp]);
            } else {
                t.emit(theProp + ":" + "change");
            }
        });
    },
    
    mixinProperties: {
        alignChanged: function () {
            var align = this.get("align"),
                attrName = koiosUI._layoutAttrs.alignItems;
            
            if (align) {
                this.setStyle(attrName, align);
            } else {
                this.removeStyle(attrName);
            }
        },
        
        flexChanged: function () {
            var flex = this.get("flex"),
                attrName = koiosUI._layoutAttrs.flex;
            
            if (flex > 0) {
                this.setStyle(attrName, flex);
                this.addClass("koios-flexbox");
            } else {
                this.removeStyle(attrName);
                this.removeClass("koios-flexbox");
            }
        },
                
        directionChanged: function () {
            var direction = this.get("direction");
            
            if (direction === "row") {
                this.removeClass(_classes.vertical);
                this.addClass(_classes.horizontal);
            } else {
                this.addClass(_classes.vertical);
                this.removeClass(_classes.horizontal);
            }
        },
        
        widthChanged: function () {
            var width = this.get("width");
            
            if (width > 0 && width !== "0px") {
                if (koios.isNumber(width)) {
                    this.setStyle("max-width", width + "px");
                    this.setStyle("min-width", width + "px");
                } else {
                    this.setStyle("max-width", width);
                    this.setStyle("min-width", width);
                }
            } else {
                this.removeStyle("max-width");
                this.removeStyle("min-width");
            }
        },
        
        heightChanged: function () {
            var height = this.get("height");
            
            if (height > 0 && height !== "0px") {
                if (koios.isNumber(height)) {
                    this.setStyle("max-height", height + "px");
                    this.setStyle("min-height", height + "px");
                } else {
                    this.setStyle("max-height", height);
                    this.setStyle("min-height", height);
                }
            } else {
                this.removeStyle("max-height");
                this.removeStyle("min-height");
            }
        }
    }
});

// =================================================
/*
We inherit flex layout support after definition in order to be able to
polyfill this later on for browsers with little to no CSS Flexible Box
Model support
*/

koiosUI.View.inherit("koiosUI.FlexLayoutSupport");
