/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koiosui_drawablesupport.js
Provides koiosUI.DrawableSupport
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module, koiosUI*/

var baseProps = {
    views: [],
    defaultDrawable: "koiosUI.View",
    tagName: "div",
    domEvents: {},
    domId: null,
    innerHTML: ""
};

/**
Provides basic UI rendering and handling class for KoiosJS apps

@module web/uicore
*/

/**
koiosUI.DrawableSupport is the mixin class that provides basic drawability
to KoiosJS objects. It doesn't provide all of the view convenience methods -
for that it would be suggested that you subclass koiosUI.View (or one of
it's subviews)

@class koiosUI.DrawableSupport
@extend koios.Mixin
*/

/**
The DOM node rendered and owned by the drawable object

__Inherited from koiosUI.DrawableSupport__

@attribute domNode
@type DOM Node
*/

koios.define({
    className: "koiosUI.DrawableSupport",
    extend: "koios.Mixin",
    
    mixin: function (theConstructor, thePrototype) {
//        if (!koios.contains(thePrototype.accumulated, "views")) {
//            thePrototype.accumulated.push("views");
//        }
        
        koios.each(baseProps, function (theKey, theValue) {
            if (!thePrototype[theKey]) {
                thePrototype[theKey] = theValue;
            }
        });
        
        thePrototype.domAttributes = {};
//        thePrototype._views = thePrototype.views;
        
        if (!koios.contains(thePrototype._initFn, this.createDomNode)) {
            thePrototype._initFn.push(this.createDomNode);
            thePrototype._initFn.push(this.decorateNode);
        }
    },
    
    createDomNode: function () {
//        this.views = koios.clone(this.views);
        
        if (this.domNode) {
            koios.log("has dom node?");
            if (koios.isString(this.domNode)) {
                this.domNode = koios.query(this.domNode, true);
            }
        } else {
            this.domNode = koios.el(this.tagName, this.domAttributes);
        }
        
        if (!this.renderNode) {
            this.renderNode = this.domNode;
        }
    },
    
    decorateNode: function () {
        if (!this.domId) {
            this.domId = this.name;
        }
        
        this.attr("id", this.domId);
        this.attr("koios-view", this.className);
        this.attr("koios-id", this.__id);
        if (this.innerHTML && this.innerHTML !== "") {
            this.html(this.innerHTML);
        }
        
        this._rendered = false;
        koiosUI._$[this.__id] = this;
    },
    
    mixinProperties: {
        
/**
Effectively the same as calling:

    koios.attr(this.domNode, theKey, theValue)
    
__Inherited from koiosUI.DrawableSupport__

@method attr
@param {String} theKey - the key to set (optional)
@param {String} theValue - the value to set theKey to (optional)
@returns this or Value
*/
        
        attr: function (key, value) {
            if (value) {
                koios.attr(this.domNode, key, value);
                return this;
            } else {
                return koios.attr(this.domNode, key, value);
            }
        },
 
/**
Effectively the same as calling:

    koios.html(this.domNode, theHTML)
    
__Inherited from koiosUI.DrawableSupport__

@method html
@param {String} theHTML - the inner html to set (optional)
@returns this or HTML
*/

        html: function (theHTML) {
            if (theHTML) {
                this.innerHTML = theHTML;
                koios.html(this.domNode, theHTML);
                return this;
            } else {
                return koios.html(this.domNode);
            }
        },

/**
Renders the owned DOM node (this.domNode) into either another DOM node, or the first returned
value of calling koios.query(theNode); A destroyed view cannot be re-rendered.
    
__Inherited from koiosUI.DrawableSupport__

@method renderInto
@param {String or DOM Node} theNode - the node to render into, or a query to return one.
@returns this or null (if view is already destroyed)
*/

        renderInto: function (domNode) {
            if (!this._destroyed) {
//                if (!koios.isNode(domNode)) {
//                    domNode = domNode.domNode;
//                }
                
                if (domNode) {
                    koios.appendNode(domNode, this.domNode);
                    
                    if (!this._rendered) {
                        this.emit("render");
                        this._rendered = true;
                        
                        if (this.appear) {
                            this.appear();
                        }
                    }
                    return this;
                }
                return this;
            } else {
                koios.log(this);
                koios.log("| destroyed");
                return koios.warn(this.className + ".renderInto() error: view already destroyed.");
            }
        },
        
        destroy: function () {
            this.$super("destroy", arguments);
            koiosUI._$[this.__id] = null;
        }
    }
});