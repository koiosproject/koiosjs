/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koiosui_accessory.js
Provides koiosUI.Accessory - a floating, flex-less koiosUI.View.
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module, koiosUI*/

/**
Provides basic UI rendering and handling class for KoiosJS apps

@module web/uicore
*/

/**
A koiosUI.Accessory is a floating overlay type of view that does not take part
in the standard layout style. It's ideal for secondary action buttons adorning
a view and shouldn't be overused.

@class koiosUI.Accessory
@extend koiosUI.View
*/

koios.define({
    className: "koiosUI.Accessory",
    extend: "koiosUI.View",
    ignoreLayout: true,
    
    properties: {
        accessoryTarget: {value: null}
    },
    
    cssClass: ["koios-accessory"],
    
    $init: function () {
        this.$super("$init", arguments);
        
        this.setStyle("display", "none");
        this.on("accessoryTarget:change", this.bind("updateAccessoryTarget"));
    },
        
    updateAccessoryTarget: function () {
        var owner = this.get("accessoryTarget");
        
        if (!this._owner) {
            this.removeStyle("display");
        }
        
        this._owner = owner;
        
        if (!this._owner) {
            this.setStyle("display", "none");
        }
        
        owner.addClass("koios-accessory-container");
    }
});