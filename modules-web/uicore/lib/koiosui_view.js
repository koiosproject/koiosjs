/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koiosui_view.js
Provides koiosUI.View - the base drawable item in KoiosJS.
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module, koiosUI*/

/**
Provides basic UI rendering and handling class for KoiosJS apps

@module web/uicore
*/

/**
koiosUI.View is the base drawable for KoiosJS applications.

@class koiosUI.View
@extend koios.Base
@uses koiosUI.DrawableSupport
@uses koiosUI.AnimationSupport
@uses koiosUI.EffectSupport
*/

koios.define({
    className: "koiosUI.View",
    extend: "koios.Base",
    inherit: ["koiosUI.DrawableSupport"],
    flex: 1,
    
    _views: [],
    cssClass: "",
    cssStyle: "",
        
    $constructor: function () {
        this.views = [];
        this._untilNode = [];
        this.domNode = null;
        this.renderNode = null;
        
        this.cssClass = koios.mixin([], this.cssClass);
        if (koios.isObject(this.cssStyle)) {
            this.cssStyle = koios.mixin({}, this.cssStyle);
        } else if (koios.isString(this.cssStyle) && koios.hasOwnProperty("cssToObject")) {
            this.cssStyle = koios.mixin({}, koios.string.toObject(this.cssStyle, "css"));
        } else {
            koios.warn(this.className + ".$constructor() warning - invalid cssStyle");
        }
        
        this.$super("$constructor", arguments);
    },
    
    $init: function () {
        this.$super("$init", arguments);
        
        this._setCssClass();
        this._setCssStyle();
        this._renderOwnedView();
        this._handleRequiresNode();

        this.on("owner:change", this.bind("_viewOwnerChanged"));

        if (this.get("owner")) {
            this._viewOwnerChanged();
        }
    },

// =================================================
// CSS Classes
    
    _setCssClass: function () {
        var classes = (koios.isString(this.cssClass)) ? this.cssClass.split(",") : this.cssClass,
            addClass = this.bind("addClass");
        
        koios.each(classes, function (theClass) {
            addClass(theClass);
        });
    },
    
/**
Adds a CSS class to the class attribute of a view

@method addClass
@param {String} theClass - the class to add
*/
    
    addClass: function (theClass) {
        koios.addClass(this.domNode, theClass);
    },
    
/**
Removes a CSS class from the class attribute of a view

@method removeClass
@param {String} theClass - the class to remove
*/
    
    removeClass: function (theClass) {
        koios.removeClass(this.domNode, theClass);
    },
    
/**
Removes a CSS class from the class attribute of a view, if the view already has it, or
adds it if the class isn't present already

@method toggleClass
@param {String} theClass - the css class to toggle.
*/
    
    toggleClass: function (theClass) {
        koios.toggleClass(this.domNode, theClass);
    },
    
/**
Returns all CSS classes attached to a view

@method allClasses
@returns Array
*/
    
    allClasses: function () {
        return koios.allClasses(this.domNode);
    },
    
// =================================================
// CSS Styles
    
    _setCssStyle: function () {
        if (koios.isString(this.cssStyle)) {
            var els = this.cssStyle.split(";"),
                setStyle = this.bind("setStyle"),
                comps;
            
            koios.each(els, function (theEl) {
                comps = theEl.split(":");
                setStyle(comps[0], comps[1]);
            });
        } else {
            this.setStyle(this.cssStyle);
        }
    },
    
/**
Gets a CSS style from the style attribute of a view

@method getStyle
@param {String} theStyle - the style key to return
@returns Value
*/
    
    getStyle: function (theStyle) {
        return koios.css(this.domNode, theStyle);
    },
    
/**
Removes a CSS style from the style attribute of a view

@method removeStyle
@param {String} theStyle - the style to remove
*/
    
    removeStyle: function (theStyle) {
        koios.removeCss(this.domNode, theStyle);
    },
    
/**
Sets a view's CSS style key to theValue

    this.setStyle("background-color", "green");
    
@method setStyle
@param {String} theKey - the css style key to set
@param {String} theValue - the value to set the key to
*/
    
    setStyle: function (theKey, theValue) {
        koios.css(this.domNode, theKey, theValue);
    },
    
/**
Returns the style of the domNode owned by a view

@method style
@returns Value
*/
    
    style: function () {
        return koios.css(this.domNode);
    },

// =================================================
// View Ownership
    
    _viewOwnerChanged: function () {
        var owner = this.get("owner");
        
        if (!owner) {
            koios.removeNode(this.domNode.parentNode, this.domNode);
        } else {
            this.renderInto(owner.renderNode);
        }
    },
    
// =================================================
// Views
    
    _renderOwnedView: function () {
        var insertView = this.bind("_insertView"),
            allViews = [];
        
        koios.each(this._views, function (theView) {
            allViews.push(insertView(theView));
        });
        
        koios.each(this.views, function (theView) {
            allViews.push(insertView(theView));
        });
        
        this.views = allViews;
    },
    
    _insertView: function (theView) {
        if (!theView.className) {
            theView.className = this.defaultDrawable;
        }
        
        var item = this.addObject(theView);
        
        return item;
    },
   
/**
Adds either an object definition or a koiosUI.View derived object to the view chain of
a view.

@method addView
@param {koiosUI.View or Object} theView - either a koiosUI.View-derived object or the object to define one.
@returns koiosUI.View
*/
    
    addView: function (theView) {
        var item = this._insertView(theView);
        this.views.push(item);
        return item;
    },
    
    moveView: function (theView, theIndex) {
    },
    
    removeView: function (theView) {
    },
    
    destroySubview: function (theView) {
    },

// =================================================
// Accessories
    
    presentAccessory: function (theView) {
        var item = this.addView(theView);
        item.set("accessoryTarget", this);
    },
    
// =================================================
// Showing/Hiding
    
    show: function () {
    },
    
    hide: function () {
        this.domNode.style.display = "none";
    },
    
// Stubs
    
    appear: function () {
        this.show();
    },
    
    disappear: function () {
        this.hide();
    },
    
// =================================================
// Templating
    
/**
.template() takes a view's current innerHTML value and lets you template it using theTemplate as a guide

    view.html("${library} rocks!");
    view.template({library: "Koios"});
    view.html();
    -> "Koios rocks!"

@method template
@param {Object} theTemplate - the template object to use
*/
    
    template: function (theTemplate) {
        this.innerHTML = koios.string.template(this.domNode.innerHTML, theTemplate);
        this.html(this.innerHTML);
    },

// =================================================
// DOM Node Requiring
    
    requiresNode: function (theFn) {
        this._untilNode.push(theFn);
    },
    
    _handleRequiresNode: function () {
        koios.each(this._untilNode, function (theFn) {
            theFn();
        });
        
        this._untilNode.length = 0;
    }
});

koiosUI.View.subclass = function (className, theConstructor, thePrototype) {
    if (thePrototype.views) {
        thePrototype._views = thePrototype.views;
    }
};
