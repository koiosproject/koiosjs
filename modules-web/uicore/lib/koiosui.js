/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koiosui.js
Provides the basic koiosUI object.
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module*/

/**
A very low-level container of UI methods and attributes used by UI applications
in KoiosJS. 

@class koiosUI
*/

var koiosUI = {
    _$: {},
    _layoutAttrs: {},

// Physics Values
    
	springDamping: 0.93,
	dragDamping: 0.5,
	frictionDamping: 0.97,
	snapFriction: 0.9,
	flickScalar: 15,
    frictionEpsilon: 1e-2
};

if (koios.Device.is("safari") || koios.Device.is("ios") || koios.Device.is("android") || koios.Device.is("blackberry")) {    
    koiosUI._layoutAttrs.flex = "-webkit-flex";
    koiosUI._layoutAttrs.alignItems = "-webkit-align-items";
} else {
    koiosUI._layoutAttrs.flex = "flex";
    koiosUI._layoutAttrs.alignItems = "align-items";
}

if (typeof module !== "undefined" && module.exports) {
    module.exports = koiosUI;
} else {
    this.koiosUI = koiosUI;
}
