/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koiosui_application.js
Provides koiosUI.Application
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module*/

/**
Provides basic UI rendering and handling class for KoiosJS apps

@module web/uicore
*/

/**
A subclass of koios.Application which handles arranging and presenting
views for browser-based KoiosJS applications.

There are two lifecycle methods invoked which can be overridden by 
instances or subclasses of koiosUI.Application

    koiosUI.Application.onReady -> called when the KoiosJS is ready (koios.ready())
    koiosUI.Application.onRendered -> called once the koiosUI.Application has been rendered.

@class koiosUI.Application
@extend koios.Application
@uses koiosUI.DrawableSupport
*/

koios.define({
    className: "koiosUI.Application",
    extend: "koios.Application",
    inherit: ["koiosUI.DrawableSupport"],
    
    cssClass: ["koios-fit", "koios-app"],
    
    $init: function () {
        var classes = (this.domAttributes.hasOwnProperty("class")) ? this.domAttributes["class"] : "";
        classes += " koios-fit koios-app";
        this.domAttributes["class"] = koios.string.trimWhitespace(classes);
        
        this.$super("$init", arguments);
        this.once("render", this.bind("_updateNode"));
        
        this.viewControllers = [];
    },
    
    _updateNode: function () {
        koios.defer(function () {
            window.scrollTo(0, 1);
        });
        koios.addClass(this.domNode.parentNode, "koios-fit");
    },
        
// =================================================
// View Controllers
    
    _updateRootViewController: function (theViewController) {
        var vc = this.addObject(theViewController);//.renderInto(this);
        vc.addClass("koios-fit");
        
        this.viewControllers.push(vc);
        this.rootViewController = vc;
    },
    
    presentViewController: function (theViewController, presentationStyle) {
        if (!presentationStyle) {
            return this._updateRootViewController(theViewController);
        }
    }

});
