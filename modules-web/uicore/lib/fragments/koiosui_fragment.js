/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koiosui_fragment.js
Provides koiosUI.Fragment.
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module*/

/**
Provides basic UI rendering and handling class for KoiosJS apps

@module web/uicore
*/

/**
koiosUI.Fragment is a KoiosUI view containing some form of outside or deferred
content. The base koiosUI.Fragment can be used to wrap existing DOM elements
in a koiosUI.View.

@class koiosUI.Fragment
@extend koiosUI.View
*/

/**
The target DOM node to wrap in a fragment. This can either be an actual DOM node or
a query string to use with the dom node.

@attribute targetNode
@type String or DOM Node
*/

/**
A template object to template the fragment's innerHTML with. Set to null to prevent
templating.

@attribute templateValue
@type Object
*/

koios.define({
    className: "koiosUI.Fragment",
    extend: "koiosUI.View",
    
    targetNode: null,
    templateValue: null,
    
    $init: function () {
        this.$super("$init", arguments);
        
        if (this.targetNode) {
            koios.defer(this.bind("_renderNode"));
        } else {
            koios.warn("koiosUI.Fragment warning: no target node set for " + this.name);
        }
    },
    
    _renderNode: function () {
        var node = (koios.isNode(this.targetNode)) ? this.targetNode : koios.query(this.targetNode, true);
        
        if (!node) {
            koios.warn("koiosUI.Fragment warning: invalid target node set for " + this.name);
        }
        
        koios.appendNode(this.domNode, node);
        this.innerHTML = this.domNode.innerHTML;
        this._innerHTML = this.innerHTML;
        
        if (this.templateValue) {
            this.template(this.templateValue);
        }
    },
    
// =================================================
// Refreshing
   
/**
Refreshes the fragment, using theTemplate to template the fragment's innerHTML. The fragment retains
a copy of the original innerHTML so that you can template the same value over and over again.

@method refresh
@param {Object} theTemplate - the object to template with
@returns this
*/
    
    refresh: function (templateObject) {
        this.html(this._innerHTML);
        this.templateValue = templateObject;
        this.template(this.templateValue);
        return this;
    }
});