/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koiosui_ajaxfragment.js
Provides koiosUI.AjaxFragment.
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module*/

/**
Provides basic UI rendering and handling class for KoiosJS apps

@module web/uicore
*/

/**
koiosUI.AjaxFragment is a fragment which loads content via Ajax, and optionally
templates it before presenting it in a koiosUI.View.

If you don't include the modules-web/ajax module prior to uicore, this class won't be defined.

@class koiosUI.AjaxFragment
@extend koiosUI.View
*/

/**
A template object to template the fragment's innerHTML with. Set to null to prevent
templating.

@attribute templateValue
@type Object
*/

/**
The endpoint to call to load the content

@attribute endpoint
@type String
*/

/**
If set to true, deferred will tell the AjaxFragment to not load it's content until you manually
call .reload()

@attribute deferred
@type BOOL
*/

if (koios.hasOwnProperty("ajax")) {
    koios.define({
        className: "koiosUI.AjaxFragment",
        extend: "koiosUI.View",

        endpoint: null,
        templateValue: null,
        deferred: false,
        
        $init: function () {
            this.$super("$init", arguments);
            
            if (this.endpoint && !this.deferred) {
                koios.defer(this.bind("reload"));
            }
        },
        
// =================================================
// Reloading & Refreshing
        
/**
Reloads the AjaxFragment from the datasource, and re-applies theTemplate to it.

.reload() takes two optional parameters

    theEndpoint: allows you to override this.endpoint temporarily and load a different URL
    theTemplate: allows you to override this.templateValue temporarily and template with a different object
    
@method reload
@param {String} theEndpoint - the URL to load (optional)
@param {Object} theTemplate - the object to template the response with.
@returns this
*/
        
        reload: function (theEndpoint, theTemplate) {
            if (arguments.length === 0) {
                theEndpoint = this.endpoint;
            }
            
            if (!koios.isString(theEndpoint)) {
                theTemplate = theEndpoint;
                theEndpoint = this.endpoint;
            }
            
            if (!theEndpoint) {
                return;
            }
            
            if (!theTemplate) {
                theTemplate = this.templateValue;
            }
            
            var t = this;
            
            koios.ajax(theEndpoint).then(function (response) {
                t.innerHTML = response.text;
                t._innerHTML = t.innerHTML;
                t.refresh(theTemplate);
            });
            
            return this;
        },
      
/**
Refreshes the already fetched content of the AjaxFragment with a different template object.

@method refresh
@param {Object} theTemplate - the object to template with
@returns this
*/
        
        refresh: function (theTemplate) {
            this.html(this._innerHTML);
            this.templateValue = theTemplate;
            this.template(this.templateValue);
            return this;
        }
    });
}