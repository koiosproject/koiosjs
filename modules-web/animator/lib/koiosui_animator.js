/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koiosui_animator.js
Provides koiosUI.Animator
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module, koiosUI*/

/**
Provides the toolset to make vibrant, animated web apps using
KoiosJS

@module web/animator
*/

/**
The koiosUI.Animator is a singleton object that handles creating, managing
and coalescing animations for performance purposes.

You should have no need to interface with koiosUI.Animator directly.

@class koiosUI.Animator
@extends koios.Class
*/

koios.singleton({
    className: "koiosUI.Animator",
    extend: "koios.Class",
    
    _animations: [],
    animating: false,
    
    $init: function () {
        this.$super("$init", arguments);
    },
    
// =================================================
// Animation Manipulation
    
    add: function (theAnimation) {
        this._animations.push(theAnimation);
        theAnimation.ts = koios.now();
        
        if (!this.animating) {
            this.start();
        }
    },

    start: function () {
        this.animating = true;
        this.next();
    },
    
    stop: function () {
        this._animations.length = 0;
        this.animating = false;
    },
    
    next: function () {
        if (this._animations.length === 0) {
            this.animating = false;
            return;
        }
        
        this._step();
    },
    
// =================================================
// Steps
    
    _step: function () {
        var steps = [];
        
        koios.each(this._animations, function (anim) {
            var step = anim._step();
            if (step) {
                steps.push(step);
            }
        });
        
        if (steps.length === 0) {
            return this.stop();
        }
        
        steps.push(this.bind("next"));
        
        koiosUI.requestAnimationFrame(function () {
            koios.each(steps, function (step) {
                step();
            });
        });
    },
    
// =================================================
// Animation

    animate: function (theNode, theDefinition) {
        if (!koios.isString(theNode) && !koios.isNode(theNode)) {
            theDefinition = theNode;
            theNode = theDefinition.node;
        }
        
        if (koios.isString(theNode)) {
            theNode = koios.query(theNode, true);
        }
        
        theDefinition.node = theNode;
        
        var anim = new koiosUI.Animation(theDefinition);
        return anim.promise;
    }
});

// =================================================

/**
@class koiosUI
*/

/**
Returns a koiosUI.Animation Object from either a node query and an object definition,
or just an object definition.

    koiosUI.animate("#node", {css: {height: "300px"}});
    koiosUI.animate({node:"#node", css: {height: "300px"}});
    koiosUI.animate({node:node, css: {height: "300px"}});

__Requires the modules-web/animator module__

@method animate
@returns koios.Promise
*/

koiosUI.animate = koiosUI.Animator.bind("animate");