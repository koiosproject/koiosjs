/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koiosui_animation.js
Provides koiosUI.Animation
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module, koiosUI*/

/**
Provides the toolset to make vibrant, animated web apps using
KoiosJS

@module web/animator
*/

/**
A koiosUI.Animation is returned via koiosUI.animate() which can be used
in one of two ways:

    koiosUI.animate("#node", {css: {height: "300px"}});
    
Or

    koiosUI.animate({node:"#node", css: {height: "300px"}});
    
@class koiosUI.Animation
@extend koios.Class
*/

/**
The number of milliseconds to delay before starting the animation

@attribute delay
@type Number
*/

/**
The easing function to apply to the animation - default is linear. This
can be either a function or a String.

    {easing: easingFn, ...}
    {easing: "spring", ...}
    
If the easing is a String, it will look for an easing function in koios.easing
of the same name.

@attribute easing
@type Function or String
*/

/**
All standard CSS attributes to animate as a key-value object

    {css: {"background-color": "#FFFFFF", width: "400px"}}
    
Keys can be as a style key, or a CSS definition. So the following is equivalent to the above:

    {css: {backgroundColor: "#FFFFFF", width: "400px"}}

By default all simple numeric keys are animateable. If you include the optional __modules-web/style__ module,
colors will be animateable as well.

__Note__

The following things aren't animateable using .css

    Compound Items - items such as border, which are a composite of multiple definitions (TODO)
    
@attribute css
@type Object
*/

/**
The number of milliseconds to run the animation. This can be either a number, or a macro String. If
the duration is a String, it looks for a key in koiosUI.animationDurations - available values include
"very fast", "fast", "slow", "very slow"

@attribute duration
@type Number or String
*/

/**
Whether or not to automatically add the Animation to koiosUI.Animator. Setting .deferred to true
will let you hold onto a koiosUI.Animation object until you're ready for it.

@attribute deferred
@type BOOL
*/

koios.define({
    className: "koiosUI.Animation",
    extend: "koios.Class",
    
    delay: 0,
    easing: koios.easing.linear,
    css: {},
    progress: 0,
    duration: 400,
    deferred: false,
    
    $init: function () {
        this.$super("$init", arguments);
        
        this._setupEasing();
        this._setupDuration();
        this._setupData();
        
        this.promise = koios.promise();
        
        if (!this.deferred) {
            this.play();
        }
    },
  
// =================================================
// Setup
    
    _setupEasing: function () {
        if (!koios.isFunction(this.easing)) {
            this.easing = koiosUI._animationNormalizer.easingFromString(this.easing);
        }
    },
    
    _setupDuration: function () {
        if (!koios.isNumber(this.duration)) {
            this.duration = koiosUI.animationDurations[this.duration] || koiosUI.animationDurations.fast;
        }
    },
    
    _setupData: function () {
        var t = this,
            compStyle = window.getComputedStyle(this.node),
            norm = koiosUI._animationNormalizer;
        
        t.from = {};
        t.to = {};
        t.suffixes = {};
        t.keys = [];
        
        koios.each(this.css, function (key, value) {
            if (koios.contains(key, "transform")) {
                if (koiosUI.transform) {
                    norm.transformProps(t, key, value);
                }
                t.keys.push("transform");
            } else {
                var camelKey = koios.string.camelCase(key, "-");
                t.from[key] = norm.extract(camelKey, compStyle[camelKey]);
                t.suffixes[key] = norm.suffix(t.from[key], compStyle[camelKey], t.to[key], value);
                t.to[key] = norm.extract(camelKey, value);
                t.keys.push(key);
            }
        });
    },
    
    _add: function () {
        koiosUI.Animator.add(this);
    },
    
// =================================================
// Steps
    
    onStep: function () {
        var t = this,
            norm = koiosUI._animationNormalizer,
            defn = "",
            delta,
            progress,
            camelKey;
        
        this.progress = koios.easedLerp(this.ts, this.duration, this.easing);
        koios.each(this.keys, function (key) {
            if (koios.contains(key, "transform")) {
                var val = norm.stepValue(key, t.to, t.from, t.suffixes, t.progress);
                t.node.style[koiosUI.cssKeys.styleTransform] = val;
            } else {
                camelKey = koios.string.camelCase(key);
                t.node.style[camelKey] = norm.stepValue(key, t.to, t.from, t.suffixes, t.progress);
            }
        });
    },
    
    _step: function () {
        if (this.progress < 1) {
            return this.bind("onStep");
        }
        
        this.promise.fulfill(this);
        
        return null;
    },
    
// =================================================
// Playing
    
/**
If the animation is deferred (.deferred = true), you need to manually call .play()
to start it

@method play
*/
    
    play: function () {
        koios.timeout(this.name, this.bind("_add"), this.delay);
    }
});