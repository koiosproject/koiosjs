/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koiosui_animationsupport.js
Provides koiosUI.AnimationSupport
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module, koiosUI*/

/**
Provides the toolset to make vibrant, animated web apps using
KoiosJS

@module web/animator
*/

/**
A simple Mixin which adds this.animate() to any class that inherits it. It will automatically
animate this.domNode

@class koiosUI.AnimationSupport
@extends koios.Mixin
*/

koios.define({
    className: "koiosUI.AnimationSupport",
    extend: "koios.Mixin",
    
    mixin: function (theConstructor, thePrototype) {
    },
    
    mixinProperties: {
/**
Allows you to animate the domNode of an owned koiosUI.View

The usage of this method is the same as koiosUI.animate()

__Inherited from koiosUI.AnimationSupport__

__Requires the modules-web/animator module__

@method animate
@returns koios.Promise
*/
        animate: function (theAnimation) {
            return koiosUI.animate(this.domNode, theAnimation);
        }
    }
});

koiosUI.View.inherit("koiosUI.AnimationSupport");
