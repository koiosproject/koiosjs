/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koios_easing.js
Provides Additional koios.easing functions
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module, koiosUI*/

/**
Provides the toolset to make vibrant, animated web apps using
KoiosJS

@module web/animator
*/


// =================================================

function generateStep(steps) {
    return function (n) {
        return Math.round(n * steps) * (1 / steps);
    };
}

/* Bezier curve function generator. Copyright Gaetan Renaudeau. MIT License: http://en.wikipedia.org/wiki/MIT_License */
var generateBezier = (function () {
    function a(aA1, aA2) {
        return 1.0 - 3.0 * aA2 + 3.0 * aA1;
    }

    function b(aA1, aA2) {
        return 3.0 * aA2 - 6.0 * aA1;
    }
    function c(aA1) {
        return 3.0 * aA1;
    }

    function calcBezier(aT, aA1, aA2) {
        return ((a(aA1, aA2) * aT + b(aA1, aA2)) * aT + c(aA1)) * aT;
    }

    function getSlope(aT, aA1, aA2) {
        return 3.0 * a(aA1, aA2) * aT * aT + 2.0 * b(aA1, aA2) * aT + c(aA1);
    }

    return function (mX1, mY1, mX2, mY2) {
        var i;
        
        /* Must contain four arguments. */
        if (arguments.length !== 4) {
            return false;
        }

        /* Arguments must be numbers. */
        for (i = 0; i < 4; i += 1) {
            if (typeof arguments[i] !== "number" || isNaN(arguments[i]) || !isFinite(arguments[i])) {
                return false;
            }
        }

        /* X values must be in the [0, 1] range. */
        mX1 = Math.min(mX1, 1);
        mX2 = Math.min(mX2, 1);
        mX1 = Math.max(mX1, 0);
        mX2 = Math.max(mX2, 0);

        function getTForX(aX) {
            var aGuessT = aX,
                i,
                currentSlope,
                currentX;

            for (i = 0; i < 8; i += 1) {
                currentSlope = getSlope(aGuessT, mX1, mX2);

                if (currentSlope === 0.0) {
                    return aGuessT;
                }

                currentX = calcBezier(aGuessT, mX1, mX2) - aX;

                aGuessT -= currentX / currentSlope;
            }

            return aGuessT;
        }

        return function (aX) {
            if (mX1 === mY1 && mX2 === mY2) {
                return aX;
            } else {
                return calcBezier(getTForX(aX), mY1, mY2);
            }
        };
    };
}());

    /* Runge-Kutta spring physics function generator. Adapted from Framer.js, copyright Koen Bok. MIT License: http://en.wikipedia.org/wiki/MIT_License */
    /* Given a tension, friction, and duration, a simulation at 60FPS will first run without a defined duration in order to calculate the full path. A second pass
       then adjusts the time dela -- using the relation between actual time and duration -- to calculate the path for the duration-constrained animation. */
var generateSpringRK4 = (function () {

    function springAccelerationForState(state) {
        return (-state.tension * state.x) - (state.friction * state.v);
    }

    function springEvaluateStateWithDerivative(initialState, dt, derivative) {
        var state = {
            x: initialState.x + derivative.dx * dt,
            v: initialState.v + derivative.dv * dt,
            tension: initialState.tension,
            friction: initialState.friction
        };

        return { dx: state.v, dv: springAccelerationForState(state) };
    }

    function springIntegrateState(state, dt) {
        var a = {
                dx: state.v,
                dv: springAccelerationForState(state)
            },
            b = springEvaluateStateWithDerivative(state, dt * 0.5, a),
            c = springEvaluateStateWithDerivative(state, dt * 0.5, b),
            d = springEvaluateStateWithDerivative(state, dt, c),
            dxdt = 1.0 / 6.0 * (a.dx + 2.0 * (b.dx + c.dx) + d.dx),
            dvdt = 1.0 / 6.0 * (a.dv + 2.0 * (b.dv + c.dv) + d.dv);

        state.x = state.x + dxdt * dt;
        state.v = state.v + dvdt * dt;

        return state;
    }

    return function springRK4Factory(tension, friction, duration) {

        var initState = {
                x: -1,
                v: 0,
                tension: null,
                friction: null
            },
            path = [0],
            time_lapsed = 0,
            tolerance = 1 / 10000,
            DT = 16 / 1000,
            have_duration,
            dt,
            last_state;

        tension = parseFloat(tension) || 600;
        friction = parseFloat(friction) || 20;
        duration = duration || null;

        initState.tension = tension;
        initState.friction = friction;

        have_duration = duration !== null;

        /* Calculate the actual time it takes for this animation to complete with the provided conditions. */
        if (have_duration) {
            /* Run the simulation without a duration. */
            time_lapsed = springRK4Factory(tension, friction);
            /* Compute the adjusted time delta. */
            dt = time_lapsed / duration * DT;
        } else {
            dt = DT;
        }

        while (true) {
            /* Next/step function .*/
            last_state = springIntegrateState(last_state || initState, dt);
            /* Store the position. */
            path.push(1 + last_state.x);
            time_lapsed += 16;
            /* If the change threshold is reached, break. */
            if (!(Math.abs(last_state.x) > tolerance && Math.abs(last_state.v) > tolerance)) {
                break;
            }
        }

/* If duration is not defined, return the actual time required for completing this animation. Otherwise, return a closure that holds the  computed path and returns a snapshot of the position according to a given percentComplete. */
        return !have_duration ? time_lapsed : function (percentComplete) {
            return path[(percentComplete * (path.length - 1)) | 0];
        };
    };
}());

koios.mixin(koios.easing, {
    ease: generateBezier(0.25, 0.1, 0.25, 1.0),
    easeIn: generateBezier(0.42, 0.0, 1.00, 1.0),
    easeOut: generateBezier(0.00, 0.0, 0.58, 1.0),
    easeInOut: generateBezier(0.42, 0.0, 0.58, 1.0),
    spring: function (p) {
        return 1 - (Math.cos(p * 4.5 * Math.PI) * Math.exp(-p * 6));
    }
});

/* jQuery UI's Robert Penner easing equations. Copyright The jQuery Foundation. MIT License: https://jquery.org/license */

var baseEasings = {};

koios.each(["Quad", "Cubic", "Quart", "Quint", "Expo"], function (el, idx) {
    baseEasings[el] = function (p) {
        return Math.pow(p, idx + 2);
    };
});

koios.mixin(baseEasings, {
    Sine: function (p) {
        return 1 - Math.cos(p * Math.PI / 2);
    },

    Circ: function (p) {
        return 1 - Math.sqrt(1 - p * p);
    },

    Elastic: function (p) {
        return p === 0 || p === 1 ? p : -Math.pow(2, 8 * (p - 1)) * Math.sin(((p - 1) * 80 - 7.5) * Math.PI / 15);
    },

    Back: function (p) {
        return p * p * (3 * p - 2);
    },

    Bounce: function (p) {
        var pow2,
            bounce = 4;

        while (p < ((pow2 = Math.pow(2, --bounce)) - 1) / 11) {}
        return 1 / Math.pow(4, 3 - bounce) - 7.5625 * Math.pow((pow2 * 3 - 2) / 22 - p, 2);
    }
});

koios.each(baseEasings, function (key, theFn) {
    koios.easing["easeIn" + key] = theFn;
    koios.easing["easeOut" + key] = function (p) {
        return 1 - theFn(1 - p);
    };
    koios.easing["easeInOut" + key] = function (p) {
        return p < 0.5 ? theFn(p * 2) / 2 : 1 - theFn(p * -2 + 2) / 2;
    };
});