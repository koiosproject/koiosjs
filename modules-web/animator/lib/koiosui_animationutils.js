/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koiosui_animationutils.js
Provides utility methods for koiosUI
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module, koiosUI*/

/**
Provides the toolset to make vibrant, animated web apps using
KoiosJS

@module web/animator
*/

/**
@class koiosUI
*/

/**
A polyfilled version (with appropriate vendor prefixing) of window.requestAnimationFrame. We use
this internally for animations, but it's provided for you should you need it.

__Requires the modules-web/animator module__

@method requestAnimationFrame
@param {Function} theCallback - the method to execute with the animation frame
@returns Value
*/

/**
A polyfilled version (with appropriate vendor prefixing) of window.cancelAnimationFrame.

__Requires the modules-web/animator module__

@method cancelAnimationFrame
@param {Value} theID - the id returned from requestAnimationFrame to cancel.
@returns Value
*/

// =================================================
// Animation Frame

var gl = this;

(function () {
    var frameRate = Math.round(1000 / 60),
        name = "requestAnimationFrame",
        cancel = "cancelAnimationFrame",
        prefix = koios.Device.vendorPrefix(),
        
        _reqFrame =  function (theCallback) {
            return gl.setTimeout(theCallback, frameRate);
        },

        _canFrame = function (theID) {
            return gl.clearTimeout(theID);
        };

    if (gl[cancel]) {
        koiosUI.requestAnimationFrame = koios.bind(gl, name);
        koiosUI.cancelAnimationFrame = koios.bind(gl, cancel);
    }

    if (!koiosUI[cancel] && gl[prefix + koios.string.cap(cancel)]) {
        koiosUI[name] = koios.bind(gl, prefix + koios.string.cap(name));
        koiosUI[cancel] = koios.bind(gl, prefix + koios.string.cap(cancel));
    }

    if (!koiosUI[cancel]) {
        koiosUI[name] =  _reqFrame;
        koiosUI[cancel] = _canFrame;
    }
}());

// =================================================
// Animation Normalizer

koiosUI._animationNormalizer = {
    easingFromString: function (theString) {
        if (koios.easing[theString]) {
            return koios.easing[theString];
        } else if (koios.easing[koios.string.camelCase(theString, "-")]) {
            return koios.easing[koios.string.camelCase(theString, "-")];
        } else {
            return koios.easing.linear;
        }
    },
    
    extract: function (key, value) {
        if (koios.contains(this._colors, key)) {
            if (!koios.contains(value, "rgb")) {
                return koiosUI.color.hexToRgb(value);
            }
            return value;
        }
        
        return parseFloat(value) || 0;
    },
    
    suffix: function (fromVal, fromRaw, toVal, toRaw) {
        var pref = koios.string.trimWhitespace(fromRaw.replace(fromVal.toString(), ""));
        
        if (pref !== "") {
            return pref;
        }
        
        if (toVal) {
            pref = koios.string.trimWhitespace(toRaw.replace(toVal.toString(), ""));
        }
        
        return pref || "";
    },
  
// =================================================
// Color Support
    
    _colors: ["color", "backgroundColor", "borderColor", "borderTopColor", "borderRightColor", "borderBottomColor", "borderLeftColor", "outlineColor"],

    _colorStepValue: function (key, to, from, progress) {
        function indStepVal(to, from, progress) {
            var _delta = to - from,
                _progress = from + (progress * _delta);
            
            return Math.round(_progress);
        }
        
        function parts(val) {
            var clean = val.replace('rgb(', '').replace(")", "");
            return clean.split(",");
        }

        var _to = parts(to[key]),
            _from = parts(from[key]),
            i,
            vals = [],
            val = "";
        
        for (i = 0; i < 3; i += 1) {
            vals.push(indStepVal(parseInt(_to[i], 10), parseInt(_from[i], 10), progress));
        }
        
        val = "rgb(" + (vals.join(",")) + ")";
        return val;
    },
 
// =================================================
// Transform Support
        
/*
Break an individual component of a transform into a parsed object
*/
    
    transformPropValue: function (transformKey, valueParts) {
        var ret = {
            values: [],
            suffixes: []
        },
            t = this;
        
        koios.each(valueParts, function (value) {
            var val = t.extract(transformKey, value),
                suffix = t.suffix(val, value);
            ret.values.push(val);
            ret.suffixes.push(suffix);
        });
        
        return ret;
    },
    
/*
Break apart the transform into its component parts
*/
    
    transformProps: function (anim, animKey, animValue) {
        var fromValue = koiosUI.domTransforms(anim.node),
            toValue = koiosUI.transformsToObject(animValue),
            transformValue = {},
            t = this,
            keys = [],
            fromKeys = koios.object.keys(fromValue);

        koios.each(toValue, function (key, value, idx) {
            var val = value.split(","),
                transformObj = t.transformPropValue(key, val);
            
            anim.suffixes["transform-" + key] = transformObj.suffixes;
            transformValue[key] = transformObj.values;
            keys.push(key);
        });
        
        anim.to.transform = transformValue;
        transformValue = {};
        
        koios.each(keys, function (transformKey) {
            var rawVal = fromValue[transformKey] || "",
                val = rawVal.split(","),
                transformObj = t.transformPropValue(transformKey, val);
            
            transformValue[transformKey] = transformObj.values;
        });
        
        anim.from.transform = transformValue;
        
        koios.each(fromKeys, function (key) {
            if (!anim.to.transform[key]) {
                var rawVal = fromValue[key] || "",
                    val = rawVal.split(","),
                    transformObj = t.transformPropValue(key, val);
                
                anim.suffixes["transform-" + key] = transformObj.suffixes;
                anim.from.transform[key] = anim.to.transform[key] = transformObj.values;
            }
        });
    },
    
/*
Returns an eased transform css definition
*/
    
    _transformValue: function (key, to, from, suffixes, progress) {
        var fromTransformVal = from[key],
            toTransformVal = to[key],
            ret = "";
        
        koios.each(toTransformVal, function (key, values) {
            var val = "",
                suffix = suffixes["transform-" + key],
                mapped = [];
            
            koios.each(values, function (value, idx) {
                var _allFrom = fromTransformVal[key],
                    _from = _allFrom[idx] || 0,
                    _delta = value - _from,
                    _progress = _from + (progress * _delta);
                
                mapped.push(_progress + suffix[idx]);
            });
            
            val = key + "(" + mapped.join(",") + ")";
            ret += val + " ";
        });
                
        return ret;
    },

// =================================================
// Step Values
    
    stepValue: function (key, to, from, suffix, progress) {
        var _to = to[key],
            _from = from[key],
            _delta, // = _to - _from,
            _progress; // = _from + (progress * _delta);
        
        if (koios.contains(this._colors, key)) {
            return this._colorStepValue(key, to, from, suffix, progress);
        }
        
        if (key === "transform") {
            return this._transformValue(key, to, from, suffix, progress);
        }
        
        _delta = _to - _from;
        _progress = _from + (progress * _delta);
        _progress += suffix[key];
        
        return _progress;
    }
};

/**
A set of string to number conversions that allow for String named animation durations

    {
        "very fast": 200,
        "fast": 400,
        "slow": 700,
        "very slow": 1000
    }
    
You can add to this at anytime - and the String will be available via any koiosUI.Animation

__Requires the modules-web/animator module__

@attribute animationDurations
@type Object
*/

koiosUI.animationDurations = {
    "very fast": 200,
    "fast": 400,
    "slow": 700,
    "very slow": 1000
};