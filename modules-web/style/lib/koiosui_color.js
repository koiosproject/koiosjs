/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koiosui_color.js
Provides color manipulation methods.
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module, koiosUI*/

/**
If you include the optional modules-web/style module, koiosUI gets a color object
containing a handful of helpful color manipulation methods

@class koiosUI.color
*/

koiosUI.color = {
    change: function (theColor, theRatio, makeDarker) {
        // Trim whitespace, replace 3 digit hex.
        theColor = koios.string.trimWhitespace(theColor).replace(/^#?([a-f0-9])([a-f0-9])([a-f0-9])$/i, '#$1$1$2$2$3$3');
        
        var difference = Math.round(theRatio * 256) * (makeDarker ? -1 : 1),
        // Determine if input is RGB(A)
            rgb = theColor.match(new RegExp('^rgba?\\(\\s*' +
                '(\\d|[1-9]\\d|1\\d{2}|2[0-4][0-9]|25[0-5])' +
                '\\s*,\\s*' +
                '(\\d|[1-9]\\d|1\\d{2}|2[0-4][0-9]|25[0-5])' +
                '\\s*,\\s*' +
                '(\\d|[1-9]\\d|1\\d{2}|2[0-4][0-9]|25[0-5])' +
                '(?:\\s*,\\s*' +
                '(0|1|0?\\.\\d+))?' +
                '\\s*\\)$', 'i')),
            alpha = !!rgb && rgb[4] !== null ? rgb[4] : null,

        // Convert hex to decimal
            decimal = !!rgb ? [rgb[1], rgb[2], rgb[3]] : theColor.replace(
                /^#?([a-f0-9][a-f0-9])([a-f0-9][a-f0-9])([a-f0-9][a-f0-9])/i,
                function () {
                    return parseInt(arguments[1], 16) + ',' +
                        parseInt(arguments[2], 16) + ',' +
                        parseInt(arguments[3], 16);
                }
            ).split(/,/),
            returnValue;

        return !!rgb ?
                'rgb' + (alpha !== null ? 'a' : '') + '(' +
                Math[makeDarker ? 'max' : 'min'](parseInt(decimal[0], 10) + difference, makeDarker ? 0 : 255) + ', ' +
                Math[makeDarker ? 'max' : 'min'](parseInt(decimal[1], 10) + difference, makeDarker ? 0 : 255) + ', ' +
                Math[makeDarker ? 'max' : 'min'](parseInt(decimal[2], 10) + difference, makeDarker ? 0 : 255) +
                (alpha !== null ? ', ' + alpha : '') + ')' :
            // Return hex
                [
                    '#',
                    koios.pad(Math[makeDarker ? 'max' : 'min'](parseInt(decimal[0], 10) + difference, makeDarker ? 0 : 255).toString(16), 2),
                    koios.pad(Math[makeDarker ? 'max' : 'min'](parseInt(decimal[1], 10) + difference, makeDarker ? 0 : 255).toString(16), 2),
                    koios.pad(Math[makeDarker ? 'max' : 'min'](parseInt(decimal[2], 10) + difference, makeDarker ? 0 : 255).toString(16), 2)
                ].join('');
    },

/** 
Returns a lighter color based on theColor, with theRatio as the amount to change. 

@method lighter
@param {String} theColor - the color in RGB or Hex to start with.
@param {Number} theRatio - The ratio (0 to 1) of the change of color.
@returns String 
*/

    lighter: function (theColor, theRatio) {
        return this.change(theColor, theRatio, false);
    },

/** 
Returns a darker color based on theColor, with theRatio as the amount to change. 

@method darker
@param {String} theColor - the color in RGB or Hex to start with.
@param {Number} theRatio - The ratio (0 to 1) of the change of color.
@returns String 
*/

    darker: function (theColor, theRatio) {
        return this.change(theColor, theRatio, true);
    },
    
// =================================================
// Hex -> RGB
    
    _hexToR: function (h) {
        return parseInt((this._cutHex(h)).substring(0, 2), 16);
    },
    
    _hexToG: function (h) {
        return parseInt((this._cutHex(h)).substring(2, 4), 16);
    },
    
    _hexToB: function (h) {
        return parseInt((this._cutHex(h)).substring(4, 6), 16);
    },
    
    _cutHex: function (h) {
        return (h.charAt(0) === "#") ? h.substring(1, 7) : h;
    },

/** 
Returns a rgb() CSS string from a hex (#) CSS color definition 

@method hexToRgb
@param {String} hexVal - the CSS hex CSS color string 
@returns String 
*/

    hexToRgb: function (hexVal) {
        return "rgb(" + this._hexToR(hexVal) + "," + this._hexToG(hexVal) + "," + this._hexToB(hexVal) + ")";
    },

// =================================================
// Hex <- RGB

    _componentToHex: function (c) {
        var hex = c.toString(16);
        return hex.length === 1 ? "0" + hex : hex;
    },

    _rgbToHex: function (r, g, b) {
        return "#" + this._componentToHex(r) + this._componentToHex(g) + this._componentToHex(b);
    },

/** 
Returns a HEX value (with #) from an RGB() color definition. Does not work with RGBA at this point.

@method rgbToHex
@param {String} rgbVal - the RGB() CSS string to convert.
@returns String 
*/

    rgbToHex: function (rgbVal) {
        var plainVal = rgbVal.replace('rgb(', '').replace(')', ''),
            comp = plainVal.split(',');
        
        return this._rgbToHex(comp[0], comp[1], comp[2]);
    },

// =================================================
// Brightness
    
/** 
Returns the perceived brightness of a color (either hex or rgb)

@method brightness
@param {String} theColor - the color you want to know the perceived brightness of.
@returns Number 
*/

    brightness: function (theColor) {
        if (theColor.charAt(0) === "#") {
            theColor = this.hexToRgb(theColor);
        }

        var clean = theColor.replace('rgb(', '').replace(')', ''),
            parts = clean.split(','),
            brightness = ((parts[0] * 299) + (parts[1] * 587) + (parts[2] * 114)) / 1000;

        return brightness;
    }

};