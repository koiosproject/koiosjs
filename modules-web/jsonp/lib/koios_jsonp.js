/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koios_jsonp.js
Provides koios.JSONPRequest - a lightweight JSONP wrapper.
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module*/

/**
A module providing a helper class for JSONP requests (koios.JSONPRequest)

@module web/jsonp
*/

/**
koios.JSONPRequest handles JSONP requests by querying a remote URL. koios.JSONPRequest isn't aliased,
and should be generated via koios.jsonp()

    var promise = koios.jsonp("http://ip.jsontest.com/")
    
koios.jsonp() can take a single String (which will be used as the URL) or a full object definition. It will return a promise. 
The promise will be resolved with a koios.Response (if the Ajax webmodule is included) or just the raw response otherwise.

    promise.then(function (response) {
        // Do something with the response...
    });
    
@class koios.JSONPRequest
@extend koios.Class
@uses koios.EventSupport
*/

/**
The URL to query

@attribute url
@type String
*/

/**
The name of the callback parameter to pass onto the JSONP request (default is "callback")

@attribute callback
@type String
*/

/**
An optional charset to specify

(not currently implemented)

@attribute charset
@type String
*/

/**
Whether or not to allow cached response. Default is false - which will append a cache=koios.now()
GET parameter

@attribute useCache
@type BOOL
*/

/**
The number of milliseconds to wait until considering the JSONP Request "timed out" - default is 5000. Set to 0
(or less) to omit the timeout.

@attribute timeout
@type Number
*/

koios.define({
    className: "koios.JSONPRequest",
    extend: "koios.Class",
    inherit: ["koios.EventSupport"],
    ignoreAlias: true,
    
    url: "",
    callbackName: "callback",
    charset: null,
    useCache: false,
    timeout: 5000,
    
    $init: function () {
        this.promise = koios.promise();
        this.$super("$init", arguments);
        
        this.start();
    },
    
    start: function () {
        this.timeout = parseInt(this.timeout, 10);
        this.callback = "onJSONPReceive" + this.__id;
        this.src = this._buildUrl();
        
        window[this.callback] = this.bind("onResponse");
        
        this._setupScript();
        if (this.timeout > 0) {
            koios.timeout(this.callback, this.bind("onTimeout"), this.timeout);
        }
    },
    
// =================================================
// Private
    
    _buildUrl: function () {
        var parts = this.url.split("?"),
            uri = parts.shift() || "",
            query = koios.queryString(this.url).set(this.callbackName, this.callback);
        
        if (this.useCache) {
            query.set("cache", koios.now());
        }
        
        return uri + query.toString();
    },
    
    _setupScript: function () {
		this.scriptNode = document.createElement("script");
		this.scriptNode.setAttribute("src", this.src);
		this.scriptNode.setAttribute("async", "async");
		this.scriptNode.setAttribute("charset", (this.charset || "UTF-8"));

		this.scriptNode.onerror = this.bind("onError");
		document.getElementsByTagName('head').item(0).appendChild(this.scriptNode);
    },
    
    _removeScript: function () {
        document.getElementsByTagName('head').item(0).removeChild(this.scriptNode);
		this.scriptNode.onerror = null;
		this.scriptNode = null;
		this.destroyLater();
    },
    
// =================================================
// Script Events

    onError: function () {
        this.promise.reject("error");
        this._removeScript();
    },
    
    onResponse: function (response) {
        koios.clearTimeout(this.callback);
        this._removeScript();
        
        if (!this.promise.settled()) {
            if (koios.hasOwnProperty("Response")) {
                this.promise.fulfill(new koios.Response({text: koios.JSON.stringify(response), json: response}));
            } else {
                this.promise.fulfill(response);
            }
        }
    },
    
    onTimeout: function () {
        if (!this.promise.settled()) {
            this.promise.reject("timeout");
        }
        
        this._removeScript();
    },
    
// =================================================
// Garbage Collection
    
    destroyLater: function () {
        koios.defer(this.bind("destroy"));
    }
});

// =================================================

/**
@class koios
*/

/**
Returns a koios.Promise from a koios.JSONPRequest. 

    var promise = koios.jsonp("http://ip.jsontest.com/");
    
koios.jsonp() can take a single String (which will be used as the URL) or a full object definition. It will return a promise. 
The promise will be resolved with a koios.Response (if the Ajax webmodule is included) or just the raw response otherwise.

    promise.then(function (response) {
        // Do something with the response...
    });
    
__Requires the modules-web/jsonp module__

@method jsonp
@returns koios.Promise
*/

koios.jsonp = function (args) {
	if (koios.isString(args)) {
		args = {url: args};
	}

	var req = new koios.JSONPRequest(args);
	return req.promise;
};
