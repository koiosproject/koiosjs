/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koiosui_keyboardhandler.js
Provides koiosUI.KeyboardHandler
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module*/

koios.singleton({
    className: "koiosUI.KeyboardHandler",
    extend: "koios.Class",
    ignoreAlias: true,
    
    $init: function () {
        this.$super("$init", arguments);
        
        koios.on("koiosui:keypress", this.bind("onKeypress"));
        koios.on("koiosui:keydown", this.bind("onKeydown"));
        koios.on("koiosui:keyup", this.bind("onKeyup"));
    },
    
// =================================================
// Event Handlers
    
    onKeypress: function (event) {
        koios.log("on keypress");
    },
    
    onKeydown: function (event) {
        koios.log("on keydown");
    },
    
    onKeyup: function (event) {
    }
});
