/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koiosui_transform.js
Provides transform utility methods
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module, koiosUI*/

/**
Provides tools for adding CSS3 Transform properties to dom elements.

@module web/transform
*/

var cssTransformProps = ["transform", "-webkit-transform", "-moz-transform", "-ms-transform", "-o-transform"],
    styleTransformProps = ["transform", "webkitTransform", "MozTransform", "msTransform", "OTransform"],
    
    _canAccelerate = function () {
        if (koios.Device.isLessThan("android", 2)) {
            return false;
        }
        
        var ret = false;
        koios.each(["perspective", "WebkitPerspective", "MozPerspective", "msPerspective", "OPerspective"], function (el) {
            if (typeof document.body.style[el] !== "undefined") {
                ret = true;
            }
        });
        
        return ret;
    },
    
    _canTransform = function () {
        return Boolean(koiosUI.cssKeys.styleTransform);
    },
        
    _styleTransformProp = function () {
        var ret = null;
        
        koios.each(styleTransformProps, function (el) {
            if (typeof document.body.style[el] !== "undefined") {
                ret = el;
            }
        });
        
        return ret;
    },
        
    _cssTransformProp = function () {
        var idx = koios.indexOf(styleTransformProps, _styleTransformProp());
        if (idx > -1) {
            return cssTransformProps[idx];
        } else {
            return cssTransformProps[0];
        }
    };

// =================================================
// Animation Props - must be done after the DOM is ready in order to be able to query
// the document body node.

/**
@class koiosUI
*/

/**
A set of internally used css keys

__Requires the modules-web/transform module__

@attribute cssKeys
@type Object
*/

/**
Whether or not 3D Hardware Acceleration is available on the device.

__Requires the modules-web/transform module__

@attribute canAccelerate
@type BOOL
*/

/**
Whether or not CSS3 Transforms are available on the device.

__Requires the modules-web/transform module__

@attribute canTransform
@type BOOL
*/

koios.ready(function () {
    koiosUI.cssKeys = {
        styleTransform: _styleTransformProp(),
        cssTransform: _cssTransformProp()
    };

    if (koios.Device.vendorPrefix() === "webkit") {
        koiosUI.cssKeys.transition = "-webkit-transition";
    } else if (koios.Device.vendorPrefix() === "moz") {
        koiosUI.cssKeys.transition = "-moz-transition";
    } else {
        koiosUI.cssKeys.transition = "transition";
    }

    koiosUI.canAccelerate = _canAccelerate(); // Deprecated
    koiosUI.canTransform = _canTransform(); // Deprecated
});

// =================================================
// Transforming

/**
Returns an object containing all of the current transforms on theNode

__Requires the modules-web/transform module__

@method domTransforms
@param {String or DOMNode} theNode - the Node to query
@returns Object
*/

koiosUI.domTransforms = function (theNode) {
    var style = koios.css(theNode, koiosUI.cssKeys.cssTransform),
        ret = koiosUI.transformsToObject(style);

    return ret;
};

/**
Takes a CSS style definition used with transforms, and converts it to a key-value object

__Requires the modules-web/transform module__

@method transformsToObject
@param {String} theStyle - the CSS transform style
@returns Object
*/

koiosUI.transformsToObject = function (style) {
    var ret = {},
        els = style ? style.split(")") : [],
        elParts;
    
    koios.each(els, function (el) {
        el = koios.string.trimWhitespace(el);
        
        if (el.length > 0) {
            elParts = el.split("(");
            ret[elParts[0]] = elParts[1];
        }
    });
    
    return ret;
};

/**
Converts an Object containing transform key-value pairs into a CSS
appropriate definition (minus the CSS key itself)

__Requires the modules-web/transform module__

@method transformsToCss
@param {Object} theCss - the object to convert
@returns String
*/

koiosUI.transformsToCss = function (theCss) {
    var ret = "";
    
    koios.each(theCss, function (key, value) {
        ret += key + "(" + value + ")";
    });
    
    return ret;
};

/**
Converts a key-value object to a properly vendor prefixed style, and appends
the result to theNode's style.

    koiosUI.transform("#node", {translate: "40px 40px"});
    
__Requires the modules-web/transform module__

@method transform
@param {String or DOMNode} theNode - the Node to transform
@param {Object} theTransform - the transforms to apply
*/

koiosUI.transform = function (theNode, theTransform) {
    var transforms = koios.mixin(this.domTransforms(theNode), theTransform),
        css = this.transformsToCss(transforms);
    
    koios.css(theNode, koiosUI.cssKeys.cssTransform, css);
};
