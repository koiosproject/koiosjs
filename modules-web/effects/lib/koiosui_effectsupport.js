/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koiosui_effectsupport.js
Provides koiosUI.EffectSupport
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module, koiosUI*/

/**
Provides effects - koiosUI.attention(), .appear(), .disappear(). These
are based around Animate.css by daneden, with additional effects.

https://github.com/daneden/animate.css

@module web/effects
*/

/**
If the effects module is included, derived objects based on koiosUI.View will gain
.appear(), .attention() and .disappear() methods via koiosUI.EffectSupport

__Requires the modules-web/effects module__

@class koiosUI.EffectSupport
@extends koios.Mixin
*/

koios.define({
    className: "koiosUI.EffectSupport",
    extend: "koios.Mixin",
    
    mixin: function (theConstructor, thePrototype) {
        if (!thePrototype.effects) {
            thePrototype.effects = {};
        }
    },
    
    mixinProperties: {

/**
Calls koiosUI.appear() on this

__Requires the modules-web/effects module__

@method appear
@param {String} theEffect - the name of the effect to run
*/
        
        appear: function (theEffect) {
            if (!theEffect) {
                theEffect = this.effects.appear;
            }
            
            if (theEffect) {
                koiosUI.appear(this.domNode, theEffect);
            } else {
                this.show();
            }
        },

/**
Calls koiosUI.attention() on this

__Requires the modules-web/effects module__

@method attention
@param {String} theEffect - the name of the effect to run
*/
        
        attention: function (theEffect) {
            if (!theEffect) {
                theEffect = this.effects.appear;
            }
            
            if (theEffect) {
                koiosUI.attention(this.domNode, theEffect);
            }
        },
        
/**
Calls koiosUI.disappear() on this

__Requires the modules-web/effects module__


@method disappear
@param {String} theEffect - the name of the effect to run
*/
        
        disappear: function (theEffect) {
            if (!theEffect) {
                theEffect = this.effects.disappear;
            }
            
            if (theEffect) {
                koiosUI.disappear(this.domNode, theEffect);
            }
        }
    }
});

koiosUI.View.inherit("koiosUI.EffectSupport");