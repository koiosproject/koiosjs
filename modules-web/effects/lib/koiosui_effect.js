/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koiosui_effect.js
Provides koiosUI effects - .attention(), .appear(), .disappear()
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module, koiosUI*/

/**
Provides effects - koiosUI.attention(), .appear(), .disappear(). These
are based around Animate.css by daneden, with additional effects.

https://github.com/daneden/animate.css

@module web/effects
*/

var _effects = {
    _default: "fade",
    _base: "animated",
    _infinite: "infinite",
    _attention: {
        bounce: "bounce",
        flash: "flash",
        flip: "flip",
        pulse: "pulse",
        rubber: "rubberBand",
        rubberBand: "rubberBand",
        shake: "shake",
        swing: "swing",
        tada: "tada",
        wobble: "wobble"
    },
    _appear: {
        bounce: "bounceIn",
        bounceDown: "bounceInDown",
        bounceLeft: "bounceInLeft",
        bounceRight: "bounceInRight",
        bounceUp: "bounceInUp",
        fade: "fadeIn",
        fadeDown: "fadeInDown",
        fadeDownBig: "fadeInDownBig",
        fadeLeft: "fadeInLeft",
        fadeLeftBig: "fadeInLeftBig",
        fadeRight: "fadeInRight",
        fadeRightBig: "fadeInRightBig",
        fadeUp: "fadeInUp",
        fadeUpBig: "fadeInUpBig",
        flipX: "flipInX",
        flipY: "flipInY",
        lightSpeed: "lightSpeedIn",
        rotate: "rotateIn",
        rotateDownLeft: "rotateInDownLeft",
        rotateDownRight: "rotateInDownRight",
        rotateUpLeft: "rotateInUpLeft",
        rotateUpRight: "rotateInUpRight",
        slideUp: "slideInUp",
        slideDown: "slideInDown",
        slideLeft: "slideInLeft",
        slideRight: "slideInRight",
        roll: "rollIn"
    },
    _disappear: {
        bounce: "bounceOut",
        bounceDown: "bounceOutDown",
        bounceLeft: "bounceOutLeft",
        bounceRight: "bounceOutRight",
        bounceUp: "bounceOutUp",
        fade: "fadeOut",
        fadeDown: "fadeOutDown",
        fadeDownBig: "fadeOutDownBig",
        fadeLeft: "fadeOutLeft",
        fadeLeftBig: "fadeOutLeftBig",
        fadeRight: "fadeOutRight",
        fadeRightBig: "fadeOutRightBig",
        fadeUp: "fadeOutUp",
        fadeUpBig: "fadeOutUpBig",
        flipX: "flipOutX",
        flipY: "flipOutY",
        lightSpeed: "lightSpeedOut",
        rotate: "rotateOut",
        rotateDownLeft: "rotateOutDownLeft",
        rotateDownRight: "rotateOutDownRight",
        rotateUpLeft: "rotateOutUpLeft",
        rotateUpRight: "rotateOutUpRight",
        slideLeft: "slideOutLeft",
        slightRight: "slideOutRight",
        slideUp: "slideOutUp",
        slideDown: "slideOutDown",
        hinge: "hinge",
        roll: "rollOut"
    },
    
    addBase: function (theNode) {
        koios.addClass(theNode, this._base);
    },
    infinite: function (theNode, isInfinite) {
        if (isInfinite) {
            koios.addClass(theNode, this._infinite);
        }
    }
};

/**
@class koiosUI
*/

koiosUI.effect = function (theNode, theEffect, isInfinite) {
    _effects.addBase(theNode);
    _effects.infinite(theNode, isInfinite);
    
    if (!theEffect) {
        theEffect = _effects._default;
    }
    
    koios.addClass(theNode, theEffect);
    var listener = theNode.addEventListener(this.animationEvent(), function (event) {
        koios.removeClass(theNode, theEffect);
        theNode.removeEventListener(listener);
    });
};

/**
Runs an attention effect on theNode.

@method attention
@param {DOM Node} theNode - the dom node to call attention to
@param {String} theEffect - the name of the effect to provide
@param BOOL isInfinite - whether or not to continue looping the effect
*/

koiosUI.attention = function (theNode, theEffect, isInfinite) {
    if (_effects._attention[theEffect]) {
        this.effect(theNode, _effects._attention[theEffect], isInfinite);
    }
};

/**
Runs an appearance effect on theNode.

@method appear
@param {DOM Node} theNode - the dom node to call attention to
@param {String} theEffect - the name of the effect to provide
@param BOOL isInfinite - whether or not to continue looping the effect
*/

koiosUI.appear = function (theNode, theEffect, isInfinite) {
    this.effect(theNode, _effects._appear[theEffect], isInfinite);
};

/**
Runs an disappear effect on theNode.

@method disappear
@param {DOM Node} theNode - the dom node to call attention to
@param {String} theEffect - the name of the effect to provide
@param BOOL isInfinite - whether or not to continue looping the effect
*/

koiosUI.disappear = function (theNode, theEffect, isInfinite) {
    this.effect(theNode, _effects._disappear[theEffect], isInfinite);
};

koiosUI.animationEvent = function () {
    if (koios.Device._elStyle.hasOwnProperty("webkitAnimation")) {
        return "webkitAnimationEnd";
    } else {
        return "animationend";
    }
};