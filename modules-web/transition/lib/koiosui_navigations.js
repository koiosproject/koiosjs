/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koiosui_navigations.js
Provides basic koiosUI.NavigationTransition animations
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module, koiosUI*/

/**
Provides animated transitions used by koiosUI.NavigationPane

@module web/transition
*/

koiosUI.transitions = {};
koiosUI.registerTransition = function (name, transition) {
    koiosUI.transitions[name] = transition;
};

koiosUI.transitionForName = function (name) {
    if (koiosUI.transitions[name]) {
        return koiosUI.transitions[name];
    }
    
    return koiosUI.transitions.default;
};

koiosUI.navigations = {
    coverDown: function (sizing, offsetWidth, offsetHeight) {
        return {
            start: {
                from: sizing + " position: absolute; top: 0px;",
                to: sizing + " position: absolute; top : -" + offsetHeight + "px;"
            },
            animation: {
                from: {top: "0px"},
                to: {transform: "translate3d(0px, " + offsetHeight + "px, 0px)"}
            }
        };
    },
    coverLeft: function (sizing, offsetWidth, offsetHeight) {
        return {
            start: {
                from: sizing + " position: absolute; left: 0px;",
                to: sizing + " position: absolute; left : " + offsetWidth + "px;"
            },
            animation: {
                from: {left: "0px"},
                to: {transform: "translate3d(-" + offsetWidth + "px, 0px, 0px)"}
            }
        };
    },
    coverRight: function (sizing, offsetWidth, offsetHeight) {
        return {
            start: {
                from: sizing + " position: absolute; left: 0px;",
                to: sizing + " position: absolute; left : -" + offsetWidth + "px;"
            },
            animation: {
                from: {left: "0px"},
                to: {transform: "translate3d(" + offsetWidth + "px, 0px, 0px)"}
            }
        };
    },
    coverUp: function (sizing, offsetWidth, offsetHeight) {
        return {
            start: {
                from: sizing + " position: absolute; top: 0px;",
                to: sizing + " position: absolute; top : " + offsetHeight + "px;"
            },
            animation: {
                from: {top: "0px"},
                to: {transform: "translate3d(0px, -" + offsetHeight + "px, 0px)"}
            }
        };
    },
    fade: function (sizing, offsetWidth, offsetHeight) {
        return {
            start: {
                from: sizing + " opacity: 1; position: absolute;",
                to: sizing + " opacity: 0; position: absolute;"
            },
            animation: {
                from: {opacity: 0.5},
                to: {opacity: 1}
            }
        };
    },
    pushDown: function (sizing, offsetWidth, offsetHeight) {
        return {
            start: {
                from: sizing + " position: absolute; top: 0px;",
                to: sizing + " position: absolute; top : -" + offsetHeight + "px;"
            },
            animation: {
                from: {transform: "translate3d(0px, " + offsetHeight + "px, 0px)"},
                to: {transform: "translate3d(0px, " + offsetHeight + "px, 0px)"}
            }
        };
    },
    pushLeft: function (sizing, offsetWidth, offsetHeight) {
        return {
            start: {
                from: sizing + " position: absolute; left: 0px;",
                to: sizing + " position: absolute; left : " + offsetWidth + "px;"
            },
            animation: {
                from: {transform: "translate3d(-" + offsetWidth + "px, 0px, 0px)"},
                to: {transform: "translate3d(-" + offsetWidth + "px, 0px, 0px)"}
            }
        };
    },
    pushRight: function (sizing, offsetWidth, offsetHeight) {
        return {
            start: {
                from: sizing + " position: absolute; left: 0px;",
                to: sizing + " position: absolute; left : -" + offsetWidth + "px;"
            },
            animation: {
                from: {transform: "translate3d(" + offsetWidth + "px, 0px, 0px)"},
                to: {transform: "translate3d(" + offsetWidth + "px, 0px, 0px)"}
            }
        };
    },
    pushUp: function (sizing, offsetWidth, offsetHeight) {
        return {
            start: {
                from: sizing + " position: absolute; top: 0px;",
                to: sizing + " position: absolute; top : " + offsetHeight + "px;"
            },
            animation: {
                from: {transform: "translate3d(0px, -" + offsetHeight + "px, 0px)"},
                to: {transform: "translate3d(0px, -" + offsetHeight + "px, 0px)"}
            }
        };
    },
    revealDown: function (sizing, offsetWidth, offsetHeight) {
        return {
            start: {
                from: sizing + " position: absolute; top: 0px;  z-index:1;",
                to: sizing + " position: absolute; top: 0px;"
            },
            animation: {
                from: {transform: "translate3d(0px, " + offsetHeight + "px, 0px)"},
                to: {}
            }
        };
    },
    revealLeft: function (sizing, offsetWidth, offsetHeight) {
        return {
            start: {
                from: sizing + " position: absolute; top: 0px;  z-index:1;",
                to: sizing + " position: absolute; top: 0px;"
            },
            animation: {
                from: {transform: "translate3d(" + offsetWidth + "px, 0px, 0px)"},
                to: {}
            }
        };
    },
    revealRight: function (sizing, offsetWidth, offsetHeight) {
        return {
            start: {
                from: sizing + " position: absolute; top: 0px;  z-index:1;",
                to: sizing + " position: absolute; top: 0px;"
            },
            animation: {
                from: {transform: "translate3d(-" + offsetWidth + "px, 0px, 0px)"},
                to: {}
            }
        };
    },
    revealUp: function (sizing, offsetWidth, offsetHeight) {
        return {
            start: {
                from: sizing + " position: absolute; top: 0px;  z-index:1;",
                to: sizing + " position: absolute; top: 0px;"
            },
            animation: {
                from: {transform: "translate3d(0px, -" + offsetHeight + "px, 0px)"},
                to: {}
            }
        };
    }
};
