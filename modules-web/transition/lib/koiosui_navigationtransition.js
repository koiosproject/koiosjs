/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koiosui_navigationtransition.js
Provides the basic koiosUI.NavigationTransition
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module, koiosUI*/

/**
Provides animated transitions used by koiosUI.NavigationPane

@module web/transition
*/

/**
An abstract animation wrapper used by koiosUI.NavigationPane when transitions are
animated. You have no need to create one of these yourself.

@class koiosUI.NavigationTransition
@extends koios.Base
*/

koios.define({
    className: "koiosUI.NavigationTransition",
    extend: "koios.Base",
    
    fromView: null,
    toView: null,
    container: null,
    transitionName: "default",
    
    start: function () {
        var sizing = "height: " + this.container.domNode.offsetHeight + "px; width: " + this.container.domNode.offsetWidth + "px;",
            offsetWidth = this.container.domNode.offsetWidth,
            offsetHeight = this.container.domNode.offsetHeight,
            transition,
            promise;
        
        this.transition = this.transitionStyle(sizing, offsetWidth, offsetHeight);
        
        this.offsetWidth = offsetWidth;
        this.offsetHeight = offsetHeight;
        this.oldOwner = this.fromView.get("owner");
        
        this.container.setStyle("position", "relative");
        this.container.setStyle("overflow", "hidden");
        
        transition = this.transition;

        this.fromNode = this.create({
            className: "koiosUI.View",
            name: this.name + "-transition-from",
            cssStyle: transition.start.from
        });
        
        this.fromView.renderInto(this.fromNode.renderNode);
        this.fromNode.set("owner", this.container);
        
        this.toNode = this.create({
            className: "koiosUI.View",
            name: this.name + "-transition-to",
            cssStyle: transition.start.to
        });
        
        this.toView.renderInto(this.toNode.renderNode);
        this.toNode.set("owner", this.container);
                
        promise = koiosUI.animate({node: this.fromNode.renderNode, css: transition.animation.from, delay: 25, duration: 500, easing: "easeOutQuint"});
        promise.then(this.bind("done"));

        koiosUI.animate({node: this.toNode.renderNode, css: transition.animation.to, duration: 500, easing: "easeOutQuint"});
        
        return promise;
    },
    
    done: function () {
        koios.removeNode(this.fromNode.renderNode, this.fromView.domNode);
        koios.removeNode(this.toNode.renderNode, this.toView.domNode);
        koios.removeNode(this.container.renderNode, this.fromNode.domNode);
        koios.removeNode(this.container.renderNode, this.toNode.domNode);
        
        this.fromNode.destroy();
        this.toNode.destroy();
        
        this.toView.renderInto(this.container.renderNode);
    },
    
    reverse: function () {
//        koios.log("reversing");
//        koios.log(this);
//        var to = this.toView,
//            from = this.fromView;
//        
//        this.toView = from;
//        this.fromView = to;
//        this.direction = "reverse";
//        this.start();
    }
});

koiosUI.NavigationTransition.subclass = function (className, theConstructor, thePrototype) {
    koiosUI.registerTransition(thePrototype.transitionName, thePrototype.className);
};

koiosUI.registerTransition("default", "koiosUI.NavigationTransition");