/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koios_dom.js
Provides a fleet of DOM manipulation methods for KoiosJS
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module*/

/**
Provides a set of DOM manipulation tools for Koios

@module web/dom
*/

/**
@class koios
*/

/*
A method that simplifies the ability to use either a query string or an
actual node in koios DOM manipulation methods
*/

function _node(theNode) {
    if (koios.isString(theNode)) {
        return koios.query(theNode, true);
    } else {
        return theNode;
    }
}

// =================================================
// Attributes

/**
A convenience method that when theNode, theAttribute and theValue is passed in,
calls koios.setAttr - if no theValue is passed in, it's the same as koios.getAttr

__Requires the modules-web/dom module__

@method attr
@param {DOM Node or String} theNode - the DOM node or a query string for a node
@param {String} theAttribute - the name of the attribute to set or get
@param {String} theValue - the value to set theAttribute to
@returns DOM Node or Value
*/

koios.attr = function (theNode, theAttribute, theValue) {
    if (arguments.length === 3) {
        return koios.setAttr(theNode, theAttribute, theValue);
    } else {
        return koios.getAttr(theNode, theAttribute);
    }
};

/**
Sets an attribute on theNode.

    koios.setAttr("#node", "class", "classValue");
    // -> <div id="node" class="classValue">
    
__Requires the modules-web/dom module__

@method setAttr
@param {DOM Node or String} theNode - the DOM node or a query string for a node
@param {String} theAttribute - the name of the attribute to set
@param {String} theValue - the value to set theAttribute to
@returns DOM Node
*/

koios.setAttr = function (theNode, theAttribute, theValue) {
    var node = _node(theNode);
    if (!node) {
        return null;
    }
    
    if (arguments.length === 3) {
        node.setAttribute(theAttribute, theValue);
    } else {
        koios.removeAttr(theNode, theAttribute);
    }
    
    return node;
};

/**
Returns an attribute on theNode.

    // <div id="node" class="classValue">
    koios.getAttr("#node", "class");
    // -> "classValue"
    
__Requires the modules-web/dom module__

@method getAttr
@param {DOM Node or String} theNode - the DOM node or a query string for a node
@param {String} theAttribute - the name of the attribute to get
@returns Value
*/

koios.getAttr = function (theNode, theAttribute, theValue) {
    var node = _node(theNode);
    
    if (node && node.getAttribute) {
        return node.getAttribute(theAttribute);
    } else {
        return null;
    }
};

/**
Removes an attribute from theNode.

    // <div id="node" class="classValue">
    koios.removeAttr("#node", "class");
    // -> <div id="node">
    
__Requires the modules-web/dom module__

@method removeAttr
@param {DOM Node or String} theNode - the DOM node or a query string for a node
@param {String} theAttribute - the name of the attribute to remove
@returns DOM Node
*/

koios.removeAttr = function (theNode, theAttribute) {
    var node = _node(theNode);
    
    if (node) {
        node.removeAttribute(theAttribute);
        return node;
    } else {
        return null;
    }
};

// =================================================
// CSS Classes

/**
Returns an array of all classes on a DOM node's class attribute, or an empty
array if no classes are defined.

    // <div id="node" class="koios-fit koios-hidden">
    koios.allClasses("#node");
    // -> ["koios-fit", "koios-hidden"]
    
__Requires the modules-web/dom module__

@method allClasses
@param {DOM Node or String} theNode - the DOM node or a query string for a node
@returns Array
*/

koios.allClasses = function (theNode) {
    var node = _node(theNode),
        attr = koios.attr(theNode, "class");
    
    if (node && attr) {
        return attr.split(" ");
    } else {
        return [];
    }
};

/**
Returns whether or not a DOM node contains a given class

    // <div id="node" class="koios-fit koios-hidden">
    koios.hasClass("#node", "koios-hidden");
    // -> true
    koios.hasClass("#node", "other-class");
    // -> false
    
__Requires the modules-web/dom module__

@method hasClass
@param {DOM Node or String} theNode - the DOM node or a query string for a node
@param {String} theClass - the class to query for
@returns BOOL
*/

koios.hasClass = function (theNode, theClass) {
    var attr = koios.allClasses(theNode);
    return koios.contains(attr, theClass);
};

/**
Adds a CSS class to a DOM node and returns the node itself. Adding a class that the node already
contains will do nothing.

    // <div id="node" class="koios-fit">
    koios.addClass("#node", "koios-hidden");
    // -> <div id="node" class="koios-fit koios-hidden">

__Requires the modules-web/dom module__

@method addClass
@param {DOM Node or String} theNode - the DOM node or a query string for a node
@param {String} theClass - the class to add
@returns DOM Node
*/

koios.addClass = function (theNode, theClass) {
    var classes = koios.allClasses(theNode);
    
    if (!koios.contains(classes, theClass)) {
        classes.push(theClass);
        koios.attr(theNode, "class", classes.join(" "));
    }
    
    return _node(theNode);
};

/**
Removes a CSS class to a DOM node and returns the node. Remove a class that the node 
doesn't contain will do nothing.

    // <div id="node" class="koios-fit koios-hidden">
    koios.removeClass("#node", "koios-hidden");
    //  -> <div id="node" class="koios-fit">

__Requires the modules-web/dom module__

@method removeClass
@param {DOM Node or String} theNode - the DOM node or a query string for a node
@param {String} theClass - the class to remove
@returns DOM Node
*/

koios.removeClass = function (theNode, theClass) {
    var classes = koios.allClasses(theNode);
    
    if (koios.contains(classes, theClass)) {
        classes = koios.array.remove(classes, theClass);
        koios.attr(theNode, "class", classes.join(" "));
    }
    
    return _node(theNode);
};

/**
Addes a CSS class to a DOM node if the node doesn't contain it, removes the class if
the node does.

    // <div id="node" class="koios-fit">
    koios.toggleClass("#node", "koios-hidden")
    // -> <div id="node" class="koios-fit koios-hidden">
    koios.toggleClass("#node", "koios-hidden")
    // -> <div id="node" class="koios-fit">
    
__Requires the modules-web/dom module__

@method toggleClass
@param {DOM Node or String} theNode - the DOM node or a query string for a node
@param {String} theClass - the class to toggle
@returns DOM Node
*/

koios.toggleClass = function (theNode, theClass) {
    var classes = koios.allClasses(theNode);
    
    if (koios.contains(classes, theClass)) {
        return koios.removeClass(theNode, theClass);
    } else {
        return koios.addClass(theNode, theClass);
    }
};

// =================================================
// CSS Styles

/**
Returns a node's style attribute as a key-value object

    <div id="node" style="color:green; width:100px">
    koios.cssToObject("#node");
    // -> {color:"green", width:"100px"}

__Requires the modules-web/dom module__

@method cssToObject
@param {DOM Node or String} theNode - the DOM node or a query string for a node
@returns Object
*/

koios.cssToObject = function (theNode) {
    var css = koios.attr(theNode, "style"),
        ret = {};
    
    if (!css) {
        return {};
    }
    
    return koios.string.toObject(css, "css");
};

/**
.css() is an overloaded function.

It can be used to return the style definition of theNode (if no key or value is provided)

    <div id="node" style="color:green; width:100px">
    koios.css("#node");
    // -> "color:green; width: 100px"
    
It can be used to return a given css syle, depending on theKey.

    <div id="node" style="color:green; width:100px">
    koios.css("#node", "color");
    // -> "green"

It can be used to set a given key-value pairing of theNode's style

    <div id="node" style="color:green; width:100px">
    koios.css("#node", "color", "blue");
    // -> <div id="node" style="color:blue; width:100px">

Finally, it can be used to mix in a key-value object's set of definitions into the
style definition of a node

    <div id="node" style="color:green; width:100px">
    koios.css("#node", {color:"blue", height:"200px"});
    // -> <div id="node" style="color:blue; width:100px; height:200px">

__Requires the modules-web/dom module__

@method css
@param {DOM Node or String} theNode - the DOM node or a query string for a node
@param {Object or String} theKey - either an object to define the new css, or the key to get or set (optional)
@param {String} theValue - the value to set theKey to.
@returns Value or DOM Node
*/

koios.css = function (theNode, theKey, theValue) {
    if (arguments.length === 1) {
        return koios.attr(theNode, "style");
    }
    
    var obj = koios.cssToObject(theNode);
    
    if (arguments.length === 2 || !theValue) {
        if (koios.isString(theKey)) {
            return obj[theKey];
        } else {
            if (!koios.isEmpty(theKey)) {
                obj = koios.mixin(obj, theKey);
                koios.attr(theNode, "style", koios.object.toString(obj, "css"));
            }
            return _node(theNode);
        }
    }
    
    obj[theKey] = theValue;
    koios.attr(theNode, "style", koios.object.toString(obj, "css"));
    return _node(theNode);
};

/**
Safely calls window.getComputedStyle on theNode

    koios.getComputedStyle(node)
    koios.getComputedStyle("#node");
    
@method getComputedStyle
@returns Object
*/

koios.getComputedStyle = function (el) {
    var node = _node(el);
    
    return window.getComputedStyle(node);
};

/**
Removes a given key from theNode's style attribute

    <div id="node" style="color:green; width:100px">
    koios.removeCss("#node", "color");
    // -> <div id="node" style="width:100px">
    
__Requires the modules-web/dom module__

@method removeCss
@param {DOM Node or String} theNode - the DOM node or a query string for a node
@param {Object or String} theKey - the style key to remove
@returns DOM Node
*/

koios.removeCss = function (theNode, theKey) {
    var css = koios.cssToObject(theNode);
    delete css[theKey];
    
    koios.attr(theNode, "style", koios.object.toString(css, "css"));
    return _node(theNode);
};

// =================================================
// Dom Nodes & HTML

/**
Generates a DOM node of type theName, mixing in theParams as attributes
on the node

    koios.el("div", {id:"node", class:"koios-hidden", style:"background-color:green"});
    // -> <div id="node" class="koios-hidden" style="background-color:green">
    
__Requires the modules-web/dom module__

@method el
@param {DOM Node or String} theName - the node name to create
@param {Object or String} theParams - a key-value object to mixin as attributes on the node (optional)
@returns DOM Node
*/

koios.el = function (theName, theParams) {
    if (arguments.length < 2) {
        theParams = {};
    }
    
    var el = document.createElement(theName),
        key;
    
    koios.each(theParams, function (theKey, theValue) {
        el.setAttribute(theKey, theValue);
    });
    
    return el;
};

/**
.html() is an overloaded function.

If you pass in a single DOM node, or a query string to return a DOM node, it will return just
the inner HTML of the node.

    <div id="node">Koios Rocks!</div>
    koios.html("#node");
    // -> Koios Rocks!

If you pass in an additional String as the second parameter, it will set the DOM node's
inner HTML to theHTML and return the node.

    <div id="node">Waiting...</div>
    koios.html("#node", "Koios Rocks!");
    // -> <div id="node">Koios Rocks!</div>

If you pass in a third object, .html() will template theHTML using the parameters of theTemplate

    <div id="node">Waiting...</div>
    koios.html("#node", "${library} Rocks!", {library:"Koios"});
    // -> <div id="node">Koios Rocks!</div>


__Requires the modules-web/dom module__

@method html
@param {DOM Node or String} theNode - the DOM node or a query string for a node
@param {String} theHTML - the HTML to set the innerHTML of theNode to. (optional)
@param {Object} theTemplate - an object to template theHTML with. (optional)
@returns DOM Node or HTML
*/

koios.html = function (theNode, theHTML, theTemplate) {
    var node = _node(theNode);
    
    if (arguments.length === 1) {
        return (node) ? node.innerHTML : "";
    }
    
    if (arguments.length === 3) {
        theHTML = koios.string.template(theHTML, theTemplate);
    }
    
    if (node) {
        node.innerHTML = theHTML;
    }
        
    return node;
};

// =================================================
// Nodes/Child Nodes

/**
If the second parameter is a string, it will append the string to the innerHTML of theNode, otherwise
it will attempt to append the second value as a child node of the first.

__Requires the modules-web/dom module__

@method appendNode
@param {DOM Node or String} theNode - the object to append to.
@param {DOM Node or String} theAppend - the DOM node to append, or additional innerHTML to append.
@returns DOM Node
*/

koios.appendNode = function (theNode, theAppend) {
    var node = _node(theNode),
        innerHTML = "";
    
    if (!node) {
        return;
    }
    
    if (koios.isString(theAppend)) {
        innerHTML = node.innerHTML + theAppend;
        koios.html(node, innerHTML);
    } else {
        node.appendChild(theAppend);
    }
    
    return node;
};

/**
Returns the child nodes of theNode, or null if theNode is not found.

__Requires the modules-web/dom module__

@method containsNode
@param {DOM Node or String} theNode - the DOM node to get the child nodes of.
@returns Array
*/

koios.childNodes = function (theNode) {
    var node = _node(theNode);
    
    if (node) {
        return node.childNodes;
    } else {
        return null;
    }
};

/**
Returns whether or not theNode contains theContains

__Requires the modules-web/dom module__

@method containsNode
@param {DOM Node or String} theNode - the object to search in.
@param {DOM Node or String} theContains - the DOM node to query for.
@returns BOOL
*/

    
koios.containsNode = function (theNode, theContains) {
    var node = _node(theNode);

    if (node) {
        return node.contains(theContains);
    } else {
        return false;
    }
};

/**
Returns the first child node of theNode

__Requires the modules-web/dom module__

@method firstChild
@param {DOM Node or String} theNode - the DOM node.
@returns DOM Node
*/

koios.firstChild = function (theNode) {
    var children = koios.childNodes(theNode) || [];
    
    if (children.length > 0) {
        return children[0];
    }
    
    return null;
};

/**
Returns whether or not theNode is a DOM Node (HTMLElement)

__Requires the modules-web/dom module__

@method isNode
@param {Value} theNode - the object to determine if it is a DOM node.
@returns BOOL
*/

/*global HTMLElement*/

koios.isNode = function (theNode) {
    try {
        return theNode instanceof HTMLElement;
    } catch (error) {
        return (typeof theNode === "object") && (theNode.nodeType === 1) && (typeof theNode.style === "object") && (typeof theNode.ownerDocument === "object");
    }
};

/**
Returns whether or not theNodeList is a NodeList

@method isNodeList
@param {Object} theNodeList
@returns BOOL
*/

/* Copyright Martin Bohm. MIT License: https://gist.github.com/Tomalak/818a78a226a0738eaade */
koios.isNodeList = function (variable) {
    return typeof variable === "object" &&
        /^\[object (HTMLCollection|NodeList|Object)\]$/.test(Object.prototype.toString.call(variable)) &&
        variable.length !== undefined &&
        (variable.length === 0 || (typeof variable[0] === "object" && variable[0].nodeType > 0));
};

/**
Returns the last child node of theNode

__Requires the modules-web/dom module__

@method lastChild
@param {DOM Node or String} theNode - the DOM node.
@returns DOM Node
*/

koios.lastChild = function (theNode) {
    var children = koios.childNodes(theNode) || [];
    
    if (children.length > 0) {
        return children[children.length - 1];
    }
    
    return null;
};


/**
Removes theNode from theParentNode

__Requires the modules-web/dom module__

@method lastChild
@param {DOM Node or String} theParentNode - the node to remove theNode from.
@param {DOM Node} theNode - the node to remove.
*/

koios.removeNode = function (theParentNode, theNode) {
    var node = _node(theParentNode),
        childNode = _node(theNode);
    
    if (koios.containsNode(node, childNode)) {
        node.removeChild(childNode);
    }
};
