/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koios_query.js
Provides koios.query()
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module*/

/**
Provides a set of DOM manipulation tools for Koios

@module web/dom
*/

/**
@class koios
*/

/**
Queries the DOM

__Requires the modules-web/dom module__

@method query
*/

koios.query = function (theSearch, useFirst) {
    if (koios.isNode(theSearch)) {
        return theSearch;
    }
    
    var results = document.querySelectorAll(theSearch),
        arr = koios.toArray(results);
    
    if (arr.length === 0) {
        return null;
    }
    
    if (useFirst) {
        return arr[0];
    } else {
        return koios.dom(arr);
    }
};