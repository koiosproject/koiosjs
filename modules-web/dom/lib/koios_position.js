/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koios_dom.js
Provides a fleet of DOM manipulation methods for KoiosJS
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module, _node*/

/**
Provides a set of DOM manipulation tools for Koios

@module web/dom
*/

/**
@class koios
*/

// =================================================
// Scroll Position

koios.scrollPos = function () {
};

// =================================================
// Node Positioning & Sizing

/**
Safely returns the scrollTop value of theNode

__Requires the modules-web/dom module__

@method scrollTop
@param {DOM Node or String} theNode - the DOM node or a query string for a node
@returns Number
*/

koios.scrollTop = function (theNode) {
    var node = _node(theNode);
    
    if (node) {
        if (node.hasOwnProperty("scrollTop")) {
            return node.scrollTop;
        } else {
            return node.scrollY;
        }
    }
    
    return null;
};

/**
Safely returns the origin (an object containing an x value and a y value) of theNode

__Requires the modules-web/dom module__

@method position
@param {DOM Node or String} theNode - the DOM node or a query string for a node
@returns Object
*/


koios.position = function (theNode) {
    var frame = koios.frame(theNode);
    return frame ? frame.origin : null;
};

/**
Returns the height of theNode or null.

__Requires the modules-web/dom module__

@method height
@param {DOM Node or String} theNode - the DOM node or a query string for a node
@returns Number
*/

koios.height = function (theNode) {
    var node = _node(theNode);
    
    if (node) {
        return node.offsetHeight;
    } else {
        return null;
    }
};

/**
Returns the width of theNode or null.

__Requires the modules-web/dom module__

@method width
@param {DOM Node or String} theNode - the DOM node or a query string for a node
@returns Number
*/

koios.width = function (theNode) {
    var node = _node(theNode);
    
    if (node) {
        return node.offsetWidth;
    } else {
        return null;
    }
};

/**
Returns the size of theNode (an object containing a height and a width value) or null.

__Requires the modules-web/dom module__

@method size
@param {DOM Node or String} theNode - the DOM node or a query string for a node
@returns Object
*/

koios.size = function (theNode) {
    var frame = koios.frame(theNode);
    return frame ? frame.size : null;
};

/**
Safely returns the clientBoundingRect of theNode.

__Requires the modules-web/dom module__

@method clientRect
@param {DOM Node or String} theNode - the DOM node or a query string for a node
@returns Object
*/

koios.clientRect = function (theNode) {
    var node = _node(theNode),
        frame;
    
    if (node) {
        frame = node.getBoundingClientRect();
        return frame;
    }
    
    return null;
};

/**
Returns the frame of theNode. The frame consists of the origin (position), the
size, and the frame - an object containing the left, right, top and bottom values.

Returns null if theNode is not found.

__Requires the modules-web/dom module__

@method frame
@param {DOM Node or String} theNode - the DOM node or a query string for a node
@returns Object
*/

koios.frame = function (theNode) {
    var rect = koios.clientRect(theNode),
        frame;
    
    if (!rect) {
        return rect;
    }
    
    frame = {
        origin: {
            x: parseInt(rect.left, 10),
            y: parseInt(rect.top, 10)
        },
        frame: {
            left: rect.left,
            right: rect.right,
            top: rect.top,
            bottom: rect.bottom
        },
        size: {
            width: parseInt(rect.width, 10),
            height: parseInt(rect.height, 10)
        }
    };
    
    return frame;
};
