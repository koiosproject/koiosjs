/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koios_domwrapper.js
Provides a abstract wrapper returned when multiple DOM
nodes are returned from koios.query()
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module*/

/**
Provides a set of DOM manipulation tools for Koios

@module web/dom
*/

koios.define({
    className: "koios.DOMWrapper",
    extend: "koios.Class",
    alias: "koios.dom",
    
    $constructor: function (args) {
        this.nodes = args;
        this.$super("$constructor");
    }
});

var mixin = {},
    calls = ["attr", "setAttr", "getAttr", "removeAttr", "allClasses", "hasClass", "addClass", "removeClass", "toggleClass", "cssToObject", "css", "removeCss", "html", "scrollTop", "position", "height", "width", "size", "clientRect", "frame"];

function _makeFn(theCall) {
    return function () {
        var args = koios.toArray(arguments);
        return koios.array.map(this.nodes, function (theNode) {
            return koios[theCall].apply(koios, [theNode].concat(args));
        });
    };
}

koios.each(calls, function (theCall) {
    mixin[theCall] = _makeFn(theCall);
});


koios.DOMWrapper.mixin(mixin);