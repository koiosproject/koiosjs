/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koiosui_arrangersupport.js
Provides koiosUI.ArrangerSupport
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module, koiosUI*/

/**
Provides the toolset to make multiple layouts for the same app, depending
on screen width.

@module web/arranger
*/

/**
koiosUI.ArrangerSupport supplies a koiosUIArranger and it's requisite connections
for any object inheriting it. By default koiosUI.ViewController inherits it if the
modules-web/arranger module is included - otherwise handling responsive View Controllers
is up to you.

@class koiosUI.ArrangerSupport
@extend koios.Mixin
*/

koios.define({
    className: "koiosUI.ArrangerSupport",
    extend: "koios.Mixin",
    
    mixin: function (theConstructor, thePrototype) {
        thePrototype._initFn.push(this.createArranger);
    },
        
    createArranger: function () {
        
        if (this.layouts && this.layouts.length > 0) {
            this.renderNode = koios.el("div");
            var arr = new koiosUI.Arranger();
            
            koios.each(this.layouts, function (theLayout) {
                arr.addLayout(theLayout);
            });
            
            arr.updateLayouts();
            
            this.arranger = arr;
            this.arrange = arr.bind("arrange");
            this.on("render", this.bind("_renderArranger"));
        }
   
/**
The behind the scenes koiosUI.Arranger that supports multiple layouts per View Controller
depending on screen size. In order to prevent creating unnecessary objects, an arranger is only available if 
the View contains a layouts array.

__Inherited from the koiosUI.ArrangerSupport__

__Requires the modules-web/arranger module__

@attribute arranger
@type koiosUI.Arranger
*/
        
    },
    
    mixinProperties: {
        _renderArranger: function () {
            this.arranger.renderInto(this.domNode);
            this.addObject(this.arranger);
        },
        
/**
Returns all available layouts

__Inherited from the koiosUI.ArrangerSupport__

__Requires the modules-web/arranger module__

@method allLayouts
@returns Array
*/
        
        allLayouts: function () {
            return this.arranger.layouts;
        }
    }
});

koiosUI.ViewController.inherit("koiosUI.ArrangerSupport");