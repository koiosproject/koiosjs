/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koiosui_arrangerjs
Provides koiosUI.Arranger
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module*/

/**
Provides the toolset to make multiple layouts for the same app, depending
on screen width.

@module web/arranger
*/

/**
A koiosUI.Arranger allows you to have multiple layouts depending on window width. Support for
arrangers are filled into koiosUI.ViewController via koiosUI.ArrangerSupport

The arranger itself only handles resizing and arranging the contents within koiosUI.Panes. It does
not have it's own DOM node and you probably won't have any need to access the arranger directly.

@class koiosUI.Arranger
@extend koios.Base
*/

koios.define({
    className: "koiosUI.Arranger",
    extend: "koios.Base",
    
    $init: function () {
        this.layouts = {};
        this._layouts = [];
        this._presented = [];
        
        this.$super("$init", arguments);
        
        koios.addListener(window, "resize", this.bind("onWindowResize"));
    },
    
// =================================================
// Layouts
    
    addLayout: function (theLayout) {
        if (!theLayout.className) {
            theLayout.className = "koiosUI.Layout";
        }
        
        var layout = this.addObject(theLayout);
        
        this.layouts[layout.maxWidth] = layout;
    },
    
    currentLayout: function () {
        return this._currentLayout;
    },
    
    updateLayouts: function () {
        this._default = this.layouts["-1"];
        
        var keys = koios.object.keys(this.layouts).sort(function (a, b) { return parseInt(a, 10) > parseInt(b, 10); }),
            minVal = 0,
            t = this,
            winWidth = window.innerWidth,
            theLayout;
        
        koios.each(keys, function (theKey) {
            if (theKey === "-1") {
                return;
            }
            
            theLayout = t.layouts[theKey];
            theLayout.minWidth = parseInt(minVal, 10);
            minVal = theKey;
            t._layouts.push(theLayout);
            
            if (theLayout._fitsWidth(winWidth)) {
                t._currentLayout = theLayout;
            }
        });
        
        if (this._default) {
            this._default.minWidth = minVal;
            this._layouts.push(this._default);
        }
        
        if (!this._currentLayout) {
            this._currentLayout = this._default;
        }
    },
    
    _setCurrentLayout: function (theLayout) {
        this._currentLayout = theLayout;
        this.currentLayout().renderInto(this.targetNode);
        
        this.reflow();
        
        var owner = this.get("owner");
        if (owner && owner.emit) {
            owner.emit("layoutChange");
        }
    },
    
// =================================================
// Panes
    
    paneWithName: function (theName) {
        return this.currentLayout().paneWithName(theName);
    },
    
// =================================================
// Rendering
    
    renderInto: function (domNode) {
        this.targetNode = domNode;
        koios.css(this.targetNode, "display", "none");
        
        // Preload each of the layouts into the arranger target node to prevent flickering.
        // Not a great solution...
        
        koios.each(this._layouts, function (theLayout) {
            koios.appendNode(domNode, theLayout.domNode);
            koios.removeNode(domNode, theLayout.domNode);
        });
        
        koios.removeCss(this.targetNode, "display");
        this.currentLayout().renderInto(domNode);
    },
   
// =================================================
// Presenting
    
    _present: function (theElement, theTarget) {
        if (!theElement.isaSubclassOf) {
            theElement = this.create(theElement);
        }
        
        var pane = this.paneWithName(theTarget);
//        if (pane) {
            pane.present(theElement);
//        }
        
        return pane;
    },
    
    arrange: function (theElement, theTarget) {
        var pane = this._present(theElement, theTarget);
        this._presented.push({target: theTarget, pane: pane, element: theElement});
    },

// =================================================
// Updating
    
    reflow: function () {
        var paneWithName = this.bind("paneWithName");
        
        koios.each(this._presented, function (thePresented) {
            var pane = paneWithName(thePresented.target);
            pane.present(thePresented.element);
        });
    },
    
// =================================================
// Events
    
    onWindowResize: function () {
        var winWidth = window.innerWidth,
            layout = this.currentLayout(),
            _update = this.bind("_setCurrentLayout");
        
        if (!layout._fitsWidth(winWidth)) {
            koios.removeNode(this.targetNode, layout.domNode);
            koios.each(this._layouts, function (theLayout) {
                if (theLayout._fitsWidth(winWidth)) {
                    _update(theLayout);
                }
            });
        }
    }
    
});