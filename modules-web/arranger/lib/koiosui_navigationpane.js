/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koiosui_navigationpane.js
Provides koiosUI.NavigationPane
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module, koiosUI*/

/**
Provides the toolset to make multiple layouts for the same app, depending
on screen width.

@module web/arranger
*/

/**
A type of koiosUI.Pane that allows for animated transitions between views (typically koiosUI.ViewControllers)
presented into it.

@class koiosUI.NavigationPane
@extend koiosUI.Pane
*/

/**
The name of the transition to use by default between views presented in the NavigationPane. It can be overriden
if the presented view has its own .navigationTransition attribute

@attribute transition
@type String
*/

/**
Whether or not to add a navigation bar to present the back button within.

@attribute showsNavBar
@type BOOL
*/

koios.define({
    className: "koiosUI.NavigationPane",
    extend: "koiosUI.Pane",
    
    defaultDrawable: "koiosUI.Pane",
    transition: "pushLeft",
    reverseTransition: "pushRight",
    showsNavBar: true,
    
    views: [
        {name: "navbar", innerHTML: "Title", height: 60},
        {name: "renderTarget"}
    ],
    
    events: {
        render: "onRender"
    },
    
    onRender: function () {
        this.stack = [];
        
        if (!this.showsNavBar) {
            koios.defer(this.bind(function () {
                this.$("navbar").hide();
            }));
        }
    },

/*
If we present a next view controller with a transition attribute, it will override the
the transition attribute temporarily.
*/
    
    transitionStyle: function (transitionStyle, defaultTransition) {
        var val = (!koios.isEmpty(transitionStyle)) ? transitionStyle : defaultTransition;
        return (!koios.isEmpty(val)) ? val : "none";
    },
    
    transitionFunction: function (transitionVal) {
        if (koios.isFunction(transitionVal)) {
            return transitionVal;
        } else if (koios.isString(transitionVal)) {
            if (koiosUI.navigations && koiosUI.navigations[transitionVal]) {
                return koiosUI.navigations[transitionVal];
            }
        }
        
        return "none";
    },
    
    _present: function (fromView, toView) {
        koios.removeNode(this.$("renderTarget").renderNode, fromView.domNode);
        toView.renderInto(this.$("renderTarget").renderNode);
    },
    
    present: function (theView) {
        var previous = this._presented,
            transition,
            style,
            ctorName,
            fn,
            Ctor,
            promise;
        
        this._presented = theView;
        theView.navigationPane = this;
        
        if (this.stack.length === 1) {
            this.backButton = this.create({
                className: "koiosUI.Accessory",
                name: "backButton",
                effects: {
                    appear: "fadeUp",
                    disappear: "fadeDown"
                },
                events: {
                    tap: "dismiss"
                },
                cssStyle: "background-color:black; color:white; height: 60px; width: 60px",
                innerHTML: "back"
            });
            
            this.presentAccessory(this.backButton);
        } else {
            theView.renderInto(this.$("renderTarget").renderNode);
        }

        this.stack.push(theView);

        if (this.stack.length > 1) {
            style = this.transitionStyle(this._presented.transition, this.transition);
                        
            if (!style || style === "none") {
                return this._present(previous, this._presented);
            }
            
            ctorName = koiosUI.transitionForName(style);
            fn = this.transitionFunction(style);
            Ctor = koios.getConstructor(ctorName);
            
            if (!koios.isEmpty(Ctor)) {
                promise = new Ctor({
                    fromView: previous,
                    toView: this._presented,
                    container: this.$("renderTarget"),
                    transitionStyle: fn
                }).start();
            }
        }
    },
    
    dismiss: function () {
        if (this.stack.length < 2) {
            return;
        }
        
        var fromView = this.stack[this.stack.length - 1],
            toView = this.stack[this.stack.length - 2],
            style = this.transitionStyle(fromView.reverseTransition, this.reverseTransition),
            ctorName,
            fn,
            Ctor,
            promise,
            backButton = this.backButton;
        
        this._presented = toView;
        
        if (!style || style === "none") {
            return this._present(fromView, toView);
        }
        
        ctorName = koiosUI.transitionForName(style);
        fn = this.transitionFunction(style);
        Ctor = koios.getConstructor(ctorName);
        
        if (!koios.isEmpty(Ctor)) {
            promise = new Ctor({
                fromView: fromView,
                toView: toView,
                container: this.$("renderTarget"),
                transitionStyle: fn
            }).start();
            
            promise.then(function () {
                fromView.destroy();
            });
        }
        
        this.stack.pop();
        
        if (this.stack.length === 1) {
            this.backButton.disappear();
            this.backButton.once("animationend", function () {
                backButton.destroy();
            });
        }
    }
});