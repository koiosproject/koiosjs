/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koiosui_layout.js
Provides koiosUI.Layout
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module, koiosUI*/

/**
Provides the toolset to make multiple layouts for the same app, depending
on screen width.

@module web/arranger
*/

/**
A koiosUI.Layout is a view class used to define the layout of multiple panes, used 
in coordination with a koiosUI.Arranger

@class koiosUI.Layout
@extend koiosUI.View
*/

koios.define({
    className: "koiosUI.Layout",
    extend: "koiosUI.View",
    
    cssClass: ["koios-fit"],
    defaultDrawable: "koiosUI.Pane",
    
    $init: function () {
        this.$super("$init", arguments);
        
        if (!this.hasOwnProperty("maxWidth")) {
            this.maxWidth = -1;
        }
        
        if (!koios.isNumber(this.maxWidth)) {
            this.maxWidth = parseInt(this.maxWidth, 10);
        }
        
        this.attr("koios-layout", this.name);
    },
    
    paneWithName: function (theName) {
        var panes = [],
            el = null;
        
        panes = koios.array.filter(this.$(), function (theEl) {
            return theEl.isaSubclassOf("koiosUI.Pane");
        });
        
        koios.each(panes, function (thePane) {
            var resp = thePane.paneWithName(theName);
            if (resp) {
                el = resp;
            }
        });
        
        return el;
    },
    
    _fitsWidth: function (theWidth) {
        if (this.maxWidth > -1) {
            return (theWidth > this.minWidth && theWidth < this.maxWidth);
        } else {
            return (theWidth > this.minWidth);
        }
    }
});