/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koiosui_layout.js
Provides koiosUI.Layout
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module, koiosUI*/

/**
Provides the toolset to make multiple layouts for the same app, depending
on screen width.

@module web/arranger
*/

/**
A koiosUI.Pane is a building block piece of a koiosUI.Layout. As you define layouts,
you will invariably create panes in order to tell the koiosUI.Arranger how to order
the presented information

@class koiosUI.Pane
@extend koiosUI.View
*/

/**
The name of the pane, used to arrange content via the arranger.present(view, paneName) method

This can be either a String or an Array - allowing you to fold multiple targets into the same
pane for smaller layouts.

@attribute paneName
@type String or Array
*/

koios.define({
    className: "koiosUI.Pane",
    extend: "koiosUI.View",
    
    defaultDrawable: "koiosUI.Pane",

    $init: function () {
        this.$super("$init", arguments);
        
        if (koios.exists(this.paneName) && koios.isString(this.paneName)) {
            this.paneName = [this.paneName];
        }
    },
    
    paneWithName: function (theName) {
        if (koios.contains(this.paneName, theName)) {
            return this;
        }
        
        var panes = [],
            el = null;
        
        panes = koios.array.filter(this.$(), function (theEl) {
            // Temporary fix for something...
            
            if (theEl) {
                return theEl.isaSubclassOf("koiosUI.Pane");
            } else {
                return false;
            }
        });
        
        koios.each(panes, function (thePane) {
            var resp = thePane.paneWithName(theName);
            if (resp) {
                el = resp;
            }
        });
        
        return el;
    },
    
    present: function (theView) {
        if (this._presented) {
            koios.removeNode(this.renderNode, this._presented.domNode);
        }
        
        this._presented = theView;
        theView.renderInto(this.renderNode);
    }
});