/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koiosui_scrollanimator.js
Provides koiosUI.ScrollAnimator
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module, koiosUI*/

koios.define({
    className: "koiosUI.ScrollAnimator",
    extend: "koios.Class",
    inherit: ["koios.EventSupport"],
    
    x: 0,
    x0: 0,
    y: 0,
    y0: 0,
    
    _frame: 10,
    
// =================================================
// Passed in by the scroller
    
    onDragStart: function (event) {
        this.touch = true;
        this.bounds = this.scroller.scrollBounds();
        this.position = this.scroller.scrollPosition();
        this.vertical = this.scroller._vertical();
        this.horizontal = this.scroller._horizontal();
        
        this.x = this.position.left;
        this.y = this.position.top;
                
        this.evX = event.pageX;
        this.evY = event.pageY;
        
        this._x = this.dx = -this.x;
        this._y = this.dy = -this.y;
        
        this.emit("scrollstart");
    },
    
    onDragMove: function (event) {
        var dx = this.horizontal ? event.pageX - this.evY : 0,
            dy = this.vertical ? event.pageY - this.evY : 0;
        
        this.dx = dx + this._x;
        this.dy = dy + this._y;
        
        // Damping
        this.dx = this.boundaryX(this.dx, koiosUI.dragDamping);
        this.dy = this.boundaryY(this.dy, koiosUI.dragDamping);
        
        this.startAnimating();
    },
    
    onDragEnd: function (event) {
        if (this.touch) {
            var scalar = 0.5;

            this.x = this.dx;
			this.x0 = this.x - (this.x - this.x0) * scalar;

			this.y = this.dy;
			this.y0 = this.y - (this.y - this.y0) * scalar;
        }
        
        this.touch = false;
    },
    
    onFlick: function (event) {
        koios.log(event.deltaX + " || " + event.deltaY);
        
        var v = event.deltaY > 0 ? Math.min(2, event.deltaY) : Math.max(-2, event.deltaY);
        this.y = this.y0 + v * koiosUI.flickScalar;
        
        this.startAnimating();
    },
    
// =================================================
// Damping

    damping: function (theValue, theOrigin, co, theSign) {
		var epsilon = 0.5,
            dv = theValue - theOrigin;
        
		if (Math.abs(dv) < epsilon) {
			return theOrigin;
		}
		return theValue * theSign > theOrigin * theSign ? co * dv + theOrigin : theValue;
    },
    
    boundary: function (pos, topBound, botBound, co) {
        return this.damping(this.damping(pos, topBound, co, 1), botBound, co, -1);
    },
    
    boundaryX: function (x, co) {
        return this.boundary(x, this.bounds.left, -this.bounds.right, co);
    },
    
    boundaryY: function (y, co) {
        return this.boundary(y, this.bounds.top, -this.bounds.bottom, co);
    },

// =================================================
// Physics

	constrain: function () {
		var x = this.boundaryX(this.x, koiosUI.springDamping),
            y = this.boundaryY(this.y, koiosUI.springDamping);

        if (x !== this.x) {
			this.x0 = x - (this.x - this.x0) * koiosUI.snapFriction;
			this.x = x;
		}

		if (y !== this.y) {
			this.y0 = y - (this.y - this.y0) * koiosUI.snapFriction;
			this.y = y;
		}
	},

	verlet: function (p) {
		var x = this.x,
            y = this.y;
        
		this.x += x - this.x0;
		this.x0 = x;

        this.y += y - this.y0;
		this.y0 = y;
	},

	friction: function (ex, ex0, co) {
		var dp = this[ex] - this[ex0],
            c = Math.abs(dp) > koiosUI.frictionEpsilon ? co : 0;
        
		this[ex] = this[ex0] + c * dp;
	},

    simulateScroll: function (t) {
        while (t >= this._frame) {
            t -= this._frame;
            if (!this.touch) {
                this.constrain();
            }
            
            this.verlet();
			this.friction('y', 'y0', koiosUI.frictionDamping);
			this.friction('x', 'x0', koiosUI.frictionDamping);
        }
        
        return t;
    },
    
// =================================================
// Animation
    
/**
We no longer do the animation in the animator - just decide the actual scroll value and
emit a scroll event containing the x and y positions as they change. We allow the scroller's
.handler to actually update the scroll
*/
    
    startAnimating: function () {
        if (!this._fn) {
            this.animate();
        }
    },
    
    animate: function () {
        this.stopAnimating(); // Force kill any possible other animations going on.
        
        var t0 = koios.now(),
            t = 0,
            x0,
            y0,
            
            fn = this.bind(function () {
                var t1 = koios.now(),
                    dt = t1 - t0;
                
                t0 = t1;
                
                this._fn = koiosUI.requestAnimationFrame(fn);
                
                if (this.touch) {
                    this.x0 = this.x = this.dx;
                    this.y0 = this.y = this.dy;
                }
                
//                t += Math.max(16, dt);
                t = 20;
                
                t = this.simulateScroll(t);

                if (y0 !== this.y || x0 !== this.x) {
                    this._scroll();
                } else if (!this.touch) {
                    this.stopAnimating(true);
                    this._scroll();
                }
                y0 = this.y;
                x0 = this.x;

            });
        
        this._fn = koiosUI.requestAnimationFrame(fn);
    },
    
    stopAnimating: function (emit) {
        this._fn = koiosUI.cancelAnimationFrame(this._fn);
        if (emit) {
            this.emit("scrollend");
        }
    },
    
    _scroll: function () {
        this.scroller._scroll({x: this.x, y: this.y, y0: this.y0, touch: this.touch});
    }
});
    