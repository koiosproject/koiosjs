/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koiosui_touchscrollhandler.js
Provides koiosUI.TouchScrollHandler
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module, koiosUI*/

koios.define({
    className: "koiosUI.TouchScrollHandler",
    extend: "koiosUI.ScrollHandler",
        
    _extend: function (scroller) {
        this.$super("_extend", arguments);
        
        this.domNode.style.overflowY = "auto";
    },

    shouldDrag: function (x, y) {
        return true;
    },
    
    _updateDefns: function () {
        this._styleKey = koiosUI.cssKeys.styleTransform;
        this._hasAccelerate = koios.Device.hasAccelerate;
        this._hasTransform = koios.Device.hasTransform;
    },
    
// =================================================
// Events
    
    onDragStart: function (event) {
        if (!this._styleKey) {
            this._updateDefns();
        }
                
        this._touch = true;
    },

    onDragMove: function (event) {
        if (!this._touch) {
            return false;
        }
    },

    onDragEnd: function (event) {
        this._touch = false;
    },
    
// =================================================
// Animator Events
    
    onScrollStart: function (event) {
    },
    
    onScroll: function (event) {
        var x = event.x,
            y = event.y;
        
//        if (!event.touch) {
//            koios.log(koios.JSON.stringify(event));
//        }
        
        this._update(x, y);
    },
    
    onScrollEnd: function (event) {
    },
    
    _update: function (x, y) {
        if (this._isOverflowY(y)) {
            this._overscrollY(y);
        } else {
            this.transform(0, 0);
            this.setScrollTop(-y);
        }
    },
    
    _isOverflowY: function (y) {
//        koios.log(y);
        return (y > 0) || (y < -this.scroller._bounds.bottom);
    },
    
    _overscrollY: function (y) {
        var dif;
        
        if (y > 0) {
            this.setScrollTop(0);
            this.transform(0, y);
        } else {
            dif = y + this.scroller._bounds.bottom;
            
            this.setScrollTop(this.scroller._bounds.bottom);            
            this.transform(0, dif);
        }
    },
    
// =================================================
// Scroll Top / Scroll Left

// TODO: Offload to base class?
    
    setScrollTop: function (y) {
        this.domNode.scrollTop = y;
    },
    
    setScrollLeft: function (x) {
        this.domNode.scrollLeft = x;
    },
    
// =================================================
// Transform
    
    transform: function (x, y) {
//        window.alert("transform");
        this.renderNode.style[this._styleKey] = "translate(" + x + "px," + y + "px)"; //, 0px)";
    }
});
