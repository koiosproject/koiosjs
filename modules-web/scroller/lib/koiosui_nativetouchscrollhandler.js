/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koiosui_nativetouchscrollhandler.js
Provides koiosUI.NativeTouchScrollHandler
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module, koiosUI*/

koios.define({
    className: "koiosUI.NativeTouchScrollHandler",
    extend: "koiosUI.ScrollHandler",
    
    _extend: function (theScroller) {
        this.$super("_extend", arguments);

        theScroller.setStyle("-webkit-overflow-scrolling", "touch");
        theScroller.setStyle("overflow-y", "scroll");
        theScroller.setStyle("z-index", "0");
        theScroller.renderNode.style.zIndex = 0;
        
        if (koios.Device.is("ios")) {
            theScroller.on("tapstart", this.bind("tapStart"));
        }

        theScroller.draggable = false;
        
        return this;
    },
        
    tapStart: function (event) {
// This prevents the overflow scroll dragging the whole window down on iOS.
        if (this.domNode.scrollTop === 0) {
            this.domNode.scrollTop = 1;
        }
    }
});
