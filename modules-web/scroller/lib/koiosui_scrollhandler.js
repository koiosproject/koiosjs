/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koiosui_scrollhandler.js
Provides koiosUI.ScrollHandler
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module, koiosUI*/

koios.define({
    className: "koiosUI.ScrollHandler",
    extend: "koios.Class",
    
    _extend: function (scroller) {
        this.scroller = scroller;
        this.domNode = scroller.domNode;
        this.renderNode = scroller.renderNode;
        this.animator = scroller.animator;
        
        this.animator.on("scrollstart", this.bind("onScrollStart"));
        this.animator.on("scroll", this.bind("onScroll"));
        this.animator.on("scrollend", this.bind("onScrollEnd"));

        return this;
    },

    onDragStart: koios.noop,
    onDragMove: koios.noop,
    onDragEnd: koios.noop,
    onFlick: koios.noop,
    onScrollStart: koios.noop,
    onScroll: koios.noop,
    onScrollEnd: koios.noop,
    
    setScrollLeft: function (left) {
    },
    
    setScrollRight: function (right) {
    },
});