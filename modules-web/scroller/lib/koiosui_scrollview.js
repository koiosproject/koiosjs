/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koiosui_scrollview.js
Provides koiosUI.ScrollView
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module, koiosUI*/

/**
Provides cross-platform scrolling options

@module web/scroller
*/

/**
A koiosUI.ScrollView provides a touch and mouse-wheel, platform approrpiate scrolling
viewport usable within KoiosJS apps.

TODO: Document

@class koiosUI.ScrollView
@extend koiosUI.View
*/

koios.define({
    className: "koiosUI.ScrollView",
    extend: "koiosUI.View",
    cssClass: ["koios-scroller"],
    draggable: true,
    
    properties: {
        horizontal: {value: "hidden", validate: koios.isString},
        vertical: {value: "default", validate: koios.isString},
        scrollLeft: {value: 0, validate: koios.isNumber, get: function () {
            return this.domNode.scrollLeft;
        }},
        scrollTop: {value: 0, validate: koios.isNumber, get: function () {
            return this.domNode.scrollTop;
        }}
    },
    
    events: {
        dragstart: "onDragStart",
        dragmove: "onDragMove",
        dragend: "onDragEnd",
        flick: "onFlick",
        render: "onRender",
        "scrollLeft:change": "_updateScrollLeft",
        "scrollTop:change": "_updateScrollTop"
    },
    
    statics: {
        optimalScrollHandler: function () {
            if (koios.Device.is("ios") || koios.Device.is("androidChrome")) {
                return new koiosUI.NativeTouchScrollHandler();
            }
            
            return new koiosUI.TouchScrollHandler();
        }
    },
    
    $init: function () {
        this.renderNode = koios.el("div", {"class": "koios-scroller-inner", "onclick": "koios.preventDefault(arguments[0]);"});
        this.$super("$init", arguments);
        
//        this.renderNode.addEventListener("touchstart", this.bind("onDragStart"), false);
//        this.renderNode.addEventListener("touchmove", this.bind("onDragMove"), false);
//        this.renderNode.addEventListener("touchend", this.bind("onDragEnd"), false);
        
        this.animator = new koiosUI.ScrollAnimator();
        this.animator.scroller = this;

        this.handler = koiosUI.ScrollView.optimalScrollHandler();
        this.handler._extend(this);
             
        this.animator.on("scrollstart", this.bind("onScrollStart"));
        this.animator.on("scroll", this.bind("onScroll"));
        this.animator.on("scrollend", this.bind("onScrollEnd"));

        koios.appendNode(this.domNode, this.renderNode);
        
        koios.defer(this.bind("_updateBounds"));
        this.windowListener = koios.on("koiosui:resize", this.bind("_updateBounds"));
    },
    
    shouldDrag: function (dx, dy) {
        return (koios.isFunction(this.handler.shouldDrag)) ? this.handler.shouldDrag(dx, dy) : true;
    },
     
// =================================================
// Event Handlers
    
    onDragStart: function (event) {
//        event.preventDefault();
//        event.stopPropagation();
        
        this._updateBounds();
        this.handler.onDragStart(event);
        return this.animator.onDragStart(event);
    },

    onDragMove: function (event) {
//        event.preventDefault();
//        event.stopPropagation();

        this.handler.onDragMove(event);
        return this.animator.onDragMove(event);
    },

    onDragEnd: function (event) {
//        event.preventDefault();
//        event.stopPropagation();

        this.handler.onDragEnd(event);
        return this.animator.onDragEnd(event);
    },

    onFlick: function (event) {
        this.handler.onFlick(event);
        return this.animator.onFlick(event);
    },
    
    onRender: function () {
        koios.defer(this.bind("_updateBounds"));
    },
    
    _updateScrollLeft: function (left) {
        this.handler.setScrollTop(left);
    },
    
    _updateScrollTop: function (top) {
        this.handler.setScrollTop(top);
    },
    
    onScrollStart: function () {
        this.emit("scrollstart");
    },
    
    onScroll: function () {
        this.emit("scroll");
    },
    
    onScrollEnd: function () {
        this.emit("scrollend");
    },
    
// =================================================
// CSS Classes
        
/**
We have to prevent any flexbox classes from being added in order
to keep the inner renderNode from messing up.
*/
    
    addClass: function (theClass) {
        if (!koios.contains(theClass, "koios-flexbox")) {
            koios.addClass(this.domNode, theClass);
        }
    },
        
    removeClass: function (theClass) {
        if (!koios.contains(theClass, "koios-flexbox")) {
            koios.removeClass(this.domNode, theClass);
        }
    },
        
    toggleClass: function (theClass) {
        if (!koios.contains(theClass, "koios-flexbox")) {
            koios.toggleClass(this.domNode, theClass);
        }
    },

// =================================================
// Directions
   
// TODO: Update to handle actual sizing
    _horizontal: function () {
        var h = this.get("horizontal");
        return (h === "default") || (h === "auto");
    },
    
// TODO: Update to handle actual sizing
    _vertical: function () {
        var v = this.get("vertical");
        return (v === "default") || (v === "auto");
    },

    _scroll: function (event) {
        var handler = this.handler;
        
//        koios.log(event);
        handler.onScroll(event);
    },
    
// =================================================
// Bounds & Positioning
    
    _updateBounds: function () {
        var size = this.scrollSize();
        
        this._bounds = {
            scrollHeight: size.height,
            scrollWidth: size.width,
            top: 0,
            left: 0,
            bottom: Math.max(0, this.domNode.scrollHeight - this.domNode.clientHeight),
            right: Math.max(0, this.domNode.scrollWidth - this.domNode.clientWidth)
        };
        
        this._bounds.position = this.scrollPosition();
    },
    
    scrollBounds: function () {
        this._updateBounds();
        return {
            top: this._bounds.top,
            left: this._bounds.left,
            bottom: this._bounds.bottom,
            right: this._bounds.right
        };
    },
    
    scrollPosition: function () {
        return {
            top: this.get("scrollTop"),
            left: this.get("scrollLeft")
        };
    },
    
    scrollSize: function () {
        return {
            height: this.domNode.scrollHeight,
            width: this.domNode.scrollWidth
        };
    },
    
    scrollLeft: function () {
        return this.get("scrollLeft");
    },
    
    scrollTop: function () {
        return this.get("scrollTop");
    },

// TODO: animated
    scrollTo: function (x, y, animated) {
        this.set("scrollLeft", x);
        this.set("scrollTop", y);
    },
    
// TODO: animated
    scrollToX: function (x, animated) {
        this.set("scrollLeft", x);
    },
    
// TODO: animated
    scrollToY: function (y, animated) {
        this.set("scrollTop", y);
    },
    
// =================================================
// Garbage Collection
    
    destroy: function () {
        this.handler.destroy();
        this.$super("destroy", arguments);
    }
});

