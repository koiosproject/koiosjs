/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koiosui_mousevents.js
Provides koiosUI.EventHandler routing for mouse events
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module, koiosUI*/

(function () {
//    var _events = ["mousedown", "mouseup", "mouseover", "mouseout", "mousemove", "mousewheel", "click", "dblclick",
//                   "keydown", "keyup", "keypress"],
    var _events = ["mousedown", "mouseup", "mousemove", "click"],
    
        _noTarget = ["keydown", "keyup", "keypress"];
    
    koios.each(_events, function (theEvent) {
        koiosUI.EventHandler.connect(document, theEvent, koios.contains(_noTarget, theEvent));
    });
}());