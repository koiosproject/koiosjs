/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koiosui_touchevents.js
Provides koiosUI.EventHandler routing for touch events
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module, koiosUI*/

(function () {
//    return;
    
    var _events = ["touchstart", "touchmove", "touchend", "click"];
    
    koios.each(_events, function (theEvent) {
        koiosUI.EventHandler.connect(document, theEvent, false);
    });
}());