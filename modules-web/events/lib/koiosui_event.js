/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koiosui_event.js
Provides koios.preventDefault() and koios.stopPropagation()
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module*/

/**
Provides DOM event support for koiosUI.Views and event normalization on
touch/pointer/mouse platforms, as well as basic gesture support (flick, hold, holdRelease)

@module web/events
*/

/**
@class koios
*/

/**
Safely calls preventDefault() on theEvent, either a HTML event or a 
Koios-generated event

__Requires modules-web/events module__

@method preventDefault
@param {Event or Object} theEvent - the event to preventDefault on
*/

koios.preventDefault = function (theEvent) {
    if (theEvent.event) {
        return koios.preventDefault(theEvent.event);
    }
    
    if (theEvent.preventDefault) {
        theEvent.preventDefault();
    }
};

/**
Safely calls stopPropagation() on theEvent, either a HTML event or a 
Koios-generated event

__Requires modules-web/events module__

@method stopPropagation
@param {Event or Object} theEvent - the event to stopPropagation on
*/

koios.stopPropagation = function (theEvent) {
    if (theEvent.event) {
        return koios.stopPropagation(theEvent.event);
    }
    
    if (theEvent.stopPropagation) {
        theEvent.stopPropagation();
    }
};