/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koiosui_normalizer.js
Provides the event normalizer used by koiosUI.EventHandler
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module, koiosUI*/

// =================================================
// Basic Normalization

koiosUI._eventNormalizer = {
    holdTriggers: ["tapstart"],
    holdEnd: ["tapend", "tapmove"],
    isHeld: false,
    
    _startTs: null,
    _startTarget: null,
    _startEv: null,
    
    _flickTs: null,
    _flickTarget: null,
    _flickEv: null,
    
    _lastTapTs: null,
    
    _draggable: [],
    _dragEl: [],
    _dragPt: null,
    _dragging: false,
    
    events: {
        mousedown: function (event) {
            this._tapStart(event, "mouse");
        },
        touchstart: function (event) {
            this._tapStart(event, "touch");
        },
        mouseup: function (event) {
            this._tapEnd(event, "mouse");
        },
        touchend: function (event) {
            this._tapEnd(event, "touch");
        },
        mousemove: function (event) {
            this._tapMove(event, "mouse");
        },
        touchmove: function (event) {
            this._tapMove(event, "touch");
        }
    },
    
    props: ["target", "which", "altKey", "ctrlKey", "metaKey", "shiftKey", "pageX", "pageY", "screenX", "screenY"],
    pointProps: ["pageX", "pageY", "screenX", "screenY"],
    
    mouse: {
        createEvent: function (eventName, event) {
            var ev = {
                _eventType: "mouse",
                targetObject: event.targetObject,
                type: eventName,
                event: event,
                preventDefault: function () {
                    koios.preventDefault(ev);
                },
                stopPropagation: function () {
                    koios.stopPropagation(ev);
                }
            };

            koios.each(koiosUI._eventNormalizer.props, function (prop) {
                ev[prop] = event[prop];
            });
                        
            return ev;
        }
    },
    
    touch: {
        createEvent: function (eventName, event) {
            var ev = {
                _eventType: "touch",
                targetObject: event.targetObject,
                type: eventName,
                event: event,
                preventDefault: function () {
                    koios.preventDefault(ev);
                },
                stopPropagation: function () {
                    koios.stopPropagation(ev);
                }
            },
                touch = event.touches ? event.touches[0] : null;

            koios.each(koiosUI._eventNormalizer.props, function (prop) {
                ev[prop] = event[prop];
            });
            
            if (touch) {
                koios.each(koiosUI._eventNormalizer.pointProps, function (prop) {
                    ev[prop] = touch[prop];
                });
            }
            
            return ev;
        }
    },
    
    _tapStart: function (event, type) {
        var ev = this[type].createEvent("tapstart", event);
        koiosUI.EventHandler.dispatch(ev);
    },
    
    _tapEnd: function (event, type) {
        var ev = this[type].createEvent("tapend", event);
        koiosUI.EventHandler.dispatch(ev);
    },
    
    _tapMove: function (event, type) {
        var ev = this[type].createEvent("tapmove", event);
        koiosUI.EventHandler.dispatch(ev);
    },
    
    _tap: function () {
        var type = this._startEv._eventType,
            ev = this[type].createEvent("tap", this._startEv);
        this._lastTapTs = koios.now();
        koiosUI.EventHandler.dispatch(ev);
    },
    
    _doubleTap: function () {
        var type = this._startEv._eventType,
            ev = this[type].createEvent("doubletap", this._startEv);
        this._lastTapTs = null;
        koiosUI.EventHandler.dispatch(ev);
    },
    
    _hold: function () {
        var type = this._holdEv._eventType,
            ev = this[type].createEvent("hold", this._holdEv);
        this.isHeld = true;
        this._dragging = false;
        this._draggable.length = 0;
        koiosUI.EventHandler.dispatch(ev);
    },
    
    _holdEnd: function () {
        var type = this._holdEv._eventType,
            ev = this[type].createEvent("holdEnd", this._holdEv);
        this.isHeld = false;
        koiosUI.EventHandler.dispatch(ev);
    },
    
    _flick: function (dx, dy) {
        var type = this._flickEv._eventType,
            ev = this[type].createEvent("flick", this._flickEv);
        
        ev.deltaX = dx;
        ev.deltaY = dy;
        
        koiosUI.EventHandler.dispatch(ev);
    },
    
    _dragstart: function (event) {
        var type = event._eventType,
            ev = this[type].createEvent("dragstart", event);
        koiosUI.EventHandler.dispatch(ev);
    },
    
    _dragend: function (event) {
        var type = event._eventType,
            ev = this[type].createEvent("dragend", event);
        koiosUI.EventHandler.dispatch(ev);
    },
    
    _dragmove: function (event) {
        var type = event._eventType,
            ev = this[type].createEvent("dragmove", event);
        koiosUI.EventHandler.dispatch(ev);
    },
    
    _animend: function (event) {
        var type = event._eventType,
            ev = this.mouse.createEvent("animationend", event);
        koiosUI.EventHandler.dispatch(ev);
    }
};

koiosUI.EventHandler._process.push(function (event) {
    var eventName = event.type,
        handler = koiosUI._eventNormalizer.events[eventName];
    
    if (handler) {
        return handler.call(koiosUI._eventNormalizer, event);
    }
});

// =================================================
// Flick Gesture

koiosUI.EventHandler._process.push(function (event) {
    var eventName = event.type,
        endTs,
        dx,
        dy,
        dt;
    
    if (eventName === "tapstart") {
        koiosUI._eventNormalizer._flickTs = koios.now();
        koiosUI._eventNormalizer._flickPoint = {x: event.pageX, y: event.pageY};
        koiosUI._eventNormalizer._flickEv = event;
    }
    
    if (eventName === "tapend") {
        endTs = koios.now();
        dt = endTs - koiosUI._eventNormalizer._flickTs;
        if (dt < 150) {
            dx = koiosUI._eventNormalizer._flickPoint.x - event.pageX;
            dy = koiosUI._eventNormalizer._flickPoint.y - event.pageY;
            
            if (Math.abs(dx) > 10 || Math.abs(dy) > 10) {
                koiosUI._eventNormalizer._flick(dx / endTs, dy / endTs);
            }
        }
        
        koiosUI._eventNormalizer._flickTs = null;
        koiosUI._eventNormalizer._flickPoint = null;
        koiosUI._eventNormalizer._flickEv = null;
    }
});

// =================================================
// Disabling other events while dragging.

koiosUI.EventHandler._process.push(function (event) {
    var eventName = event.type;
        
    if (koiosUI._eventNormalizer._dragging) {
        if (eventName === "tapmove") {
            koiosUI._eventNormalizer._dragmove(event);
        }

        if (eventName === "tapend") {
            koiosUI._eventNormalizer._dragend(event);
            koiosUI._eventNormalizer._dragging = false;
            koiosUI._eventNormalizer._draggable.length = 0;
        }
        
        if (!koios.contains(eventName, "drag") && eventName !== "flick") {
            return true;
        }
    }
});

// =================================================
// Tap

/*
We don't handle clicks on the DOM because of their unpredictability with regards
to touch browsers.
*/

koiosUI.EventHandler._process.push(function (event) {
    var eventName = event.type,
        endTs;
    
    if (eventName === "tapstart") {
        koiosUI._eventNormalizer._startTs = koios.now();
        koiosUI._eventNormalizer._startTarget = event.target;
        koiosUI._eventNormalizer._startEv = event;
    }
    
    if (eventName === "tapend") {
        endTs = koios.now();
        if (endTs - koiosUI._eventNormalizer._startTs < 300) {
            if (event.target === koiosUI._eventNormalizer._startTarget) {
                if (endTs - koiosUI._eventNormalizer._lastTapTs < 300) {
                    koiosUI._eventNormalizer._doubleTap();
                } else {
                    koiosUI._eventNormalizer._tap();
                }
            }
        }
        
        koiosUI._eventNormalizer._startTs = null;
        koiosUI._eventNormalizer._startTarget = null;
        koiosUI._eventNormalizer._startEv = null;
    }
});

// =================================================
// Dragging

koiosUI.EventHandler._process.push(function (event) {
    var eventName = event.type,
        target = event.targetObject,
        dx,
        dy;
    
    if (eventName === "tapstart") {
        while (target) {
            if (target.draggable) {
                koiosUI._eventNormalizer._draggable.push(target);
            }
            
            target = target.get("owner");
        }
        
        if (koiosUI._eventNormalizer._draggable.length > 0) {
            koiosUI._eventNormalizer._dragPt = {x: event.pageX, y: event.pageY};
        }
    }
    
    if (eventName === "tapmove" && koiosUI._eventNormalizer._draggable.length > 0) {
        // Determine direction
        dx = koiosUI._eventNormalizer._dragPt.x - event.pageX;
        dy = koiosUI._eventNormalizer._dragPt.y - event.pageY;

        koiosUI._eventNormalizer._dragEl = koios.array.filter(koiosUI._eventNormalizer._draggable, function (theEl) {
            if (theEl.shouldDrag) {
                return theEl.shouldDrag(dx, dy);
            }

            return false;
        });

        if (koiosUI._eventNormalizer._dragEl.length > 0) {
            koiosUI._eventNormalizer._dragging = true;
        }

        koiosUI._eventNormalizer._dragstart(event);
    }
    
    if (eventName === "tapend" && !koiosUI._eventNormalizer._dragging) {
        koiosUI._eventNormalizer._dragging = false;
        koiosUI._eventNormalizer._draggable.length = 0;
    }
});

// =================================================
// Hold Gesture

koiosUI.EventHandler._process.push(function (event) {
    var eventName = event.type;
    
    if (koios.contains(koiosUI._eventNormalizer.holdTriggers, eventName)) {
        koiosUI._eventNormalizer._holdEv = event;
        koios.timeout("_koiosHoldHandler", koios.bind(koiosUI._eventNormalizer, "_hold"), 500);
    }
    
    if (koios.contains(koiosUI._eventNormalizer.holdEnd, eventName)) {
        koios.clearTimeout("_koiosHoldHandler");
        if (koiosUI._eventNormalizer.isHeld) {
            koiosUI._eventNormalizer._holdEnd();
        }
    }
});

// =================================================
// Transition Normalizer

koiosUI.EventHandler._process.push(function (event) {
    var eventName = event.type;
    
    if (eventName === "webkitAnimationEnd") {
        koiosUI._eventNormalizer._animend(event);
    }
});
