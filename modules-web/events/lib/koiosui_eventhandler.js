/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koiosui_eventhandler.js
Provides koiosUI.EventHandler
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module, koiosUI*/

/**
The koiosUI.EventHandler takes all document intercepted events and reroutes them. Some events are
broadcasted globally - via the koios.messageEmitter (window events and certain target-less document events)

All mouse/pointer/touch events are sent to unabated to the target elements in their original form, in case
you want to separate touchstart and mousedown. They are also normalized:

    tapstart - mousedown/touchstart
    tapend - mouseup/touchend
    tapmove - mousemove/touchmove
    tap
    doubletap
    
    
Dragging is handled via the normalizer

    dragstart
    dragend
    drag
    
Additionally, certain gestures are provided as well

    hold
    holdEnd
    flick
    
koiosUI.EventHandler is a singleton provided for you via the events module. You do not need to create one yourself.

@class koiosUI.EventHandler
@extends koios.Class
*/

koios.singleton({
    className: "koiosUI.EventHandler",
    extend: "koios.Class",
    ignoreAlias: true,
        
    _process: [],
    
    $init: function () {
        this.$super("$init", arguments);
        
        var t = this;
        
        koios.each(this._docEvents, function (theEvent) {
            t.connect(document, theEvent);
        });
    },
    
    connect: function (target, theEvent, globally) {
        if (globally) {
            koios.messageEmitter.broadcast("koiosui:" + theEvent);
            this.listen(target, theEvent, this.bind(function (event) {
                this.emitGlobally(theEvent, event);
            }));
        } else {
            var handle = this.bind("dispatch");
            this.listen(target, theEvent, function (event) {
                return handle(event);
            });
        }
    },
    
    listen: function (theTarget, theEvent, theListener) {
        if (theTarget.addEventListener) {
            theTarget.addEventListener(theEvent, theListener, false);
        }
    },
    
    emitGlobally: function (eventName, theEvent) {
        koios.emit("koiosui:" + eventName, theEvent);
    },
    
    dispatch: function (theEvent) {
        var target,
            i,
            retVal,
            theFn;
        
                
        if (!theEvent._koiosTarget) {
            target = this.findTarget(theEvent.target);
            theEvent.targetObject = target;
        } else {
            target = theEvent.targetObject;
        }
        
        for (i = 0; i < this._process.length; i += 1) {
            theFn = this._process[i];
            retVal = theFn.call(this, theEvent);
            if (retVal === true) {
                theEvent.preventDefault();
                theEvent.stopPropagation();
                return true;
            }
        }

        if (target && !theEvent._koiosPrevent) {
            return target.bubble(theEvent.type, theEvent);
        }
    },
    
    findTarget: function (theNode) {
        var node,
            target = theNode,
            id;
        
        while (target) {
            id = koios.attr(target, "koios-id");
            if (id && koiosUI._$[id]) {
                node = koiosUI._$[id];
                break;
            }
            target = target.parentNode;
        }
        
        return node || koios.master;
    }
});
