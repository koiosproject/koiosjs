/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koiosui_window.js
Provides koiosUI.EventHandler routing for window/css events
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module, koiosUI*/

(function () {
    var _events = ["resize", "load", "unload", "message", "hashchange"],
        _css = ["webkitTransitionEnd", "transitionend", "webkitAnimationEnd", "animationend"];
    
    koios.each(_events, function (theEvent) {
        koiosUI.EventHandler.connect(window, theEvent, true);
    });
    
    koios.each(_css, function (theEvent) {
        koiosUI.EventHandler.connect(document, theEvent, false);
    });
}());