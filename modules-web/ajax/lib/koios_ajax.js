/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koios_ajax.js
Provides koios.Ajax - a lightweight AJAX wrapper.
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module*/

/**
A module providing XMLHttpRequests and AJAX calls

@module web/ajax
*/

/**
The base Ajax class. It isn't strictly aliased, but a convenience method is available via koios.ajax()

    koios.ajax("/api/endpoint");
    
You can also pass in an entire object definition

    koios.ajax({
        method: "POST",
        url: "/api/post/endpoint"
    });
    
Both of which will return a koios.Promise which will be resolved using a koios.Response containing the response
from the server.

4 class methods are available as convenience methods as well:

    koios.Ajax.GET();
    koios.Ajax.POST();
    koios.Ajax.DELETE();
    koios.Ajax.PUT();
    
Which can take either a String or Object defintion, and return a koios.Promise - just like koios.ajax()

@class koios.Ajax
@extend koios.XHR
*/

koios.define({
    className: "koios.Ajax",
    extend: "koios.XHR",
    ignoreAlias: true,
    
/**
The "Content-Type" header to append to the request headers. Default is "application/x-www-form-urlencoded"

@atribute contentType
@type String
*/
    
    contentType: "application/x-www-form-urlencoded",
    
/**
Whether or not to allow cached responses. Default is false.

@attribute useCache
@type BOOL
*/
    
    useCache: false,
    
/**
The expected response type. Default is "json" - available are "text", "xml" and "json". koios.Ajax
will attempt to read the content type of the response in order to parse the response. 

@attribute responseType
@type String
*/
    
    responseType: "json",
    
/**
Any parameters to pass into either the body of the request, or as GET params (?something=value). Default is {}. Optional

@attribute params
@type Object
*/
    
    params: {},
    
	statics: {
		GET: function (args) {
			if (koios.isString(args)) {
				args = {url: args};
			}
			args.method = "GET";
			return koios.ajax(args);
		},
		POST: function (args) {
			if (koios.isString(args)) {
				args = {url: args};
			}
			args.method = "POST";
			return koios.ajax(args);
		},
		DELETE: function (args) {
			if (koios.isString(args)) {
				args = {url: args};
			}
			args.method = "DELETE";
			return koios.ajax(args);
		},
		PUT: function (args) {
			if (koios.isString(args)) {
				args = {url: args};
			}
			args.method = "PUT";
			return koios.ajax(args);
		}
	},
    
    $init: function () {
        this.promise = koios.promise();
        this.$super("$init", arguments);
        
        this.start();
    },

// =================================================
// XHR Override
    
    start: function () {
        if (!this.url || this.url === "") {
            return koios.warn("koios.Ajax error: no URL at start()");
        }
        
        var urlParts = this.url.split("?"),
            uri = urlParts.shift() || "",
            args = koios.queryString("?" + urlParts.join("&")),
            callHeaders = {"Content-Type": this.contentType};
        
        if (!koios.isEmpty(this.params)) {
            args.set(this.params);
        }
        
        koios.mixin(this.headers, callHeaders);
        if (!this.useCache) {
            args.set("cache", koios.now());
        }
        
        if (!this.body || this.body === "") {
            this.body = args.toString();
        }
        
        if (this.method === "GET") {
            this.url = uri + args.toString();
        }
        
        this.on("timeout", this.bind("onTimeout"));
        this.on("data", this.bind("onData"));
        
        this.$super("start");
    },
    
// =================================================
// Events
    
    onTimeout: function (xhr) {
        return this.promise.reject(new koios.Response({xhr: xhr, text: "timeout"}));
    },
    
    onData: function (xhr) {
        if (this._isFailure(xhr)) {
			return this.promise.reject(new koios.Response({xhr: xhr}));
		}
        
		var respType = this._contentType(xhr.getResponseHeader('Content-Type') || this.responseType.toLowerCase()),
            call = "_to" + koios.string.cap(respType);
        
		this[call](xhr);
    },

    _toJson: function (xhr) {
		this.promise.fulfill(new koios.Response({xhr: xhr, json: koios.JSON.parse(xhr.responseText)}));
		this.destroyLater();
	},

	_toXml: function (xhr) {
		this.promise.fulfill(new koios.Response({xhr: xhr, xml: xhr.responseXML}));
		this.destroyLater();
	},

	_toText: function (xhr) {
		this.promise.fulfill(new koios.Response({xhr: xhr}));
		this.destroyLater();
	},

// =================================================
// Private Manipulation

	_isFailure: function (xhr) {
		if (xhr.status === 0 && koios.isEmpty(xhr.responseText)) {
			return true;
        }
        
		return (xhr.status !== 0) && (xhr.status < 200 || xhr.status >= 300);
	},

	_contentType: function (theHeader) {
		if (!theHeader) {
			return null;
        }
        
		if (theHeader.toLowerCase().indexOf("json") > -1) {
			return "json";
		} else if (theHeader.toLowerCase().indexOf("xml") > -1) {
			return "xml";
		} else {
			return "text";
		}
	}
});

// =================================================

/**
@class koios
*/

/**
Returns a koios.Promise that will be resolved via a koios.Ajax request. It can be used by passing
in a single String URL.

    koios.ajax("/api/endpoint");
    
Or, you can also pass in an entire object definition

    koios.ajax({
        method: "POST",
        url: "/api/post/endpoint"
    });

__Requires the modules-web/ajax module__

@method ajax
@param {String or Object} args - any arguments to pass into the koios.Ajax definition.
@returns koios.Promise
*/

koios.ajax = function (args) {
    if (koios.isString) {
        args = {url: args};
    }
    
    var t = new koios.Ajax(args);
    return t.promise;
};
    
    