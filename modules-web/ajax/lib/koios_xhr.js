/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koios_xhr.js
Provides koios.XHR - the base XMLHttpRequest object wrapper
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module*/

/**
A module providing XMLHttpRequests and AJAX calls

@module web/ajax
*/

koios.asyncProps =  {
	url: "",
	method: "GET",
	timeout: 5000,
	body: null,
	headers: {},
	username: null,
	password: null,
	async: true,
	fields: {}
};

// =================================================

/**
koios.XHR is the very base wrapper class around the standard XMLHttpRequest (XHR)

By default the following attributes are mixed into a koios.XHR

    {
        url: "",
        method: "GET",
        timeout: 5000,
        body: null,
        headers: {},
        username: null,
        password: null,
        async: true,
        fields: {}
    }
    
@class koios.XHR
@extend koios.Class
@uses koios.EventSupport
*/

koios.define({
    className: "koios.XHR",
    extend: "koios.Class",
    inherit: ["koios.EventSupport"],
    ignoreAlias: true,
    
    $constructor: function () {
        koios.mixin(this, koios.asyncProps);
        this.$super("$constructor", arguments);
    },
    
    $init: function () {
        this.$super("$init", arguments);
        this.xhr = this._makeXHR();
    },
    
/**
The actual URL to get. Default is "", but not having a URL will result in a warning.

@attribute url
@type String
*/

/**
The HTTP Request Method. Default is "GET".

@attribute method
@type String
*/
    
/**
The timeout duration in milliseconds. Default is 5000.

@attribute timeout
@type Number
*/
    
/**
Any request header fields to mix into the XHR. Default is {}

@attribute headers
@type Object
*/
    
/**
If using authentication, the username to pass along

@attribute username
@type String
*/
    
/**
If using authentication, the password to pass along

@attribute password
@type String
*/
    
/**
Whether or not to make the XHR asynchronous. Default is true.

@attribute async
@type BOOL
*/
    
/**
Any additional fields to mix into the XHR itself. Default is {}.

@attribute fields
@type Object.
*/
    
// =================================================
// Private
    
    _makeXHR: function () {
        try {
            return new XMLHttpRequest();
        } catch (error) {
            koios["throw"](error);
        }
    },
    
// =================================================
// Actions
   
/**
Stops the XHR

@method cancel
@returns this
*/
    
    cancel: function () {
        this.emit("cancel", this.xhr);
        this.xhr.onreadystatechange = null;
        this.xhr = null;
        
        return this;
    },
    
/**
Starts the XHR using the properties already mixed into
the koios.XHR

@method start
@returns this
*/
    
    start: function () {
        if (this.username && this.username !== "") {
            this.xhr.open(this.method, this.url, this.async, this.username, this.password);
        } else {
            this.xhr.open(this.method, this.url, this.async);
        }
        
        var t = this;
        
        this.emit("open", this.xhr);
        koios.mixin(this.xhr, this.fields);
        
        this.xhr.onreadystatechange = function () {
            if (this.readyState === 4) {
                t.emit("data", this);
            }
        };
        
        if (!this.headers) {
            this.headers = {};
        }
        
        if (this.method === "GET") {
            this.headers["cache-control"] = "no-cache";
        }
        
        koios.each(this.headers, function (key, value) {
            t.xhr.setRequestHeader(key, value);
        });
        
        this.xhr.timeout = this.timeout;
        this.xhr.ontimeout = function () {
            t.emit("timeout", this);
        };
        
        this.xhr.send(this.body);
        
        if (!this.async) {
            this.xhr.onreadystatechange(this.xhr);
        }
        
        return this;
    },
  
// =================================================
// Garbage Collection (borrowed from koios.Base)
    
    destroyLater: function () {
        koios.defer(this.bind("destroy"));
    }
});
