/*
KoiosJS: A Cross-Platform JavaScript Application Toolkit

=================================================
koios_response.js
Provides koios.Response - the object passed into the promise
resolved by koios.Ajax
=================================================
*/

/*jslint sloppy:true*/
/*jslint nomen:true*/
/*global koios, module*/

/**
A module providing XMLHttpRequests and AJAX calls

@module web/ajax
*/

/**
All responses of koios.Ajax requests are passed into resolved koios.Promises as a koios.Response.

@class koios.Response
@extend koios.Class
*/

/**
The raw text from the response

@attribute text
@type String
*/

/**
If the response is valid JSON - it will get parsed into an Object and saved as .json

@attribute json
@type Object
*/

/**
The XHR status (if available)

@attribute status
@type Number
*/

koios.define({
    className: "koios.Response",
    extend: "koios.Class",
    ignoreAlias: true,
    
    $init: function () {
        this.$super("$init", arguments);
        
        if (this.xhr) {
            this.text = (!this.hasOwnProperty("text")) ? this.xhr.responseText : this.text;
            this.status = this.xhr.status;
        }
    }
});
